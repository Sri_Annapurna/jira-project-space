//calls on page load
$("td a").on("click", function() {
    $("body").scrollTop(0);
});
var img = document.createElement("IMG");
img.src = "images/712.GIF";
img.height = "70";
img.width = "70";
img.style.margin ="50px 0px 0px 0px";
var dynamicMethod= document.getElementById("function").innerHTML;
var DREdata = defectsData = reviewData = drTable = drTable2 = defectsTable = defectsTable2 = SRFGraph = SRFTable = ftrTable2 = buttonData = atpData = cGraph = cData = appData = aspectData = complaintData = velocityChart = rgraph = rtable = cidiData = requirementData = cycletime = dynamicData = vCGraphData = vCTableData = vCTableData2 =  img;
var radcTable = img; 
var ftrTable = SRFGraph = SRFGraph2= img; 
var csatTarget,riskTarget,dreTarget,revTarget,srfTarget;
var atpURL,clientURL,configurationURL,networkURL,teURL,processURL,retroURL,momURL;
var isCGraphActive = isDistributionGActive = isGraphFormat = isVGraphActive = true;
var isRGraphActive = isRemovalGActive = isReviewGActive = isSRFGActive = isCard1Maximize = isCard3Maximize = isCard2Maximize = false;
var note = false;

$(document).ready(function(){
	buttonDisable("five","six");
		loadGif();
       setTargetData();
  });
	    /*   $(window).resize(function(){
	    	 
	    	}); */
$("#moreTab").click(function(){
	$(this).parent().parent().find('li a.active').removeClass('active');
});
	    	function atp(){
	    		window.open(atpURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function client(){
	    		window.open(clientURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function configuration(){
	    		window.open(configurationURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function te(){
	    		window.open(teURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function network(){
	    		window.open(networkURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function process(){
	    		window.open(processURL,"_blank","width=1000,height=500, scrollbars=1");
	    	}
        function retro(){
          window.open(retroURL,"_blank","width=1000,height=500, scrollbars=1");
        }
        function mom(){
          window.open(momURL,"_blank","width=1000,height=500, scrollbars=1");
        }
//Client Satisfaction Rating Graph
google.charts.load('current', {'packages':['line']});
function drawCSATChart(x) {
	if(isCard1Maximize){
		 document.getElementById("csat_div").classList.remove("responsiveGraphs1");
         document.getElementById("csat_div").classList.add("responsiveGraphs");
	}else{
		document.getElementById("csat_div").classList.remove("responsiveGraphs");
        document.getElementById("csat_div").classList.add("responsiveGraphs1");
	}
	if(cGraph.text()!=0){
      var values2 = x;    
      var results1 = JSON.parse(values2);    
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Period');
        data.addColumn('number', 'CSAT Rating');
        // A column for custom tooltip content
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'annotation'});
        data.addColumn('number','Target');
        data.addRows(results1);
  		var options = { 
  				title: 'Client Rating [Target>='+ csatTarget +']',
  	    		 titleFontSize:17,
  				 hAxis: {title: 'Period'},
  	             vAxis: {title: 'CSAT Rating',format: '0', viewWindow:{min:0, max:5}, gridlines:{count:6},ticks :[0,1,2,3,4,5]},
  	              pointsVisible: true, 
  	             focusTarget: 'category',
  	             series: {
  	   	    	  0:{color: '#097138'},
  	   	    	  1:{ color: 'red',
  	                 	   pointsVisible: false }
  	   	      },
  	             is3D:true
       }; 
  		loadData('csat_div');
  		var chart = new google.visualization.LineChart(document.getElementById('csat_div'));
  		chart.draw(data, options);
  		if(note=='true'){
		var p = document.createElement("p");
		var node = document.createTextNode("*Internal CSAT Rating");
		p.appendChild(node);
		p.style.textAlign = "left";
		document.getElementById("csat_div").appendChild(p);
  		}
	}else{
		var div = document.createElement("div");
		var heading = document.createElement("h4");
		heading.innerHTML = "Client Feedback Rating";
		div.appendChild(heading);
		var para = document.createElement("h5");
		para.innerHTML = "There is no data to display graph";
		div.appendChild(para);
		$("#csat_div").html(div);
	}
      }
//New Velocity Chart
google.charts.load('current', {'packages':['corechart','bar','line']});
function drawNewVelocityChart(x){
	if(isCard2Maximize){
		 document.getElementById("riskrating_div").classList.remove("responsiveGraphs1");
       document.getElementById("riskrating_div").classList.add("responsiveGraphs");
	}else{
		document.getElementById("riskrating_div").classList.remove("responsiveGraphs");
      document.getElementById("riskrating_div").classList.add("responsiveGraphs1");
	}
	if(vCGraphData!= 0){
	var values = x;
    var result1 = JSON.parse(values);
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Sprints');
    data.addColumn('number', 'Committed Story Points');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addColumn('number', 'Completed Story Points');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addColumn('number','Team Capacity');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addRows(result1);
    var options = {
            title: 'Velocity Chart',
            titleFontSize:16,
            hAxis: {title: 'Sprints'},
            vAxis: {title: 'Story Points',format: '#',viewWindow:{min:0}},
            seriesType: 'bars',
            series: {
            	0: {color: '#cccccc'},
                1: {color: '#14892c'},
                2: {type:'line', pointsVisible: true, color: '#FF9500'}
            }
      }; 
      loadData("riskrating_div");
      var chart = new google.visualization.ComboChart(document.getElementById('riskrating_div'));
      chart.draw(data, options);
	}
	else{
		$("#riskrating_div").html(vCTableData2);
	}
		
}

//Risk Exposure Rating
function drawRiskRatingGraph(x) {
	if(isCard2Maximize){
		 document.getElementById("riskrating_div").classList.remove("responsiveGraphs1");
        document.getElementById("riskrating_div").classList.add("responsiveGraphs");
	}else{
		document.getElementById("riskrating_div").classList.remove("responsiveGraphs");
       document.getElementById("riskrating_div").classList.add("responsiveGraphs1");
	}
    var values2 = x;
   
   var results1 = JSON.parse(values2);
    
     var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Before Risk Treatment');
      data.addColumn({type: 'string', role: 'tooltip'});
      data.addColumn({type: 'string', role: 'annotation'});
      data.addColumn('number', 'After Risk Treatment');
      data.addColumn({type: 'string', role: 'tooltip'});
      data.addColumn({type: 'string', role: 'annotation'});
      data.addColumn('number','Target');
      data.addRows(results1); 
     var options = {
    		 title: 'Risk Exposure Rating [Target <='+ riskTarget +'- Quarterly]', 
    		 titleFontSize:16,
    		 hAxis: {
    	          title: 'Months'
    	        },
    	        vAxis: {
    	          title: 'Risk Exposure Rating',format: '0', viewWindow:{min:0, max:5}, gridlines:{count:6},ticks :[0,1,2,3,4,5]
    	        },
    	        pointsVisible: true,
    	      focusTarget: 'category',
    	      series: {
    	    	  1:{color: '#097138'},
    	    	  2:{ color: 'red',
                  	   pointsVisible: false }
    	      }
    	      };
     	loadData("riskrating_div");
		var chart = new google.visualization.LineChart(document.getElementById('riskrating_div'));

	       chart.draw(data, options);
    }  
//DRE Graph
function drawDREChart(x) {
	if(isCard3Maximize){
		 document.getElementById("graph").classList.remove("responsiveGraphs1");
        document.getElementById("graph").classList.add("responsiveGraphs");
	}else{
		document.getElementById("graph").classList.remove("responsiveGraphs");
       document.getElementById("graph").classList.add("responsiveGraphs1");
	}
	if(DREdata.text()!=0){
		var values2 = x;
   var results1 = JSON.parse(values2);
   var data = new google.visualization.DataTable();
   data.addColumn('string', 'Month');
   data.addColumn('number', 'DRE (%)');
   data.addColumn({type: 'string', role: 'annotation'});
   data.addColumn('number','Target(%)');
   data.addRows(results1);
    var options = { 
           title: 'Defect Removal Efficiency [Target>='+ dreTarget +'% - Quarterly]',
           titleFontSize:17,
           hAxis: {title: 'Months'},
           vAxis: {title: 'DRE(%)', format: '#', viewWindow:{min:0, max:100},ticks :[0,25,50,75,100]},
           pointsVisible: true,
           series: {
               1: { color: 'red',
            	   pointsVisible: false }
               }
     }; 
    var chart = new google.visualization.LineChart(document.getElementById('graph'));

         chart.draw(data, options);
	}
	else{
		$("#graph").html(drTable2);
	}
    }     
//Defects Data Distribution Graph
google.charts.load('current', {'packages':['corechart','bar','line']});
function drawBarGraph(x) {
	if(isCard3Maximize){
		 document.getElementById("graph").classList.remove("responsiveGraphs1");
        document.getElementById("graph").classList.add("responsiveGraphs");
	}else{
		document.getElementById("graph").classList.remove("responsiveGraphs");
       document.getElementById("graph").classList.add("responsiveGraphs1");
	}
	 if(defectsData.text()!=0){
    var values = x;
    var result1 = JSON.parse(values);
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Delivered Defects');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addColumn('number', 'Review Defects');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addColumn('number', 'Testing Defects');
    data.addColumn({type: 'string', role: 'annotation'});
    data.addRows(result1);
    var options = {
            title: 'Defects Count',
            titleFontSize:16,
            hAxis: {title: 'Months'},
            vAxis: {title: 'Count of defects',format: '#',ticks :[0,2,4,6,8,10,12]},
            series: {
            	0: { color: 'red' },
            	1: {color: '#ffd1b3'},
                2: { color: '#ffd699'}
                
            }
      }; 

      var chart = new google.visualization.ColumnChart(document.getElementById('graph'));
    
      chart.draw(data, options);
	 }
	 else{
		 $("#graph").html(defectsTable2);
	 }
    }
//Review Effectiveness Graph
function drawLineChart(x) {
	if(isCard3Maximize){
		 document.getElementById("graph").classList.remove("responsiveGraphs1");
       document.getElementById("graph").classList.add("responsiveGraphs");
	}else{
		document.getElementById("graph").classList.remove("responsiveGraphs");
      document.getElementById("graph").classList.add("responsiveGraphs1");
	}
	if(reviewData.text()!=0){
    var values2 = x;
   var results1 = JSON.parse(values2);
   var data = new google.visualization.DataTable();
   data.addColumn('string', 'Month');
   data.addColumn('number', 'Review Effectiveness (%)');
   data.addColumn({type: 'string', role: 'annotation'});
   data.addColumn('number','Target(%)');
   data.addRows(results1);
  var options = { 
           title: 'Review Effectiveness[Target>='+ revTarget +'% - Quarterly]',
           titleFontSize:16,
           hAxis: {title: 'Months'},
           vAxis: {title: 'Review Effectiveness %', format: "#",viewWindow:{min:0, max :100},ticks: [0,25,50,75,100]},
           pointsVisible: true,
           series: {
               1: { color: 'red',
            	   pointsVisible: false }
               }
     }; 
  var chart = new google.visualization.LineChart(document.getElementById('graph'));

       chart.draw(data, options);
	}else{
		$("#graph").html(drTable2);
	}
    }          
//SRF Graph
function drawSRFGraph(x) {
	if(isCard3Maximize){
		 document.getElementById("graph").classList.remove("responsiveGraphs1");
       document.getElementById("graph").classList.add("responsiveGraphs");
	}else{
		document.getElementById("graph").classList.remove("responsiveGraphs");
      document.getElementById("graph").classList.add("responsiveGraphs1");
	}
	
	if(SRFGraph != img && x!=0){
	var values2 = x;  
   var results1 = JSON.parse(values2);
   var data = new google.visualization.DataTable();
   data.addColumn('string', 'Month');
   data.addColumn('number', 'Requirements Delivered First Time Right(%)');
   data.addColumn({type: 'string', role: 'annotation'});
   data.addColumn('number','Target(%)');
   data.addRows(results1);
   
		var options = { 
           title: 'Requirements Delivered First Time Right[Target>='+ srfTarget +'% - Quarterly]',
           titleFontSize:16,
           hAxis: {title: 'Months'},
           vAxis: {title: 'SRF (%)', format: '#', viewWindow:{min:0, max:100},ticks: [0,25,50,75,100]},
           pointsVisible: true,
           series: {
               1: { color: 'red',
            	   pointsVisible: false }
               }
     }; 
		var chart = new google.visualization.LineChart(document.getElementById('graph'));

	       chart.draw(data, options);
	}
	else{
		$("#graph").html(ftrTable2);
	}
    }      
//Able toggle buttons (graph and table)
function buttonAble(a,b){
	document.getElementById(a).disabled = false;
    document.getElementById(a).style.cursor = 'pointer';
    document.getElementById(b).disabled = false;
    document.getElementById(b).style.cursor = 'pointer';
}
//Disable toggle buttons (graph and table)
function buttonDisable(a,b){
	document.getElementById(a).disabled = true;
  document.getElementById(a).style.cursor = 'default';
  document.getElementById(b).disabled = true;
  document.getElementById(b).style.cursor = 'default';
}
//Show graph
function showGraphs(a , b){
	document.getElementById(a).style.display = 'block'; 
	document.getElementById(a).style.width = '100%'; 
  	document.getElementById(b).style.display = 'none';
}
//Show Table
function showData(a,b){
		  document.getElementById(a).style.display = 'none';
		  document.getElementById(b).style.display = 'block';
}
//Maximize Card
$(document).ready(function () {
    $("#maximize").click(function (e) {
    	e.preventDefault();
       
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
        	isCard1Maximize = true;
        	
        	$this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
        	isCard1Maximize = false;
        	$this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
        if(isCGraphActive == true && $("#feedback").hasClass("active")){
  			  google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));  
        }
    });
    $("#maximize1").click(function (e) {
    	e.preventDefault();
       
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
        	isCard2Maximize = true;
        	$("#secondCard").removeClass('responsiveCard');
        	/*if($("#risks").hasClass("active")){
        	document.getElementById("tableData").classList.remove("minTable");
            document.getElementById("tableData").classList.add("maxTable");
        	}*/
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
        	isCard2Maximize = false;
        	$("#secondCard").addClass('responsiveCard');
        	/*if($("#risks").hasClass("active")){
        	document.getElementById("tableData").classList.remove("maxTable");
            document.getElementById("tableData").classList.add("minTable");
        	}*/
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
        if(isRGraphActive == true && $("#risks").hasClass("active")){
        	google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));  
      }
        if(isVGraphActive == true && $("#dynamicTab").hasClass("active")){
        	google.charts.setOnLoadCallback(drawNewVelocityChart(vCGraphData));
      }
    });
    $("#maximize3").click(function (e) {
    	e.preventDefault();
    	
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
        	isCard3Maximize = true;
        	$("#thirdCard").removeClass('responsiveCard');
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');   
            /*document.getElementById("graph").classList.add("responsiveGraphs");*/
            /*$("#tblDre").removeClass('fixTable');
            $(".zui-sticky-col,.tableh").removeClass('zui-sticky-col tableh');*/
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
        	isCard3Maximize = false;
        	$("#thirdCard").addClass('responsiveCard');
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
            
            /*document.getElementById("graph").classList.remove("responsiveGraphs");
            document.getElementById("graph").classList.add("responsiveGraphs1");*/
            
            /*$("#tblDre").addClass('fixTable');
            $("#tblDre tr th:nth-child(1)").addClass('zui-sticky-col tableh');
            $("#tblDre tr td:nth-child(1)").addClass('zui-sticky-col');*/
        }
        $(this).closest('.card').toggleClass('maximize');

        $("#tblDre tr th").css('min-height','0px !important');
        var maxHeight = Math.max.apply(null, $("#tblDre tr th").map(function ()
   				{
   				    return $(this).height();
   				}).get());
   		$("#tblData tr th").css('min-height',maxHeight + 'px !important');
   		
   		$("#tblData tr th").css('min-height','0px !important');
        var height = Math.max.apply(null, $("#tblData tr th").map(function ()
   				{
   				    return $(this).height();
   				}).get());
   		
   		$("#tblData tr th").css('min-height',height + 'px !important');
   	 if(isRemovalGActive == true && $("#dre").hasClass("active")){
   		google.charts.setOnLoadCallback(drawDREChart(DREdata.text()));
   }else if(isReviewGActive == true && $("#rEff").hasClass("active")){
	   google.charts.setOnLoadCallback(drawLineChart(reviewData.text()));  
   }else if(isDistributionGActive == true && $("#defects").hasClass("active")){
	   google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));  
   }else if(isSRFGActive == true && $("#srf").hasClass("active")){
	   google.charts.setOnLoadCallback(drawSRFGraph(SRFGraph.text())); 
 }	
    });
    $("#maximize4").click(function (e) {
    	e.preventDefault();
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
        	/*document.getElementById("cidiContainer").classList.remove("minTable");
            document.getElementById("cidiContainer").classList.add("maxTable");*/
        	$("#fourthCard").removeClass('responsiveCard');
        	$this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
        	/*document.getElementById("cidiContainer").classList.remove("maxTable");
            document.getElementById("cidiContainer").classList.add("minTable");*/
        	$("#fourthCard").addClass('responsiveCard');
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
    });
});
//ajax for target
function setTargetData(){  	
	  $.ajax({
          url: "getTargetData",
          beforeSend: function (request) {
              var token = $('meta[name="token"]').attr("content");
              request.setRequestHeader("Authorization", "JWT " + token);
          },success: function (data) {
        	  
        	  atpURL = $($.parseHTML(data)).filter("#atpURL").text();
              clientURL = $($.parseHTML(data)).filter("#clientURL").text();
              configurationURL = $($.parseHTML(data)).filter("#configurationURL").text();
              networkURL = $($.parseHTML(data)).filter("#networkURL").text();
              teURL = $($.parseHTML(data)).filter("#teURL").text();
              processURL = $($.parseHTML(data)).filter("#processURL").text();
              retroURL = $($.parseHTML(data)).filter("#retroURL").text();
              momURL = $($.parseHTML(data)).filter("#momURL").text();
             /* getRiskData()*/
        	  getCSATData();
        	   getMetricsData();
        	   getRequirementsData();
        	   getCIDIData();
        },
        error: function (textStatus, errorThrown) {
        	$("#csat_div").html("Something went wrong, please try again after sometime or contact your administrator");
        	$("#cTable").html("Something went wrong, please try again after sometime or contact your administrator");
        }
        }); 
	   
  }
//ajax for CSAT data
function getCSATData(){  	
	$("#cTable").html(cData);
  	  $.ajax({
            url: "clientSatisfactionControl",
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            },
            success: function (data) {
              	cGraph =  $($.parseHTML(data)).filter("#CSAT"); 
              	csatTarget =  $($.parseHTML(data)).filter("#CSATtarget").text();
              	cData =  $($.parseHTML(data)).filter("#table");
              	note =   $($.parseHTML(data)).filter("#note").text();
              	if($("#feedback").hasClass('active')){
              	google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
              	$("#cTable").html(cData);
              	}
            },
            error: function (textStatus, errorThrown) {
            	cData = "Something went wrong, please try again after sometime or contact your administrator";
            }
          });
    }

function showNote(flag){
}

//load CSAT Data
$("#feedback").click(function(){
	if(isCGraphActive){
		showGraphs("csat_div","cTable");
	}else{
		showData("csat_div","cTable");
	}
    buttonAble("one","two");
google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
$("#cTable").html(cData);
    });
//Ajax for Appreciation Data
function getAppreciationData(){
  	$.ajax({
          url: "clientAppreciation",
          beforeSend: function (request) {
              var token = $('meta[name="token"]').attr("content");
              request.setRequestHeader("Authorization", "JWT " + token);
          },
          success: function (data) {
        	  if($("#appreciation").hasClass('active')){
        		  $("#cTable").html(data);
        	  }
        	  appData = data;
          },
          error: function (textStatus, errorThrown) {
          	appData = "Something went wrong, please try again after sometime or contact your administrator";
          }
        });
  }

//load Appreciation Data
  $("#appreciation").click(function(){
	  buttonDisable("one","two");
	  showData("csat_div", "cTable");
	 loadData("cTable");
     $('#cTable').html(appData);
     /*isCGraphActive = false; */
  });
  
//Ajax for Complaint Data
function getComplaintData(){
	  	$.ajax({
      url: "clientComplaint",
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      },
      success: function (data) {
    	  if($("#complaint").hasClass('active')){
    		  $("#cTable").html(data);
    	  }
    	  complaintData = data;
      },
      error: function (textStatus, errorThrown) {
      	complaintData = "Something went wrong, please try again after sometime or contact your administrator";
      }
    });
}
//load Complaint Data
$("#complaint").click(function(){
		buttonDisable("one","two");
  	    showData("csat_div", "cTable");
    	$('#cTable').html(complaintData);
    	/*isCGraphActive = false;*/
}); 
//Ajax to get Aspect Data
function getAspectData(){
$.ajax({
      url: "cSFAControlMethod",
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      },
      success: function (data) {
    	  if($("#aspect").hasClass('active')){
    		  $("#cTable").html(data);
    	  }
    	  aspectData = data;
      },
      error: function (textStatus, errorThrown) {
    	  aspectData = "Something went wrong, please try again after sometime or contact your administrator";
      }
    });
}
//load Aspect Data
$("#aspect").click(function(){
  buttonDisable("one","two");
  showData("csat_div", "cTable");
  $('#cTable').html(aspectData);
  /*isCGraphActive = false;*/
}); 


//ajax for Risk Data
function getRiskData(){
  	  $.ajax({
            url: "deliveryRiskControl",
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            },
            success: function (data) {
            	rgraph =  $($.parseHTML(data)).filter("#graphData"); 
              	rtable =  $($.parseHTML(data)).filter("#tableData");
              	riskTarget =  $($.parseHTML(data)).filter("#riskTarget").text();
              	if($("#risks").hasClass("active")){
              		$("#riskTable").html(rtable);
              		google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
              	}
            },
            error: function (textStatus, errorThrown) {
          	 rtable = "Something went wrong, please try again after sometime or contact your administrator";
          	 rgraph = "Something went wrong, please try again after sometime or contact your administrator";
            }
          });   
    }
$("#risks").click(function(){
	buttonAble("five","six");
	if(isRGraphActive){
		showGraphs("riskrating_div","riskTable");
	}else{
		showData("riskrating_div","riskTable");
	}
	if(rtable != img){
	google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
	$("#riskTable").html(rtable);
}else{
	$("#riskTable").html(rtable);
}
})
//ajax for Velocity Chart
function getVelocityData(){
  	  $.ajax({
            url: "velocityProjectList",
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            }, success : function(data){
            	if( $("#velocityChart").hasClass("active")){
            		$("#riskTable").html(data);
            	}
            	velocityChart = data;
            }, error : function(textStatus, errorsThrown){
            	velocityChart = "Trouble getting the data! Please try again after sometime";
            	$("#riskTable").html(velocityChart);
            }
          })
    }
$("#velocityChart"). click(function(){
	buttonDisable("five","six");
	showData('riskrating_div', 'riskTable');
    $("#riskTable").html(velocityChart);
})

//ajax for Cycle Time & Effort
function getCycleTime(){
  	  $.ajax({
            url: "meanCycleTime",
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            }, success  : function(data){
            	if($("#cycletime").hasClass("active")){
            		$("#riskTable").html(data);
            	}
            	cycletime = data;
            	
            },error: function(errorThrown, textStatus){
          cycletime = "Trouble getting the data! please try again after sometime";
          if(("#cycletime").hasClass('active')){
          $("#riskTable").html(cycletime);
          }
            }
          });   
    }


$("#cycletime"). click(function(){
	buttonDisable("five","six");
	showData('riskrating_div', 'riskTable');
    $("#riskTable").html(cycletime);
})
//Ajax for DRE Data
function getMetricsData(){
    $.ajax({
        url: "monthlyMetricsData",
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }
      }).done(function(data) {
        DREdata =  $($.parseHTML(data)).filter("#DRE"); 
        defectsData = $($.parseHTML(data)).filter("#defectsBar");
        reviewData = $($.parseHTML(data)).filter("#LineEff");
        SRFData = $($.parseHTML(data)).filter("#SRF");
        dreTarget =  $($.parseHTML(data)).filter("#dreTarget").text();
        revTarget =  $($.parseHTML(data)).filter("#revTarget").text();
        drTable= $($.parseHTML(data)).filter(".zui-wrapper");
        drTable2 = $($.parseHTML(data)).filter(".zui-wrapper");
        radcTable = $($.parseHTML(data)).filter(".adcTable");
        defectsTable =  $($.parseHTML(data)).filter("#tableOne");
        defectsTable2 = $($.parseHTML(data)).filter("#tableOne");
        buttonData = $($.parseHTML(data)).filter("#buttonData");
 	   $("#buttonData").html(buttonData);
        if($("#defects").hasClass("active")){
 		  loadData("graph");
 		  $("#defects").trigger("click");
 	   }else if($("#rEff").hasClass("active")){
 		  loadData("graph");
 		   $("#rEff").trigger("click");
 	   }else if($("#dre").hasClass("active")){
 		  loadData("graph");
 		   $("#dre").trigger("click");
 	   }
      });
}
//load DRE Data
$("#dre").click(function(){
	isRemovalGActive = true;
	buttonAble("three","four");
	$("#buttonData").hide();
	google.charts.setOnLoadCallback(drawDREChart(DREdata.text()));
	$("#dtable").html(drTable);
	});
//get requirement fulfillment data
function getRequirementsData(){
    $.ajax({
        url: "iReport",
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }, success: function(data){
        	if($("#reqFulfillment").hasClass('active')){
      		  $("#riskTable").html(data);
      	  }
        	requirementData = data;
        },error : function(textStatus, errorsThrown){
        	requirementData = "Trouble getting data! please try again after sometime or contact the system administrator!"
        		$("#riskTable").html(requirementData);
        }

      });
}
//load requirement fulfillment data
$("#reqFulfillment").click(function(){
	buttonDisable("five","six");
    showData('riskrating_div', 'riskTable');    
    $("#riskTable").html(requirementData);
    });
//load Defect Distribution Data
$("#defects").click(function(){
		isDistributionGActive = true;
	   buttonAble("three","four");
	   $("#buttonData").show();
	   $("#dtable").html(defectsTable);
	google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));
	});
//load Review Efficiency Data
	$("#rEff").click(function(){
		isReviewGActive = true;
	buttonAble("three","four");
	$("#buttonData").hide();
	$("#dtable").html(drTable);
	google.charts.setOnLoadCallback(drawLineChart(reviewData.text())); 
	});
	
	
	$("#rEffAdc").click(function(){
		  buttonDisable("three","four");
		  $("#buttonData").hide();
			  showData('graph', 'dtable');
				$("#dtable").html(radcTable);
		  });
//Ajax for SRF Data
function getSRFData(){
	$.ajax({
	  url: "tests",
	  beforeSend: function (request) {
	      var token = $('meta[name="token"]').attr("content");
	      request.setRequestHeader("Authorization", "JWT " + token);
	  }, success : function(data){
	SRFGraph =  $($.parseHTML(data)).find("#graphData"); 
	SRFGraph2 =  $($.parseHTML(data)).find("#graphData"); 
	srfTarget =  $($.parseHTML(data)).find("#srfTarget").text();
	SRFTable =  $($.parseHTML(data)).find(".zui-wrapper");
	/* SRFTable2 = $($.parseHTML(data)).find(".zui-wrapper");*/
	 ftrTable = $($.parseHTML(data)).find(".ftrTble");
	 ftrTable2 = $($.parseHTML(data)).find(".ftrTble");
	  if($("#srf").hasClass("active")){
		  loadData("graph");
		  $("#srf").trigger("click");
	   }else if($("#testing").hasClass("active")){
 		  $("#testing").trigger("click");
 	   }
	  
	  }, error : function(textStatus, errorThrown){
		  SRFGraph = img;
		  SRFTable = "Trobule loading data. Please try again after sometime";
	  }
    });
}
//load Testing Data
$("#testing").click(function(){
	  buttonDisable("three","four");
	  $("#buttonData").hide();
		  showData('graph', 'dtable');
			$("#dtable").html(SRFTable);
	  });
//load SRF Data

   $("#srf").click(function(){
	   isSRFGActive = true;
  buttonAble("three","four");
  $("#buttonData").hide();
  $("#buttonData").hide();
  $("#dtable").html(ftrTable);
		google.charts.setOnLoadCallback(drawSRFGraph(SRFGraph.text())); 
  });
//Ajax for About the project Data
function getAtpData(){
    $.ajax({
        url: "aboutProjectInfo",
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }
      }).done(function(data) {
        loadData("processData");
        $("#processData").html(data); 
      });
}
//Ajax for CIDI 
function getCIDIData(){
	$('#processData').html(cidiData);
  $.ajax({
      url: "cidiControl",
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      }
    }).done(function(data) {
    	$('#processData').html(data);
    	cidiData = data;
    	/*loadData("processData");*/
        
       getSRFData();
  	   getAppreciationData();
  	   getComplaintData();
  	   getAspectData();
  	 getDynamicMethodData();
  	   /*getVelocityData();*/
  	   getRiskData();
    });
}
//load CIDI data
$("#cidi").click(function(){
	$('#processData').html(cidiData);
})
//code for loader
function loadGif(){ 
$("#csat_div, #riskTable, #riskrating_div, #graph, #processData").html(img);
}
//Code to reset div tag
function loadData(a){
document.getElementById(a).innerHTML = "";
}

//hide and show data
function hideAndShowData(a,b,c,d){
		  document.getElementById(a).style.display = 'none';
		  document.getElementById(b).style.display = 'block';
		  switch(c){
		  case 'client':
			  isCGraphActive = d;
			  if(isCGraphActive){
					google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
				}
			  break;
		  case 'risk':
			  isRGraphActive = d;
			  if(isRGraphActive){
				  google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
			  }
			  break;
		  case 'velocity':
			  isVGraphActive = d;
			  if(isVGraphActive){
				  google.charts.setOnLoadCallback(drawNewVelocityChart(vCGraphData));
			  }
			  break;
		  case 'distribution':
			  isDistributionGActive = d;
			  if(isDistributionGActive){
				  google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));
			  }
			  break;
		  case 'review':
			  isReviewGActive = d;
			  if(isReviewGActive){
				  google.charts.setOnLoadCallback(drawLineChart(reviewData.text()));  
			  }
			  break;
		  case 'dre':
			  isRemovalGActive = d;
			  if(isRemovalGActive){
				  google.charts.setOnLoadCallback(drawDREChart(DREdata.text())); 
			  }
			  break;
		  case 'srf':
			  isSRFGActive = d;
			  if(isSRFGActive){
				  google.charts.setOnLoadCallback(drawSRFGraph(SRFGraph.text())); 
			  }
			  break;
}
}
function buildMethod(a,b,c){
	isGraphFormat = c;
	if($("#defects").hasClass("active")){
		
		hideAndShowData(a,b,'distribution',c);
	}else if($("#rEff").hasClass("active")){
		
		hideAndShowData(a,b,'review',c);
	}else if($("#dre").hasClass("active")){
		
		hideAndShowData(a,b,'dre',c)
	}else if($("#srf").hasClass("active")){
		
		hideAndShowData(a,b,'srf',c);
	}
}

function togglingRiskAndVelocity(a,b,c){
	if($("#dynamicTab").hasClass("active")){
		hideAndShowData(a,b,'velocity',c);
	}else if($("#risks").hasClass("active")){
		hideAndShowData(a,b,'risk',c);
	}
}


$(window).resize(function(){
	if(isCGraphActive && cGraph != img){
		google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
	}
	if(isRGraphActive){
		google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
	}
	if(isDistributionGActive && defectsData != img){
		google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));
	}
	if(isReviewGActive){
		google.charts.setOnLoadCallback(drawLineChart(reviewData.text()));
	}
	if(isRemovalGActive){
		google.charts.setOnLoadCallback(drawDREChart(DREdata.text()));
	}
	if(isSRFGActive){
		google.charts.setOnLoadCallback(drawSRFGraph(SRFGraph.text()));
	}
	if(isVGraphActive){
		google.charts.setOnLoadCallback(drawNewVelocityChart(vCGraphData));
	}
});


function getDynamicMethodData(){
	if(dynamicMethod.includes("velocity")){
		 buttonAble("five","six");
	}
	  $.ajax({
          url: dynamicMethod,
          beforeSend: function (request) {
              var token = $('meta[name="token"]').attr("content");
              request.setRequestHeader("Authorization", "JWT " + token);
          }, success : function(data){
          		if(dynamicMethod.includes("velocity")){
          			vCGraphData = $($.parseHTML(data)).filter("#newVelocityChartData").text();
          			vCTableData = $($.parseHTML(data)).filter("#velocityChartTable");
          			vCTableData2 = $($.parseHTML(data)).filter("#velocityChartTable");
          			if($("#dynamicTab").hasClass("active")){
              			$("#riskTable").html(vCTableData);
              			google.charts.setOnLoadCallback(drawNewVelocityChart(vCGraphData));
              	}
          		}else{
          			dynamicData = data; 
          			if($("#dynamicTab").hasClass("active")){
              			$("#riskTable").html(dynamicData);
              	}
          		}
 
          }, error : function(textStatus, errorsThrown){
          	dynamicData = "Trouble getting the data! Please try again after sometime";
          	$("#riskTable").html(dynamicData);
          }
        })
  }
$("#dynamicTab").click(function(){
	if(dynamicMethod.includes("velocity")){
	buttonAble("five","six");
	if(isVGraphActive){
		showGraphs("riskrating_div","riskTable");
	}else{
		showData("riskrating_div","riskTable");
	}
	if(vCTableData != img){
		google.charts.setOnLoadCallback(drawNewVelocityChart(vCGraphData));
		$("#riskTable").html(vCTableData);
}else{
	$("#riskTable").html(rtable);
}
	}
	else{
		buttonDisable("five","six");
	    showData('riskrating_div', 'riskTable');    
	    $("#riskTable").html(dynamicData);
	}
});
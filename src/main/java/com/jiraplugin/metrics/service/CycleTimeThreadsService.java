package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.MeanCycleTimeData;


@Service
public class CycleTimeThreadsService {
	@Autowired
	CycleTimeDatesService cycleTimeDatesService;
	public ArrayList<ArrayList<MeanCycleTimeData>> createCycleTimeThreads(String project,String sDate, String eDate,AtlassianHostUser hostUser,ArrayList<String> dates,HttpSession session) throws Exception {
		ArrayList<ArrayList<MeanCycleTimeData>> meanCycleTimeData = new ArrayList<>();
		
		
		try {
			CompletableFuture<Map<String, ArrayList<String>>> t1 = cycleTimeDatesService.getCycleTimeDates(project, "High", sDate, eDate, hostUser, dates,session);	
			
			CompletableFuture<Map<String, ArrayList<String>>> t2 = cycleTimeDatesService.getCycleTimeDates(project, "Medium", sDate, eDate, hostUser, dates,session);
			
			CompletableFuture<Map<String, ArrayList<String>>> t3 = cycleTimeDatesService.getCycleTimeDates(project, "Low", sDate, eDate, hostUser, dates,session);
			
			CompletableFuture.allOf(t1,t2,t3).join();
			
			for(String d: dates) {
				
				ArrayList<MeanCycleTimeData> meanCycleTimeArray = new ArrayList<>();
				MeanCycleTimeData meanCycleTime = new MeanCycleTimeData();
			//	MeanCycleTimeData meanEffort = new MeanCycleTimeData();
				
				
				meanCycleTime.setMonth(d);
				meanCycleTime.setMinHighTime(t1.get().get(d).get(0));
				meanCycleTime.setMaxHighTime(t1.get().get(d).get(1));
				meanCycleTime.setAvgHighTime(t1.get().get(d).get(2));
				
				meanCycleTime.setMinMedTime(t2.get().get(d).get(0));
				meanCycleTime.setMaxMedTime(t2.get().get(d).get(1));
				meanCycleTime.setAvgMedTime(t2.get().get(d).get(2));
				
				meanCycleTime.setMinLowTime(t3.get().get(d).get(0));
				meanCycleTime.setMaxLowTime(t3.get().get(d).get(1));
				meanCycleTime.setAvgLowTime(t3.get().get(d).get(2));
				
				/*meanEffort.setMonth(d);
				meanEffort.setMinHighTime(t1.get().get(d).get(3));
				meanEffort.setMaxHighTime(t1.get().get(d).get(4));
				meanEffort.setAvgHighTime(t1.get().get(d).get(5));
				
				meanEffort.setMinMedTime(t2.get().get(d).get(3));
				meanEffort.setMaxMedTime(t2.get().get(d).get(4));
				meanEffort.setAvgMedTime(t2.get().get(d).get(5));
				
				meanEffort.setMinLowTime(t3.get().get(d).get(3));
				meanEffort.setMaxLowTime(t3.get().get(d).get(4));
				meanEffort.setAvgLowTime(t3.get().get(d).get(5));*/
				
				meanCycleTimeArray.add(meanCycleTime);
			//	meanCycleTimeArray.add(meanEffort);
				
				meanCycleTimeData.add(meanCycleTimeArray);
				
			}
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		return meanCycleTimeData;
	}
}
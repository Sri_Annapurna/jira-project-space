package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskRangeData;
import com.jiraplugin.metrics.model.RiskRating;

@Service
@Primary
public class RiskService {

	private static final String ISSUES = "issues";

	@Autowired
	private AtlassianHostRestClients restClients;
	
	@Value("${riskAssessmentValue}")
	String riskassess;
	
	@Value("${riskIdentifiedValue}")
	String sta;
	
	@Value("${riskTreatmentAction}")
	String inp;
	
	Logger logger = LoggerFactory.getLogger(RiskService.class);

	public JSONArray riskOpenIssues(String pro) {

		String urlString = "/rest/api/2/search?jql=project={pro} AND issuetype={riskassess} AND (status= {sta} OR status= {inp}) &startAt={startAt}&maxResults=100";

		JSONArray jsonArray = new JSONArray();
		int startAt = 0;
		int total = 1;
		while (total > startAt) {
			String response = restClients.authenticatedAsHostActor().getForObject(
					UriComponentsBuilder.fromUriString(urlString).buildAndExpand("\"" + pro + "\"",
							"\"" + riskassess + "\"", "\"" + sta + "\"", "\"" + inp + "\"", startAt).toUri(),
					String.class);

			JSONObject jsonObj1 = new JSONObject(response);
			total = jsonObj1.getInt("total");
			JSONArray jsonArray1 = jsonObj1.getJSONArray(ISSUES);
			int length = jsonArray1.length();
			for (int j = 0; j < length; j++) {
				JSONObject objects = jsonArray1.getJSONObject(j);
				jsonArray.put(objects);
			}
			startAt = startAt + length;
		}

		return jsonArray;
	}

	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	

	
	@Value("${riskAssessmentValue}")
	String riskAssessmentType;
	
	
	@Async
	public CompletableFuture<List<ResultRangeData>> getProjectRangeData(String project, AtlassianHostUser hostUser,
			HttpSession session) throws java.text.ParseException {
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		String url ="/rest/api/2/search?jql=project={project} AND issuetype='" + riskAssessmentType + "' AND 'Actual Date' != null AND 'Reported Date'>=startOfMonth(-4)&'Reported Date'!=startOfMonth(-1)&startAt=0&maxResults=100";
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url)
				.buildAndExpand("\"" + project + "\"").toUri(), String.class);
		JSONObject jsonObj = new JSONObject(response);
		JSONArray jsonArray = jsonObj.getJSONArray(ISSUES);
		int count = jsonObj.getInt("total");
		int maxResults = jsonObj.getInt("maxResults");
		if (count > maxResults) {
			url ="/rest/api/2/search?jql=project={project} AND issuetype= '" + riskAssessmentType +"' AND 'Actual Date' != null AND 'Reported Date'>=startOfMonth(-4)&'Reported Date'!=startOfMonth(-1)&startAt=100&maxResults=100";
			String response1 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url)
					.buildAndExpand("\"" + project + "\"").toUri(), String.class);
			parseArray(jsonArray, response1);
		}
		
		
		ArrayList<RiskRangeData> riskData = new ArrayList<>();
		RiskRating fielData = new RiskRating();
		
		
		parseAvgRiskValue(fieldList, jsonArray, riskData, fielData);
		
		
		List<ResultRangeData> result;
		result = calAverage(fielData);
		return CompletableFuture.completedFuture(result);

	}

	public void parseAvgRiskValue(Map<String, String> fieldList, JSONArray jsonArray,
			List<RiskRangeData> riskData, RiskRating fielData) throws ParseException {
		for (int i = 0; i < jsonArray.length(); i++) {
			RiskRangeData data = new RiskRangeData();
			JSONObject issuesData = jsonArray.getJSONObject(i);
			JSONObject fieldsData = issuesData.getJSONObject("fields");
			Date monthandyear = new SimpleDateFormat("yyyy-MM")
					.parse(fieldsData.getString(fieldList.get("Reported Date")));
			String month = new SimpleDateFormat("MMM-yy").format(monthandyear);
			data.setCreated(month);
			int riskValue = 0;
			int residualValue = 0;
			try {
				riskValue = fieldsData.getInt(fieldList.get("Risk Value"));
				data.setRiskValue(riskValue);
			} catch (Exception e) {
				data.setRiskValue(0);
			}
			try {
				residualValue = fieldsData.getInt(fieldList.get("Residual Risk Value"));
				data.setResidualRiskValue(residualValue);
			} catch (Exception e) {
				data.setResidualRiskValue(0);
			}
			riskData.add(data);
		}

		fielData.setFields(riskData);
	}

	public void parseArray(JSONArray jsonArray, String response1) {
		JSONObject jsonObj1 = new JSONObject(response1);
		JSONArray jsonArray1 = jsonObj1.getJSONArray(ISSUES);
		for (int i = 0; i < jsonArray1.length(); i++) {
			JSONObject objects = jsonArray1.getJSONObject(i);
			jsonArray.put(objects);
		}
	}

	public List<ResultRangeData> calAverage(RiskRating data) {
		ArrayList<ResultRangeData> result = new ArrayList<>();
		String[] months = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {
			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;
		}
		String month;
		List<RiskRangeData> fieldsData;
		fieldsData = data.getFields();

		for (int j = 0; j < 4; j++) {
			
			month = months[j];
			
			ResultRangeData avgresult = getAveragesRiskValue(fieldsData, month);

			result.add(avgresult);
		}
		return result;
	}
	
	public ResultRangeData getAveragesRiskValue(List<RiskRangeData> fieldsData, String month) {
		
		float riskSum = 0;
		float residualSum = 0;
		float count = 0;
		float avgRisk = 0;
		float avgResidual = 0;
		
		ResultRangeData avgresult = new ResultRangeData();
		
		for (RiskRangeData i : fieldsData) {
			if (month.equals(i.getCreated())) {
				residualSum += i.getResidualRiskValue();
				riskSum += i.getRiskValue();
				count += 1;
			}
		}
		if (count > 0) {
			avgRisk = riskSum / count;
			avgResidual = residualSum / count;
		} else {
			avgRisk = 0;
			avgResidual = 0;
		}
		avgresult.setMonth(month);
		avgresult.setAvg_risk(riskRating(Math.round(avgRisk)));
		avgresult.setAvg_residual(riskRating(Math.round(avgResidual)));
		avgresult.setAvg_residual_value(bucketValueRange(Math.round(avgResidual)));
		avgresult.setAvg_risk_value(bucketValueRange(Math.round(avgRisk)));
		
		
		return avgresult;
	}
	
	public int riskRating(int value) {
		int rating = 0;
		if ((value < 1) || (value > 36)) {
			rating = 0;
		} else if ((value >= 1) && (value <= 4)) {
			rating = 1;
		} else if ((value >= 5) && (value <= 9)) {
			rating = 2;
		} else if ((value >= 10) && (value <= 17)) {
			rating = 3;
		} else if ((value >= 18) && (value <= 28)) {
			rating = 4;
		} else if ((value >= 29) && (value <= 36)) {
			rating = 5;
		}
		return rating;
	}
	public String bucketValueRange(int value) {
		String range = "";
		if ((value < 1) || (value > 36)) {
			range = "-";
		} else if ((value >= 1) && (value <= 4)) {
			range = "1-Low";
		} else if ((value >= 5) && (value <= 9)) {
			range = "2-Medium";
		} else if ((value >= 10) && (value <= 17)) {
			range = "3-High";
		} else if ((value >= 18) && (value <= 28)) {
			range = "4-Very High";
		} else if ((value >= 29) && (value <= 36)) {
			range = "5-Critical";
		}
		return range;
	}

}

package com.jiraplugin.metrics.service;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
@Service
public class IssueService {
	@Value("${individual_issue_date_range_url}")
	String url;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Async
	public CompletableFuture<JSONObject> getResponseData(String p, String type, String sDate, String eDate, AtlassianHostUser hostUser) {
		String url1 ="/rest/api/2/search?jql=project={p} AND  (issuetype={type}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate})";
		JSONObject response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"", "\""+ type + "\"","\""+ sDate + "\"", "\""+ eDate + "\"").toUri(), JSONObject.class);
		return CompletableFuture.completedFuture(response);
	}
}

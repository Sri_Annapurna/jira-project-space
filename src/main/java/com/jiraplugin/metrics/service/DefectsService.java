package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.DefectTypeData;
import com.jiraplugin.metrics.model.EffortData;


@Service
public class DefectsService {
	Logger logger = LoggerFactory.getLogger(DefectsService.class);
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired IssueService is;
	@Value("${review_defect}")  String review_defect_nwf;
	@Value("${testing_defect}")  String testing_defect_nwf;
	@Value("${delivered_defect}")  String delivered_defect;
	@Value("${analysis_design_review_defect}")  String analysis_design_review_defect_nwf;
	@Value("${unit_testing_defect}")  String unit_testing_defect_nwf;
	@Value("${test_case_review_defect}")  String test_case_review_defect_nwf;
	 @Value("${delivery_defect}")
	    String delivery_defect;
	    @Value("${corrective_improvement_actions}")
	    String corrective_improvement_actions;
	    @Value("${reported_date}")
	    String reported_date;
	    @Value("${category_corrective_improvement_actions}")
	    String category_corrective_improvement_actions;
	    @Value("${description}")
	    String issue_description;
	    @Value("${defect_type}")
	    String defect_type;
	    @Value("${priority}")
	    String priority;
	    @Value("${root_cause}")
	    String root_cause;
	    @Value("${action}")
	    String action;
	    @Value("${action_type}")
	    String action_type;
	    @Value("${responsibility}")
	    String responsibility;
	    @Value("${actual_date}")
	    String actual_date;
	    @Value("${target_date}")
	    String target_date;
	int rl = 0,rh = 0,rm = 0,tl = 0,tm = 0,th = 0,dh = 0,dl = 0,dm = 0;
	
	@Autowired JiraCustomFieldMap j;
	
	public CreativeIdea parseCreativeIdea(JSONObject j, Map<String,String> map) throws ParseException {
		
		JSONObject fieldsData = j.getJSONObject("fields");
		CreativeIdea cia = new CreativeIdea();
		JSONObject iss_type = fieldsData.getJSONObject("issuetype");
		String issuetype = iss_type.get("name").toString();
		try {
		String repdate = fieldsData.get(map.get(reported_date)).toString();
		Date rep = new SimpleDateFormat("yyyy-MM-dd").parse(repdate);
		String repdt = new SimpleDateFormat("dd-MMM-yyyy").format(rep);
		cia.setDate(repdt);
		}catch(Exception e) {
			cia.setDate("-");
		}
		cia.setStatus(fieldsData.getJSONObject("status").get("name").toString());
		    cia.setCategory("Delivered Defect");
		try {
			String description = fieldsData.get(map.get(issue_description)).toString();
			if(!description.equals("null") && !description.isEmpty()) {
		    cia.setDescription(description);
			}else {
				cia.setDescription("-");
			}
		}catch(Exception e) {
			cia.setDescription("-");
		}
		try {
		JSONObject prio = fieldsData.getJSONObject(map.get("Complexity"));
		cia.setPriority(prio.getString("value"));
		}catch(Exception e) {
			cia.setPriority("-");
		}
		String ac = fieldsData.get(map.get(action)).toString();
		if(!ac.equals("null") && !ac.isEmpty()) {
		cia.setAction(ac);
		}else{
			cia.setAction("-");
		}
		String rc = fieldsData.get(map.get(root_cause)).toString();
		if(!rc.equals("null") && !rc.isEmpty()) {
			cia.setRoot_cause(rc);
			}else {
				cia.setRoot_cause("-");
			}
		if (issuetype.equals(delivery_defect))
		    cia.setAction_type("Corrective Action");
		else {
		    JSONObject action_type_object = fieldsData.getJSONObject(map.get(action_type));
		    cia.setAction_type(action_type_object.get("value").toString());
		}
		try {
		JSONArray robject = fieldsData.getJSONArray(map.get(responsibility));
		String resp = "";
		boolean flag = false;
		for (int k = 0; k < robject.length(); k++) {
		    if (flag == true) {
			resp += ",";
		    }
		    JSONObject jo = robject.getJSONObject(k);
		    resp += jo.get("displayName").toString();
		    flag = true;
		}
		System.out.println("delivered defects' resposnibility ---->"+ resp);
		cia.setResponsibility(resp);
		}
		catch(Exception e) {
			cia.setResponsibility("-");
		}
		try {
		String tgd = fieldsData.get(map.get(target_date)).toString();
		if (!tgd.equals("null") && !tgd.isEmpty()) {
		Date tdate = new SimpleDateFormat("yyyy-MM-dd").parse(tgd);
		String td = new SimpleDateFormat("dd-MMM-yyyy").format(tdate);
		cia.setTarget_date(td);
		}else {
			cia.setTarget_date("-");
		}
		}catch(Exception e) {
			cia.setTarget_date("-");
		}
		String acd = "";
		try {
		acd = fieldsData.get(map.get(actual_date)).toString();
		if (!acd.equals("null") && !acd.isEmpty()) {
		    Date ad = new SimpleDateFormat("yyyy-MM-dd").parse(acd);
		    String adt = new SimpleDateFormat("dd-MMM-yyyy").format(ad);
		    cia.setActual_date(adt);
		}else {
			cia.setActual_date("-");
		}
		}catch(Exception e) {
			acd = "-";
			cia.setActual_date("-");
		}
	//	String acd = fieldsData.getString(map.get(actual_date));
		     
		// cia.setRemarks(fieldsData.getString(customfields.get("Remarks (Corr./Improv.
		// Actions)")));
		return cia;
	    }
		
	public void testingData(String severity) {
		if(severity.equals("High")) {
			th++;
		}else if(severity.equals("Low")) {
			tl++;
		}else if(severity.equals("Medium")) {
			tm++;
		}
	}
	public void reviewData(String severity) {
		if(severity.equals("High")) {
			rh++;
		}else if(severity.equals("Low")) {
			rl++;
		}else if(severity.equals("Medium")) {
			rm++;
		}
	}
	public void deliveryData(String severity) {
		if(severity.equals("High")) {
			dh++;
		}else if(severity.equals("Low")) {
			dl++;
		}else if(severity.equals("Medium")) {
			dm++;
		}
	}
	
	@Async
	public CompletableFuture<DEffectivenessData> defectsCount(String project, int i,boolean isSprintWise,String sprintCompleteDate,  AtlassianHostUser hostUser,HttpSession session) throws Exception {
		ArrayList<CreativeIdea> ciData = new ArrayList<CreativeIdea>();
		DEffectivenessData data = new DEffectivenessData();
		String url = "";
		//JSONArray response = new JSONArray();
		int startAt = 0, total = 0;
		JSONArray jsonArray = new JSONArray();
		Map<String, String> map = j.getJiraFieldList(hostUser, session);
		
		try {
			if(isSprintWise) {
				
				url = "/rest/api/2/search?jql=Sprint="+ i +" AND (issuetype='"+review_defect_nwf+"' OR issuetype='"+analysis_design_review_defect_nwf+"' OR issuetype='"+unit_testing_defect_nwf+"' OR issuetype='"+test_case_review_defect_nwf+"' OR issuetype='"+testing_defect_nwf+"' OR issuetype='"+delivered_defect+"')&startAt={startAt}&maxResults=100";
				System.out.println("the url for this method is "+ url);
				jsonArray = getResponse(url, hostUser, project, 50, isSprintWise,sprintCompleteDate);
			}else {
				url = "/rest/api/2/search?jql=project='"+ project + "' AND (issuetype='"+review_defect_nwf +"' OR issuetype='"+analysis_design_review_defect_nwf+"' OR issuetype='"+ unit_testing_defect_nwf +"' OR issuetype='"+test_case_review_defect_nwf+"' OR issuetype='"+ testing_defect_nwf+"' OR issuetype='"+ delivered_defect+"') AND 'Reported Date'>=startOfMonth("+ i +") AND 'Reported Date'<=endOfMonth("+i+")&startAt={startAt}&maxResults=100";
				jsonArray = getResponse(url, hostUser, project, 50,isSprintWise,sprintCompleteDate);
			}
		int reviewdefects_count=0;
		int review_defects_adc=0;
		int unit_testing_defects_count=0;
		int total_review_defects_count=0;
		int total_test_design_defects_count=0;
		int testingdefects_count=0;
		int deliverydefects_count = 0;
		int test_case_review_defect_count=0;
		for (int j = 0; j < jsonArray.length(); j++) {
			
			JSONObject issuesData = jsonArray.getJSONObject(j);
			JSONObject fieldsData = issuesData.getJSONObject("fields");
			
			JSONObject issuetype_object=fieldsData.getJSONObject("issuetype");
			String issue_type=issuetype_object.getString("name");
			String type = "";
			try {
			JSONObject severity_object=fieldsData.getJSONObject(map.get("Complexity"));
			type=severity_object.get("value").toString();
			System.out.println(type);
			}catch(Exception e) {
				type = "none";
				System.out.println(type);
			}
			if(issue_type.equals(review_defect_nwf) || issue_type.equals(analysis_design_review_defect_nwf)) {
				reviewdefects_count++;
				reviewData(type);
			}
			else if(issue_type.equals(unit_testing_defect_nwf) ) {
			    unit_testing_defects_count++;
			    testingData(type);
			}
			else if(issue_type.equals(test_case_review_defect_nwf) ) {
				test_case_review_defect_count++;
				reviewData(type);
			}
			else if(issue_type.equals(testing_defect_nwf)) {
				testingdefects_count++;
				testingData(type);
			}
			else if(issue_type.equals(delivered_defect)) {
			deliverydefects_count++;
			ciData.add(parseCreativeIdea(issuesData,map));
			deliveryData(type);
			}
		}
		total_review_defects_count=reviewdefects_count+test_case_review_defect_count;
		review_defects_adc=reviewdefects_count;
		total_test_design_defects_count=unit_testing_defects_count+test_case_review_defect_count;
		data.setDesign_count(total_test_design_defects_count);
		data.setdHCount(dh);
		data.setdLCount(dl);
		data.setdMCount(dm);
		data.setdTCount(deliverydefects_count);
		data.setiHCount(rh + th);
		data.setiLCount(rl + tl);
		data.setiMCount(rm + tm);
		data.setiTCount(total_review_defects_count + testingdefects_count+unit_testing_defects_count);
		data.setRd_adc_count(review_defects_adc);
		data.setrHCount(rh);
		data.setrLCount(rl);
		data.setrMCount(rm);
		data.setrTCount(total_review_defects_count);
		data.settHCount(th);
		data.settLCount(tl);
		data.settMCount(tm);
		data.settTCount(testingdefects_count + +unit_testing_defects_count);
		data.setDeliveryCausalData(ciData);
		data.setUnitTestingDefects(unit_testing_defects_count);
		rl = 0;
		rh = 0;
		rm = 0;
		tl = 0;
		tm = 0;
		th = 0;
		dh = 0;
		dl = 0;
		dm = 0;
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return CompletableFuture.completedFuture(data);
	}
	
	
	public JSONArray getResponse(String url,AtlassianHostUser hostUser, String project, int maxResults, boolean isSprintWise,String sprintCompleteDate) throws ParseException {
		System.out.println("thread for project "+ project);
		int startAt = 0;
		String response = "";
		JSONArray jsonArray = new JSONArray();
		int total = 0;
		do{
			response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand(startAt).toUri(),String.class);
			JSONObject jsonObj = new JSONObject(response);
					startAt =  startAt + maxResults;
					total = Integer.parseInt(jsonObj.get("total").toString());
					JSONArray jsonArray1 = jsonObj.getJSONArray("issues");
					/*for (int j = 0; j < jsonArray1.length(); j++) {
						JSONObject objects = jsonArray1.getJSONObject(j);
						jsonArray.put(objects);
					}*/if(isSprintWise) {
						SimpleDateFormat closedDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				        Date sprintClosedDate = closedDateFormat.parse(sprintCompleteDate);
						jsonArray = appendingJSONArraysByComparingDates(jsonArray, jsonArray1, jsonArray1.length(),sprintClosedDate);
					}else {
						jsonArray = appendingJSONArrays(jsonArray, jsonArray1, jsonArray1.length());
					}
					}while(startAt < total);
		return jsonArray;
	}
	
	public JSONArray appendingJSONArrays(JSONArray finalArray, JSONArray intermediateArray, int length) {
		for (int j = 0; j < length; j++) {
			JSONObject object = intermediateArray.getJSONObject(j);
			finalArray.put(object);
		}
		
		return finalArray;
	}

	
	public JSONArray appendingJSONArraysByComparingDates(JSONArray finalArray, JSONArray intermediateArray, int length, Date sprintCompleteDate) throws ParseException {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",Locale.FRANCE);
		SimpleDateFormat dateIncludingSeconds = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");	
		TimeZone utc = TimeZone.getTimeZone("IST");
		f.setTimeZone(utc);
		GregorianCalendar cal = new GregorianCalendar(utc);
		for (int j = 0; j < length; j++) {
			JSONObject object = intermediateArray.getJSONObject(j);
			JSONObject fields = object.getJSONObject("fields");
			String issueCreatedDate = fields.get("created").toString();
			System.out.println("The created date of current issue"+sprintCompleteDate);
			System.out.println("The created date of current issue"+issueCreatedDate);
			cal.setTime(f.parse(issueCreatedDate));
			String iCreatedDate = dateIncludingSeconds.format(cal.getTime());
			Date finalCreatedDate = dateIncludingSeconds.parse(iCreatedDate);
			System.out.println(dateIncludingSeconds.parse(iCreatedDate));
			if(finalCreatedDate.compareTo(sprintCompleteDate) <= 0) {
				System.out.println("I am in sprint");
			finalArray.put(object);
			}
		}
		return finalArray;
	}
	
	
	
	@Async
	public CompletableFuture<LinkedHashMap<String, DefectTypeData>> getDefectsData(String p, String type, String sDate, String eDate, List<String> dates, AtlassianHostUser hostUser,HttpSession session) throws InterruptedException, Throwable {
		
		String url1 = "/rest/api/2/search?jql=project={p} AND  (issuetype={type}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate})";
		String response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"","\"" + type + "\"", "\"" + sDate + "\"","\"" + eDate + "\"").toUri(), String.class);
		JSONObject response1 = new JSONObject(response);
		LinkedHashMap<String,DefectTypeData> defectData = new LinkedHashMap<String, DefectTypeData>();
		LinkedHashMap<String, ArrayList<CreativeIdea>> ciData = new LinkedHashMap<String, ArrayList<CreativeIdea>>();
		Map<String, String> map = j.getJiraFieldList(hostUser, session);
		 for(String d: dates) {
			 ArrayList<CreativeIdea> cd = new ArrayList<CreativeIdea>();
			 DefectTypeData dtd= new DefectTypeData(); 
			 dtd.sethCount(0);
			 dtd.setlCount(0);
			 dtd.setmCount(0);
			 dtd.settCount(0);
			 defectData.put(d, dtd);
			 ciData.put(d,cd);
		 }
		 
		 JSONArray issuesArray = response1.getJSONArray("issues");
		 int length = issuesArray.length();
		 for(int i = 0; i<length; i++) {
			 CreativeIdea cd = new CreativeIdea();
			 JSONObject jobj = issuesArray.getJSONObject(i);
			 if(type.equals("Delivered Defect")){
				  cd = parseCreativeIdea(jobj,map);
				  SimpleDateFormat formatterc = new SimpleDateFormat("dd-MMM-yyyy"); 
					 Date repordate = formatterc.parse(cd.getDate());
				  String key = new SimpleDateFormat("MMM-yy").format(repordate);
					cd.setKey(key);
				  ArrayList<CreativeIdea> cdi = ciData.get(cd.getKey());
				  cdi.add(cd);
				  ciData.put(cd.getKey(), cdi);
			 }
			 
			 JSONObject fieldsObj = jobj.getJSONObject("fields");
			 String date = fieldsObj.get(map.get("Reported Date")).toString();
			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
			 Date rdate = formatter.parse(date);
			 String key = new SimpleDateFormat("MMM-yy").format(rdate); 
			 DefectTypeData ed = defectData.get(key);
			 ed.setCiData(ciData.get(key));
			 String complexity = map.get("Complexity").toString();
			 String severity = "";
				try {
				JSONObject severity_object=fieldsObj.getJSONObject(complexity);
				severity=severity_object.get("value").toString();
				System.out.println(type);
				}catch(Exception e) {
					severity = "none";
					System.out.println(type);
				}
				ed.settCount(ed.gettCount() + 1);
			 if(severity.equals("High")) {
					ed.sethCount(ed.gethCount() + 1);
				}else if(severity.equals("Low")) {
					ed.setlCount(ed.getlCount() + 1);
				}else if(severity.equals("Medium")) {
					ed.setmCount(ed.getmCount() + 1);
				}
			 defectData.put(key, ed);
		}
		 
		 return CompletableFuture.completedFuture(defectData);
	}
}
package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;

@Service
public class ClientSatisfactionService {

	 private static final String VALUE = "value";
	 
	 Logger logger = LoggerFactory.getLogger(ClientSatisfactionService.class);

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private ClientSatisfactionThread cst;

	@Value("${SnapUserName}")
	String userName;

	@Value("${PassWord}")
	String password;

	@Value("${corrective_improvement_actions}")
	String complaintIssueType;

	@Value("${client_complaints}")
	String category;

	@Value("${PeriodJanS}")
	String janPS;

	@Value("${PeriodJanE}")
	String janPE;

	@Value("${PeriodMayS}")
	String mayPS;

	@Value("${PeriodMayE}")
	String mayPE;

	@Value("${PeriodSepS}")
	String sepPS;

	@Value("${PeriodSepE}")
	String sepPE;
	
	@Value("${PeriodNavS}")
	String navPS;

	@Value("${PeriodNavE}")
	String navPE;

	public Map<String, ArrayList<ComplaintData>> getComplaint(AtlassianHostUser hostUser, String selectedProject, HttpSession session)
			throws Exception {

		Map<String, ArrayList<ComplaintData>> resultArray = new LinkedHashMap<>();

		ArrayList<ArrayList<String>> callData = getFrontBackNumber();

		CompletableFuture<ArrayList<ComplaintData>> month0 = cst.findCount(hostUser, selectedProject,
				callData.get(0).get(0), callData.get(0).get(1), session);

		CompletableFuture<ArrayList<ComplaintData>> month1 = cst.findCount(hostUser, selectedProject,
				callData.get(1).get(0), callData.get(1).get(1), session);

		CompletableFuture<ArrayList<ComplaintData>> month2 = cst.findCount(hostUser, selectedProject,
				callData.get(2).get(0), callData.get(2).get(1), session);

		CompletableFuture<ArrayList<ComplaintData>> month3 = cst.findCount(hostUser, selectedProject,
				callData.get(3).get(0), callData.get(3).get(1), session);

		CompletableFuture.allOf(month0, month1, month2, month3).join();

		ArrayList<ArrayList<ComplaintData>> resultString = new ArrayList<>();
		resultString.add(month0.get());
		resultString.add(month1.get());
		resultString.add(month2.get());
		resultString.add(month3.get());
		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;

	}

	ArrayList<ArrayList<String>> getFrontBackNumber() {

		Calendar cal;

		cal = Calendar.getInstance();

		int yearNumber = cal.get(Calendar.YEAR);

		ArrayList<ArrayList<String>> collectiveData = new ArrayList<>();
		ArrayList<String> resultData;

			
			resultData = new ArrayList<>();

			resultData.add(yearNumber + mayPS);
			resultData.add(yearNumber + mayPE);
			resultData.add("Q1" +" "+yearNumber);
			collectiveData.add(resultData);
			
			resultData = new ArrayList<>();

			resultData.add(yearNumber + sepPS);
			resultData.add(yearNumber + sepPE);
			resultData.add("Q2"+" " + yearNumber);
			collectiveData.add(resultData);
			
			resultData = new ArrayList<>();

			resultData.add(yearNumber + navPS);

			resultData.add(yearNumber + navPE);
			resultData.add("Q3"+" "+ yearNumber);
			collectiveData.add(resultData);

			resultData = new ArrayList<>();

			resultData.add((yearNumber+1) + janPS);
			resultData.add((yearNumber+1) + janPE);
			resultData.add("Q4" + " " + (yearNumber+1));
			collectiveData.add(resultData);


		return collectiveData;
	}

	// Client Appreciations

	public Map<String, ArrayList<AppreciationData>> getAppreciation(AtlassianHostUser hostUser, String selectedProject, HttpSession session)
			throws Exception {
		ArrayList<ArrayList<AppreciationData>> resultString = new ArrayList<>();
		Map<String, ArrayList<AppreciationData>> resultArray = new LinkedHashMap<>();
		ArrayList<ArrayList<String>> callData = getFrontBackNumber();
		try {
		CompletableFuture<ArrayList<AppreciationData>> month0 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(0).get(0), callData.get(0).get(1), session);

		CompletableFuture<ArrayList<AppreciationData>> month1 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(1).get(0), callData.get(1).get(1), session);

		CompletableFuture<ArrayList<AppreciationData>> month2 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(2).get(0), callData.get(2).get(1), session);

		CompletableFuture<ArrayList<AppreciationData>> month3 = cst.findCountOfAppreciation(hostUser, selectedProject,

				callData.get(3).get(0), callData.get(3).get(1), session);

		CompletableFuture.allOf(month0, month1, month2, month3).join();

		

		resultString.add(month0.get());
		resultString.add(month1.get());
		resultString.add(month2.get());
		resultString.add(month3.get());
		}catch(Exception e) {
			logger.error(e.getMessage());

		}
		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;
	}

	@Value("${ClientSatisfactionSingle_IssueType}")
	String clientSatisfactionIT;

	@Value("${Quarter_Start_Date}")
	String qsd;

	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

	ArrayList<ClientSatisfactionData> cSATdata;

	ArrayList<ArrayList<String>> dateYearLink;

	ArrayList<String> dateLink;

	public CompletableFuture<Map<String, ClientSatisfactionData>> findCountClientSatisfactionSingleCall(
			AtlassianHostUser hostUser, String sce, HttpSession session) throws ParseException{

		Calendar cal;

		RestTemplate restTemplate = new RestTemplate();

		cal = Calendar.getInstance();

		int monthCount = cal.get(Calendar.MONTH);

		int yearNumber = cal.get(Calendar.YEAR);


		ArrayList<String> resultData = new ArrayList<>();

		getQuaterWiseData(monthCount, yearNumber, resultData);

		Map<String, ClientSatisfactionData> resultArray = new HashMap<>();

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName + "@senecaglobal.com", password));

		String urlPara = "/rest/api/2/search?jql=project={pro} AND issuetype={csat} AND ({qsd} = 'Q1(Jan-Apr)' OR {qsd} = 'Q2(May-Aug)' OR {qsd} = 'Q3(Sep-Dec)') AND 'Year' ~ {YearNumber} & startAt= {startAtCount} & maxResults=100";


		int initialTotal = 0;
		int count = 1;
		int startAtCount = 0;

		while (initialTotal < count) {

			String response2 = restClients.authenticatedAs(hostUser)
					.getForObject(
							UriComponentsBuilder.fromUriString(urlPara)
									.buildAndExpand("\"" + sce + "\"", "\"" + clientSatisfactionIT + "\"",
											"\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + qsd + "\"",
											"\"" + yearNumber + "\"", "\"" + startAtCount + "\"")
									.toUri(),
							String.class);

			JSONObject response = new JSONObject(response2);
			count = response.getInt("total");
			int issueMaxResult = response.getInt("maxResults");
			parseMethod2(hostUser, response, session);
			initialTotal = initialTotal + issueMaxResult;

		}

		for (int i = 0; i < 4; i++) {

			int flag = 0;
			int lastElement = 0;

			for (int j = 0; j < cSATdata.size(); j++) {
				lastElement++;

				try {

					if (dateYearLink.get(i).get(0).equals(cSATdata.get(j).getQuarter_Start_Date())
							&& dateYearLink.get(i).get(1).equals(cSATdata.get(j).getYear())) {

						flag = 1;

						resultArray.put(resultData.get(i), cSATdata.get(j));

						break;
					}

				} catch (Exception e) {
					logger.error(e.getMessage());

				}

			}
			if (flag == 0) {

				clientSatisfactionData = new ClientSatisfactionData();

				clientSatisfactionData.setCSAT_Q6("-");
				cSATdata.add(clientSatisfactionData);
				resultArray.put(resultData.get(i), cSATdata.get(lastElement));

			}

		}

	

		return CompletableFuture.completedFuture(resultArray);
	}

	private void getQuaterWiseData(int monthCount, int yearNumber, ArrayList<String> resultData) {
		dateYearLink = new ArrayList<>();

		dateLink = new ArrayList<>();

		String janApr = "Jan-Apr(";
		String quaOne = "Q1(Jan-Apr)";
		String mayAug = "May-Aug(";
		String quaTwo = "Q2(May-Aug)";
		String sepDec = "Sep-Dec(";
		String quaThree = "Q3(Sep-Dec)";
		if (monthCount >= 0 && monthCount <= 3) {
			
			resultData.add(janApr + (yearNumber - 1) + ")");
			dateLink.add(quaOne);
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);
			
			resultData.add(mayAug + (yearNumber - 1) + ")");
			dateLink.add(quaTwo);
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);
			
			resultData.add(sepDec + (yearNumber - 1) + ")");
			dateLink.add(quaThree);
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);

			resultData.add(janApr + yearNumber + ")");
			dateLink.add(quaOne);
			dateLink.add(String.valueOf(yearNumber));
			dateYearLink.add(dateLink);

			String[] dateLink1 = { quaOne,quaTwo,quaThree, quaOne};

			getDateYearLink(yearNumber, dateLink1);

		} else if (monthCount >= 4 && monthCount <= 7) {

			resultData.add(mayAug + (yearNumber - 1) + ")");
			resultData.add(sepDec + (yearNumber - 1) + ")");
			resultData.add(janApr + (yearNumber) + ")");
			resultData.add(mayAug + yearNumber + ")");


			String[] dateLink1 = { quaTwo,quaThree,quaOne, quaTwo};

			getDateYearLink(yearNumber, dateLink1);

		} else if (monthCount >= 8 && monthCount <= 11) {

			
			resultData.add(sepDec + (yearNumber - 1) + ")");
			resultData.add(janApr + (yearNumber) + ")");
			resultData.add(mayAug + (yearNumber) + ")");
			resultData.add(sepDec + yearNumber + ")");

			String[] dateLink1 = { quaThree,quaOne,quaTwo, quaThree};

			getDateYearLink(yearNumber, dateLink1);

		}
	}

	private void getDateYearLink(int yearNumber, String[] dateLink1) {
		for (int i = 0; i < 4; i++) {
			dateLink = new ArrayList<>();
			dateLink.add(dateLink1[i]);
			if (i == 3) {
				dateLink.add(String.valueOf(yearNumber - 1));
			} else {
				dateLink.add(String.valueOf(yearNumber));
			}
			dateYearLink.add(dateLink);
		}
	}

	@Value("${CSAT_Value_Q1}")
	String cSATQM1;
	@Value("${CSAT_Value_Q2}")
	String cSATQM2;
	@Value("${CSAT_Value_Q3}")
	String cSATQM3;
	@Value("${CSAT_Value_Q4}")
	String cSATQM4;
	@Value("${CSAT_Value_Q5}")
	String cSATQM5;
	@Value("${CSAT_Value_Q6}")
	String cSATQM6;
	@Value("${CSAT_Value_Q7}")
	String cSATQM7;
	@Value("${CSAT_Value_Q8}")
	String cSATQM8;

	ClientSatisfactionData clientSatisfactionData;

	void parseMethod2(AtlassianHostUser hostUser, JSONObject response, HttpSession session) throws ParseException{

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		cSATdata = new ArrayList<>();
		String cn = fieldList.get("Client Name");
		String sd = fieldList.get("Sent Date");
		String rd = fieldList.get("Received Date");
		String qsdQ = fieldList.get("Quarter");
		String qed = fieldList.get("Year");
		String qa1 = fieldList.get(cSATQM1);
		String qa2 = fieldList.get(cSATQM2);
		String qa3 = fieldList.get(cSATQM3);
		String qa4 = fieldList.get(cSATQM4);
		String qa5 = fieldList.get(cSATQM5);
		String qa6 = fieldList.get(cSATQM6);
		String qa7 = fieldList.get(cSATQM7);
		String qa8 = fieldList.get(cSATQM8);

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			clientSatisfactionData = new ClientSatisfactionData();
			JSONObject fields = j.getJSONObject("fields");

			clientSatisfactionData.setClient_Name(fields.get(cn).toString());

			Date date1 = formatter.parse(fields.get(sd).toString());
			String finalDate1 = newFormat.format(date1);
			clientSatisfactionData.setSent_Date(finalDate1);

			Date date2 = formatter.parse(fields.get(rd).toString());
			String finalDate2 = newFormat.format(date2);
			clientSatisfactionData.setReceived_Date(finalDate2);
   		    clientSatisfactionData.setQuarter_Start_Date(fields.getJSONObject(qsdQ).getString(VALUE));
			clientSatisfactionData.setYear(fields.get(qed).toString());
			clientSatisfactionData.setCSAT_Q1(fields.getJSONObject(qa1).getString(VALUE));
			clientSatisfactionData.setCSAT_Q2(fields.getJSONObject(qa2).getString(VALUE));
			clientSatisfactionData.setCSAT_Q3(fields.getJSONObject(qa3).getString(VALUE));
			clientSatisfactionData.setCSAT_Q4(fields.getJSONObject(qa4).getString(VALUE));
			clientSatisfactionData.setCSAT_Q5(fields.getJSONObject(qa5).getString(VALUE));
			clientSatisfactionData.setCSAT_Q6(fields.getJSONObject(qa6).getString(VALUE));
			clientSatisfactionData.setCSAT_Q7(fields.get(qa7).toString());
			clientSatisfactionData.setCSAT_Q8(fields.get(qa8).toString());
			
				if (clientSatisfactionData.getCSAT_Q1().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q1("-");
							} else if (clientSatisfactionData.getCSAT_Q2().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q2("-");
							} else if (clientSatisfactionData.getCSAT_Q3().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q3("-");
							} else if (clientSatisfactionData.getCSAT_Q4().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q4("-");
							} else if (clientSatisfactionData.getCSAT_Q5().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q5("-");
							} else if (clientSatisfactionData.getCSAT_Q6().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q6("-");
							} else if (clientSatisfactionData.getCSAT_Q7().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q7("-");
							} else if (clientSatisfactionData.getCSAT_Q8().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q8("-");
							}

			

			cSATdata.add(clientSatisfactionData);

		}

	}

	public Map<String, ArrayList<ClientSatisfactionFA>> getCSFA(AtlassianHostUser hostUser, String selectedProject, HttpSession session) throws Exception
			{

		Map<String, ArrayList<ClientSatisfactionFA>> resultArray = new LinkedHashMap<>();

		ArrayList<ArrayList<String>> callData = getFrontBackNumber();

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month0 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(0).get(0), callData.get(0).get(1), session);

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month1 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(1).get(0), callData.get(1).get(1), session);

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month2 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(2).get(0), callData.get(2).get(1), session);

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month3 = cst.findCountCSFA(hostUser, selectedProject,

				callData.get(3).get(0), callData.get(3).get(1), session);

		CompletableFuture.allOf(month0, month1, month2, month3).join();

		ArrayList<ArrayList<ClientSatisfactionFA>> resultString = new ArrayList<>();

		resultString.add(month0.get());
		resultString.add(month1.get());
		resultString.add(month2.get());
		resultString.add(month3.get());

		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;

	}
	public Map<String, CIDIData> getCidiMethod(AtlassianHostUser hostUser, String selectedProject, HttpSession session)
			throws Exception {

		Map<String, CIDIData> resultArray = new LinkedHashMap<>();

		CompletableFuture<CIDIData> month0 = cst.findCIDICount(hostUser, selectedProject, -1, session);

		CompletableFuture<CIDIData> month1 = cst.findCIDICount(hostUser, selectedProject, -2, session);

		CompletableFuture<CIDIData> month2 = cst.findCIDICount(hostUser, selectedProject, -3, session);

		CompletableFuture<CIDIData> month3 = cst.findCIDICount(hostUser, selectedProject, -4, session);

		CompletableFuture.allOf(month0, month1, month2, month3).join();

		ArrayList<CIDIData> resultString = new ArrayList<>();

		resultString.add(month0.get());
		resultString.add(month1.get());
		resultString.add(month2.get());
		resultString.add(month3.get());

		String[] months = new String[4];
		DateTimeFormatter formatterCidi = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 4, j = 3; i >= 1; i--, j--) {

			String month = (YearMonth.now().minusMonths(i)).format(formatterCidi);
			months[j] = month;

			resultArray.put(months[j], resultString.get(j));

		}

		return resultArray;
	}
}

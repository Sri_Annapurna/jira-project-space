package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.GoalsAndTargetsData;

@Service
public class GoalsAndTargetsService {
	private static final String VALUE = "value";
	private static final String ERROR = "error";
	private static final String FIELDS2 = "fields";
	private static final String ISSUES = "issues";
	private static final String TOTAL = "total";
	private static final String PROGOATAR = "Project Goals and Targets";
	@Autowired JiraCustomFieldMap jmap;
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	Map<String, String> fieldList = new HashMap<>();
	String status="Project Goal : Approved";
	
	String revEffTarget;
	String dreTarget;
	String srfTarget;
	String delDefectHigh;
	String delDefectMed;
	String delDefectLow;
	String inProcessDefects;
	
	Double revTarget = 0.0;
	Double defRemTarget=0.0;
	Double sftRemTarget = 0.0;
	Double delDefectHighTarget = 0.0;
	Double delDefectMedTarget = 0.0;
	Double delDefectLowTarget = 0.0;
	String inProcessDefTarget = "-";
	
	@Async
	public CompletableFuture<GoalsAndTargetsData> getGoalsAndTargetsData(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		fieldList = jmap.getJiraFieldList(hostUser, session);
		
		 revEffTarget = fieldList.get("Review Effectiveness(%)(>=)");
		 dreTarget = fieldList.get("Defect Removal Efficiency(%)(>=)");
		 srfTarget = fieldList.get("Software Requirements Functioning(%)(>=)");
		 delDefectHigh = fieldList.get("Delivered Defects - High(=)");
		 delDefectMed = fieldList.get("Delivered Defects - Medium(=)");
		 delDefectLow = fieldList.get("Delivered Defects - Low(<=)");
		 inProcessDefects = fieldList.get("In-process Defects");
		
		GoalsAndTargetsData goalsandtargetData = new GoalsAndTargetsData();
		String project = PROGOATAR;
		String dETarget="Improve Development Effectiveness";
			
		try{
			String url1="/rest/api/2/search?jql=project={p} AND (issuetype={DETarget})";
			String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"", "\"" + dETarget + "\"").toUri(), String.class);
			JSONObject response1 = new JSONObject(response);
			
			int count = response1.getInt(TOTAL);  
			if(count != 0) {
				
				
					
					
					getResultTargetData(response1);
			
				}
			else {
				String proUrl1="/rest/api/2/search?jql=project={project} AND (issuetype={DETarget})";
				String proResponse = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(proUrl1).buildAndExpand("\"" + project + "\"", "\"" + dETarget + "\"").toUri(), String.class);
				JSONObject proResponse1 = new JSONObject(proResponse);
				int proCount = proResponse1.getInt(TOTAL);  
				if(proCount != 0) {

						getResultTargetData(proResponse1);
			
					}
			}
			goalsandtargetData.setRevEffTarget(revTarget);
			goalsandtargetData.setDreTarget(defRemTarget);
			goalsandtargetData.setSrfTarget(sftRemTarget);
			goalsandtargetData.setDelDefectHigh(delDefectHighTarget);
			goalsandtargetData.setDelDefectMedium(delDefectMedTarget);
			goalsandtargetData.setDelDefectLow(delDefectLowTarget);
			goalsandtargetData.setInProcessDefects(inProcessDefTarget);
		}catch(Exception e) {
			es.writeException(e,ERROR);
	}
		return CompletableFuture.completedFuture(goalsandtargetData);
		}

	private void getResultTargetData(JSONObject response1) {
		
		JSONArray jsonArray = response1.getJSONArray(ISSUES);
		JSONObject j = jsonArray.getJSONObject(0);
		JSONObject fields = j.getJSONObject(FIELDS2);
		if(fields.get(revEffTarget)!=null)
			revTarget=(double)(fields.get(revEffTarget));
		if(fields.get(dreTarget)!=null)
			defRemTarget=(double)fields.get(dreTarget);
		if(fields.get(srfTarget)!=null)
			sftRemTarget=(double)fields.get(srfTarget);
		if(fields.get(delDefectHigh)!=null)
			delDefectHighTarget=(double)fields.get(delDefectHigh);
		if(fields.get(delDefectMed)!=null)
			delDefectMedTarget=(double)fields.get(delDefectMed);
		if(fields.get(delDefectLow)!=null)
			delDefectLowTarget=(double)fields.get(delDefectLow);
		if(fields.get(inProcessDefects)!=null)
			inProcessDefTarget=(String) fields.get(inProcessDefects);
	}
		
	

	
	String csatTargetO;
	String complaintsTargetO;
	String recurrenceComplaintsTargetO;
	Double cSATtargetD = 0.0;
	Double compTargetD = 0.0;
	Double recurrenceCompTargetD = 0.0;

	@Async
	public CompletableFuture<ArrayList<Double>> getGoalsAndTargetsCSAT(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		fieldList = jmap.getJiraFieldList(hostUser, session);
		
		 csatTargetO = fieldList.get("Overall Client Satisfaction Rating(>=)");
		 complaintsTargetO = fieldList.get("Client Complaints(<=) [Half-Yearly]");
		 recurrenceComplaintsTargetO = fieldList.get("Recurrence of same Client Complaint(=) [Half-Yearly]");
	
		ArrayList<Double> clientTargets =new ArrayList<>();
	
		String project = PROGOATAR;
		String cSATTarget="Improve Client Satisfaction";
		try{
			String url2="/rest/api/2/search?jql=project={p} AND (issuetype={CSATTarget})";
			String response2 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url2).buildAndExpand("\"" + p + "\"", "\"" + cSATTarget + "\"").toUri(), String.class);
			JSONObject response3 = new JSONObject(response2);
			int count2 = response3.getInt(TOTAL);  
			if(count2 != 0) {
	
				getResultCSAT(response3);
				
				}
			else{
				String proUrl2="/rest/api/2/search?jql=project={project} AND (issuetype={CSATTarget})";
				String proResponse2 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(proUrl2).buildAndExpand("\"" + project + "\"", "\"" + cSATTarget + "\"").toUri(), String.class);
				JSONObject proResponse3 = new JSONObject(proResponse2);
				int proCount2 = proResponse3.getInt(TOTAL);  
				if(proCount2 != 0) {
				getResultCSAT(proResponse3);
					}
			}
			clientTargets.add(cSATtargetD);
			clientTargets.add(compTargetD);
			clientTargets.add(recurrenceCompTargetD);
			
			}catch(Exception e) {
				es.writeException(e,ERROR);
		}
		return CompletableFuture.completedFuture(clientTargets);
		
	}

	private void getResultCSAT(JSONObject response3) {
		JSONArray jsonArray = response3.getJSONArray(ISSUES);
		JSONObject j = jsonArray.getJSONObject(0);
		JSONObject fields = j.getJSONObject(FIELDS2);
		if(fields.get(csatTargetO)!=null)
			cSATtargetD= ((double)(fields.get(csatTargetO)));
		if(fields.get(complaintsTargetO)!=null)
			compTargetD= ((double) (fields.get(complaintsTargetO)));
		if(fields.get(recurrenceComplaintsTargetO)!=null)
			recurrenceCompTargetD = (double) fields.get(recurrenceComplaintsTargetO);
	}

		

	@Async
	public CompletableFuture<ArrayList<Double>> getGoalsAndTargetsRisk(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		ArrayList<Double> deliveryManagTargets =new ArrayList<>();
		fieldList = jmap.getJiraFieldList(hostUser, session);
		String riskExpoTarget = fieldList.get("Delivery Risk Exposure Rating – After Risk Treatment(<=)");
		String reqDelOnTime = fieldList.get("Requirements Delivered On-time (%)");
		String workReqDelOnTime = fieldList.get("Work Requests Delivered On-time (%)");
		String risktarget;
		Double risk=0.0, reqDelOnTimeTarget=0.0;
		String project = PROGOATAR;
		String riskTarget="Improve Delivery Management Effectiveness";	
	try{
		String url3="/rest/api/2/search?jql=project={p} AND (issuetype={RiskTarget})";
		String response3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url3).buildAndExpand("\"" + p + "\"", "\"" + riskTarget + "\"").toUri(), String.class);
		JSONObject response4 = new JSONObject(response3);
		int count3 = response4.getInt(TOTAL);  
		if(count3 != 0) {
			
			
			JSONArray jsonArray = response4.getJSONArray(ISSUES);
				JSONObject j = jsonArray.getJSONObject(0);
				JSONObject fields = j.getJSONObject(FIELDS2);
				if(!fields.get(riskExpoTarget).toString().equals("null")){
					JSONObject category = fields.getJSONObject((riskExpoTarget));
					risktarget= category.getString(VALUE);
					risk= Double.parseDouble(risktarget);
				}
				if(session.getAttribute("selectedcategory").equals("Software Maintenance Release Mode") || session.getAttribute("selectedcategory").equals("Software Development") && !fields.get(reqDelOnTime).toString().equals("null"))
					{
						reqDelOnTimeTarget = (double)fields.get(reqDelOnTime);
					}
				else if(session.getAttribute("selectedcategory").equals("Software Maintenance Work Request Mode") && !fields.get(workReqDelOnTime).toString().equals("null"))
					{
						reqDelOnTimeTarget = (double)fields.get(workReqDelOnTime);
					}
			}
		else{
			String proUrl3="/rest/api/2/search?jql=project={project} AND (issuetype={RiskTarget})";
			String proResponse3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(proUrl3).buildAndExpand("\"" + project + "\"", "\"" + riskTarget + "\"").toUri(), String.class);
			JSONObject proResponse4 = new JSONObject(proResponse3);
			int proCount3 = proResponse4.getInt(TOTAL);  
			if(proCount3 != 0) {
				JSONArray jsonArray = proResponse4.getJSONArray(ISSUES);

					JSONObject j = jsonArray.getJSONObject(0);
					JSONObject fields = j.getJSONObject(FIELDS2);
					if(!fields.get(riskExpoTarget).toString().equals("null")){
					JSONObject category = fields.getJSONObject((riskExpoTarget));
						risktarget= category.getString(VALUE);
						risk= Double.parseDouble(risktarget);
					}
					if(!fields.get(reqDelOnTime).toString().equals("null"))
						reqDelOnTimeTarget = (double)fields.get(reqDelOnTime);
				}
		}
		deliveryManagTargets.add(risk);
		deliveryManagTargets.add(reqDelOnTimeTarget);
		}catch(Exception e) {
			es.writeException(e,ERROR);
	}
		return CompletableFuture.completedFuture(deliveryManagTargets);
}
	
	//CIDI Target
	
	@Async
	public CompletableFuture<Double> getGoalsAndTargetsCIDI(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		fieldList = jmap.getJiraFieldList(hostUser, session);
		String cidiTarget = fieldList.get("Creative Ideas and Delivery Improvements Implemented on Time(%)(>=)");
		Double cidiImplmentedTarget=0.0;
		String project = PROGOATAR;
		String processIssueTarget="Improve Process Effectiveness";	
	try{
		String url3="/rest/api/2/search?jql=project={p} AND (issuetype={processIssueTarget})";
		String response3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url3).buildAndExpand("\"" + p + "\"", "\"" + processIssueTarget + "\"").toUri(), String.class);
		JSONObject response4 = new JSONObject(response3);
		int count3 = response4.getInt(TOTAL);  
		if(count3 != 0) {
			
			
			JSONArray jsonArray = response4.getJSONArray(ISSUES);
				JSONObject j = jsonArray.getJSONObject(0);
				JSONObject fields = j.getJSONObject(FIELDS2);
				if(fields.get(cidiTarget)!=null)
					cidiImplmentedTarget=(double)(fields.get(cidiTarget));	
			}
		else{
			String proUrl3="/rest/api/2/search?jql=project={project} AND (issuetype={processIssueTarget})";
			String proResponse3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(proUrl3).buildAndExpand("\"" + project + "\"", "\"" + processIssueTarget + "\"").toUri(), String.class);
			JSONObject proResponse4 = new JSONObject(proResponse3);
			int proCount3 = proResponse4.getInt(TOTAL);  
			if(proCount3 != 0) {
				JSONArray jsonArray = proResponse4.getJSONArray(ISSUES);

					JSONObject j = jsonArray.getJSONObject(0);
					JSONObject fields = j.getJSONObject(FIELDS2);
					if(fields.get(cidiTarget)!=null)
						cidiImplmentedTarget=(double)(fields.get(cidiTarget));	
				}
		}
		}catch(Exception e) {
			es.writeException(e,ERROR);
	}
	return CompletableFuture.completedFuture(cidiImplmentedTarget);
}
}

	
		

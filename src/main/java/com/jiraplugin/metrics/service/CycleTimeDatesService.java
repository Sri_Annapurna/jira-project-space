package com.jiraplugin.metrics.service;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
@Service
public class CycleTimeDatesService {
	@Value("${individual_issue_date_range_url}")
	String url;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	
	@Autowired ErrorService es;
	@Value("${change_request}") String change_request;
	@Value("${new_requirement}") String new_req;
	@Value("${improvement}") String imp;
	@Async
	public CompletableFuture<Map<String, ArrayList<String>>> getCycleTimeDates(String p,String type,String sDate, String eDate, @AuthenticationPrincipal AtlassianHostUser hostUser, ArrayList<String> dates,HttpSession session) throws JSONException, ParseException {
		Map<String, ArrayList<String>> cycleTimeMap = new HashMap<String, ArrayList<String>>();
		try{
			Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
			
			String actualStartDate = fieldList.get("Actual Start Date");
			String hold = fieldList.get("Hold Time");
			
			int initialTotal = 0,flag=0;
			int count,total=1;
			int startAtCount = 0;
			double sumCycleTime=0,avgCycleTime=0,minCycleTime=Long.MAX_VALUE,maxCycleTime=Long.MIN_VALUE,sumEffort=0,avgEffort=0,minEffort=Long.MAX_VALUE,maxEffort=Long.MIN_VALUE;
			double holdTime=0.0;
			double timeSpent=0.0,timeSpentHrs=0.0;
			JSONArray jsonArray = new JSONArray();
			JSONArray finalJSONArray = new JSONArray();
			
			
			do{
			String url1="/rest/api/2/search?jql=project={p} AND (issuetype={change_request} OR issuetype={new_req} OR issuetype={imp}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate}) AND (status='Closed') AND ('Complexity'= {type})&startAt=0&maxResults=100";
			String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"", "\""+ change_request + "\"" ,"\""+ new_req + "\"" ,"\""+ imp + "\"" ,"\""+ sDate + "\"" , "\""+ eDate + "\"", "\"" + type + "\"" ).toUri(), String.class);
			JSONObject response1 = new JSONObject(response);
			count = response1.getInt("total");  
				
				jsonArray = response1.getJSONArray("issues");
				for (int i = 0; i < jsonArray.length(); i++) {
				    finalJSONArray.put(jsonArray.getJSONObject(i));
				}
				
				int issueMaxResult = response1.getInt("maxResults");
				initialTotal = initialTotal + issueMaxResult;
				startAtCount=initialTotal;
			} while(initialTotal < count);
			
			for(String d: dates) {
				ArrayList<String> e = new ArrayList<String>();
				e.add("0.0");e.add("0.0");e.add("0.0");e.add("0.0");/*e.add("0.0");e.add("0.0");e.add("0.0");*/
				cycleTimeMap.put(d, e);
			}
			
			if(count!=0){		
				for(int i=0;i<finalJSONArray.length();i++){
					JSONObject j = finalJSONArray.getJSONObject(i);
					JSONObject fields = j.getJSONObject("fields");
					if(!fields.get(hold).toString().equals("null")){
						holdTime=(double) fields.get(hold);
					}
					String actual= fields.get(actualStartDate).toString();
					Date actualDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(actual);
					//	total++;
						String resolved=fields.get("resolutiondate").toString();
						Date resolvedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(resolved);
						
						double d=(resolvedDate.getTime() - actualDate.getTime());
						double cycletime=(((double)(resolvedDate.getTime() - actualDate.getTime())/86400000)-holdTime);
						DecimalFormat df= new DecimalFormat("0.0");
						
					
						GregorianCalendar cal = new GregorianCalendar();
						cal.setTime(resolvedDate);
						System.out.println(Calendar.DATE);
						cal.add(Calendar.DATE, (int) -holdTime);
						Date after=cal.getTime();
						long diff = after.getTime() - actualDate.getTime();
						
						/*if(!fields.get("aggregatetimespent").equals(null)){
								 timeSpent = fields.getDouble("aggregatetimespent");
								 timeSpentHrs = timeSpent/3600;
						}*/
						
						String date = fields.get(fieldList.get("Reported Date")).toString();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
						Date rdate = formatter.parse(date);
						String key = new SimpleDateFormat("MMM-yy").format(rdate);
						if(cycleTimeMap.get(key).get(3).equals("0.0")){
							ArrayList<String> a = new ArrayList<String>();
							 a.add(df.format(cycletime));
							 a.add(df.format(cycletime));
							 a.add(df.format(cycletime));
							/* a.add(df.format(timeSpentHrs));
							 a.add(df.format(timeSpentHrs));
							 a.add(df.format(timeSpentHrs));*/
							 a.add(df.format(total));
							cycleTimeMap.put(key,a);
						}
						else{
								
								if(cycletime<Double.parseDouble(cycleTimeMap.get(key).get(0)))
								{
									ArrayList<String> internal =cycleTimeMap.get(key);
									internal.set(0,df.format(cycletime));
								}
								if(cycletime>Double.parseDouble(cycleTimeMap.get(key).get(1)))
								{
									ArrayList<String> internal =cycleTimeMap.get(key);
									internal.set(1,df.format(cycletime));
								}
								Double totalIssues = Double.parseDouble(cycleTimeMap.get(key).get(6));
								totalIssues++;
								avgCycleTime = (Double.parseDouble(cycleTimeMap.get(key).get(2))+cycletime)/totalIssues;
								ArrayList<String> internal =cycleTimeMap.get(key);
								internal.set(2,df.format(avgCycleTime));
								internal.set(3, df.format(totalIssues));
								/*if(timeSpentHrs<Double.parseDouble(cycleTimeMap.get(key).get(3)))
								{
									ArrayList<String> internal1 =cycleTimeMap.get(key);
									internal1.set(3,df.format(timeSpentHrs));
								}
								if(timeSpentHrs>Double.parseDouble(cycleTimeMap.get(key).get(4)))
								{
									ArrayList<String> internal1 =cycleTimeMap.get(key);
									internal1.set(4,df.format(timeSpentHrs));
								}
								avgEffort = (Double.parseDouble(cycleTimeMap.get(key).get(5))+timeSpentHrs)/totalIssues;
								ArrayList<String> internal1 =cycleTimeMap.get(key);
								internal1.set(5,df.format(avgEffort));*/
						}
						
			}
			
			}
			
			
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return CompletableFuture.completedFuture(cycleTimeMap);
	}
}

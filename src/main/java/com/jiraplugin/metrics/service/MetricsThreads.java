package com.jiraplugin.metrics.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.DefectTypeData;
import com.jiraplugin.metrics.model.EffortData;
import com.jiraplugin.metrics.model.EffortsData;
import com.jiraplugin.metrics.model.MetricsData;

@Service
public class MetricsThreads {
	@Autowired
	SubTaskDataService st;
	@Value("${new_requirement}")
	String newRequirement;
	@Value("${change_request}")
	String changeRequest;
	@Value("${task}")
	String task;
	@Value("${review_defect}")
	String reviewDefectNwf;
	@Value("${testing_defect}")
	String testingDefectNwf;
	@Value("${delivered_defect}")
	String deliveredDefect;
	@Value("${analysis_design_review_defect}")
	String analysisDesignReviewDefectNwf;
	@Value("${unit_testing_defect}")
	String unitTestingDefectNwf;
	@Value("${test_case_review_defect}")
	String testCaseReviewDefectNwf;
	@Value("${delivery_defect}")
	String deliveryDefect;
	@Autowired
	DefectsService ds;

	Logger logger = LoggerFactory.getLogger(MetricsThreads.class);

	public Map<String, MetricsData> createMetricsThreads(String p, String sDate, String eDate, List<String> dates,
			AtlassianHostUser hostUser, HttpSession session) {

		LinkedHashMap<String, MetricsData> metricsData = new LinkedHashMap<>();

		try {

			CompletableFuture<LinkedHashMap<String, EffortData>> newRequest = st.getMetricsData(p, newRequirement,
					sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, EffortData>> changeRequirement = st.getMetricsData(p, changeRequest,
					sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, EffortData>> tasks = st.getMetricsData(p, task, sDate, eDate, dates,
					hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> review = ds.getDefectsData(p, reviewDefectNwf,
					sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> testing = ds.getDefectsData(p, testingDefectNwf,
					sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> delivery = ds.getDefectsData(p, deliveredDefect,
					sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> analysisAndDesign = ds.getDefectsData(p,
					analysisDesignReviewDefectNwf, sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> unitTesting = ds.getDefectsData(p,
					unitTestingDefectNwf, sDate, eDate, dates, hostUser, session);
			CompletableFuture<LinkedHashMap<String, DefectTypeData>> testCase = ds.getDefectsData(p,
					testCaseReviewDefectNwf, sDate, eDate, dates, hostUser, session);
			CompletableFuture.allOf(newRequest, changeRequirement, tasks, review, analysisAndDesign, unitTesting,
					testCase, testing, delivery).join();
			LinkedHashMap<String, DEffectivenessData> defectsData = new LinkedHashMap<>();
			LinkedHashMap<String, EffortsData> effortsData = new LinkedHashMap<>();
			for (String d : dates) {
				DEffectivenessData dd = new DEffectivenessData();
				dd.setDeliveryCausalData(delivery.get().get(d).getCiData());
				dd.setDesign_count(unitTesting.get().get(d).gettCount() + testCase.get().get(d).gettCount());
				dd.setdHCount(delivery.get().get(d).gethCount());
				dd.setdLCount(delivery.get().get(d).getlCount());
				dd.setdMCount(delivery.get().get(d).getmCount());
				dd.setdTCount(delivery.get().get(d).gettCount());
				dd.setiHCount(review.get().get(d).gethCount() + analysisAndDesign.get().get(d).gethCount()
						+ unitTesting.get().get(d).gethCount() + testCase.get().get(d).gethCount()
						+ testing.get().get(d).gethCount());
				dd.setiLCount(review.get().get(d).getlCount() + analysisAndDesign.get().get(d).getlCount()
						+ unitTesting.get().get(d).getlCount() + testCase.get().get(d).getlCount()
						+ testing.get().get(d).getlCount());
				dd.setiMCount(review.get().get(d).getmCount() + analysisAndDesign.get().get(d).getmCount()
						+ unitTesting.get().get(d).getmCount() + testCase.get().get(d).getmCount()
						+ testing.get().get(d).getmCount());
				dd.setiTCount(review.get().get(d).gettCount() + analysisAndDesign.get().get(d).gettCount()
						+ unitTesting.get().get(d).gettCount() + testCase.get().get(d).gettCount()
						+ testing.get().get(d).gettCount());
				dd.setRd_adc_count(review.get().get(d).gettCount() + analysisAndDesign.get().get(d).gettCount()
						+ unitTesting.get().get(d).gettCount());
				dd.setrHCount(review.get().get(d).gethCount() + analysisAndDesign.get().get(d).gethCount()
						+ unitTesting.get().get(d).gethCount() + testCase.get().get(d).gethCount());
				dd.setrLCount(review.get().get(d).getlCount() + analysisAndDesign.get().get(d).getlCount()
						+ unitTesting.get().get(d).getlCount() + testCase.get().get(d).getlCount());
				dd.setrMCount(review.get().get(d).getmCount() + analysisAndDesign.get().get(d).getmCount()
						+ unitTesting.get().get(d).getmCount() + testCase.get().get(d).getmCount());
				dd.setrTCount(review.get().get(d).gettCount() + analysisAndDesign.get().get(d).gettCount()
						+ unitTesting.get().get(d).gettCount() + testCase.get().get(d).gettCount());
				dd.settHCount(testing.get().get(d).gethCount());
				dd.settLCount(testing.get().get(d).getlCount());
				dd.settMCount(testing.get().get(d).getmCount());
				dd.settTCount(testing.get().get(d).gettCount());
				defectsData.put(d, dd);
				EffortsData ed = new EffortsData();
				ed.setTotalReviewEffort(newRequest.get().get(d).getAnalysis_review_effort()
						+ newRequest.get().get(d).getCode_review_effort()
						+ newRequest.get().get(d).getDesign_review_effort()
						+ newRequest.get().get(d).getRe_by_developer() + newRequest.get().get(d).getRe_by_qa()
						+ newRequest.get().get(d).getUnit_test_design_review_effort()
						+ changeRequirement.get().get(d).getAnalysis_review_effort()
						+ changeRequirement.get().get(d).getCode_review_effort()
						+ changeRequirement.get().get(d).getDesign_review_effort()
						+ changeRequirement.get().get(d).getRe_by_developer()
						+ changeRequirement.get().get(d).getRe_by_qa()
						+ changeRequirement.get().get(d).getUnit_test_design_review_effort()
						+ tasks.get().get(d).getAnalysis_review_effort() + tasks.get().get(d).getCode_review_effort()
						+ tasks.get().get(d).getDesign_review_effort() + tasks.get().get(d).getRe_by_developer()
						+ tasks.get().get(d).getRe_by_qa() + tasks.get().get(d).getUnit_test_design_review_effort());
				ed.setReviewEffort(newRequest.get().get(d).getAnalysis_review_effort()
						+ newRequest.get().get(d).getCode_review_effort()
						+ newRequest.get().get(d).getDesign_review_effort()
						+ newRequest.get().get(d).getUnit_test_design_review_effort()
						+ changeRequirement.get().get(d).getUnit_test_design_review_effort()
						+ changeRequirement.get().get(d).getAnalysis_review_effort()
						+ changeRequirement.get().get(d).getCode_review_effort()
						+ changeRequirement.get().get(d).getDesign_review_effort()
						+ tasks.get().get(d).getAnalysis_review_effort() + tasks.get().get(d).getCode_review_effort()
						+ tasks.get().get(d).getDesign_review_effort()
						+ tasks.get().get(d).getUnit_test_design_review_effort());
				ed.setTestableCount(newRequest.get().get(d).getTestable_count()
						+ changeRequirement.get().get(d).getTestable_count() + tasks.get().get(d).getTestable_count());
				ed.setTotalUnitTestDesignReviewEffort(newRequest.get().get(d).getUnit_test_design_review_effort()
						+ changeRequirement.get().get(d).getUnit_test_design_review_effort());
				effortsData.put(d, ed);
			}

			for (Entry<String, DEffectivenessData> entry : defectsData.entrySet()) {
				logger.info(entry.getKey(), "----", entry.getValue());
			}

			for (String d : dates) {
				MetricsData data = new MetricsData();

				double reviewEfficiency = 0;
				double revEffect = 0;
				double reviewEffectiveAdc = 0;
				double testEffect = 0;
				double dRE = 0;
				DEffectivenessData performanceMetrics = defectsData.get(d);
				int reviewdefectsCount = performanceMetrics.getRd_adc_count();
				int totalReviewDefectsCount = performanceMetrics.getrTCount();
				int testingdefectsCount = performanceMetrics.gettTCount();
				int totalTestDesignDefectsCount = performanceMetrics.getDesign_count();
				int deliverydefectsCount = performanceMetrics.getdTCount();
				int totalInprocessDefects = totalReviewDefectsCount + testingdefectsCount;

				int inprocessDefects = reviewdefectsCount + testingdefectsCount;
				if (inprocessDefects != 0) {
					double reeffnss = (totalReviewDefectsCount * 100.0) / totalInprocessDefects;
					revEffect = Math.round(reeffnss * 100.0) / 100.0;
					data.setRevef(String.valueOf(revEffect));
					double reviewEffectivenessAdc = (reviewdefectsCount * 100.0) / inprocessDefects;
					reviewEffectiveAdc = Math.round(reviewEffectivenessAdc * 100.0) / 100.0;
					data.setReview_effectiveness_adc(String.valueOf(reviewEffectiveAdc));
					double te = (testingdefectsCount * 100.0) / totalInprocessDefects;
					testEffect = Math.round(te * 100.0) / 100.0;
					data.setTesteff(String.valueOf(testEffect));
					double dre = (totalInprocessDefects * 100.0) / (totalInprocessDefects + deliverydefectsCount);
					dRE = Math.round(dre * 100.0) / 100.0;
					data.setDreff(String.valueOf(dRE));
				} else {
					data.setRevef("0.0");
					data.setReview_effectiveness_adc("0.0");
					data.setTesteff("0.0");
					data.setDreff("0");
					data.setReview_efficiency("0.0");
					if (deliverydefectsCount == 0)
						data.setDreff("0.0");
				}
				double totalReviewEffort = effortsData.get(d).getTotalReviewEffort();
				if (totalReviewEffort == 0)
					data.setTotal_review_efficiency("0.0");
				else if (totalReviewEffort == 0 && totalReviewDefectsCount == 0)
					data.setTotal_review_efficiency("0.0");
				else {
					reviewEfficiency = totalReviewDefectsCount / totalReviewEffort;

					data.setTotal_review_efficiency(String.format("%.2f", reviewEfficiency));
				}
				double reviewEffort = effortsData.get(d).getReviewEffort();
				if (reviewEffort == 0)
					data.setReview_efficiency("0.0");
				else if (reviewEffort == 0 && reviewdefectsCount == 0)
					data.setReview_efficiency("0.0");
				else {
					reviewEfficiency = reviewdefectsCount / reviewEffort;

					data.setReview_efficiency(String.format("%.2f", reviewEfficiency));
				}
				data.setRevd(reviewdefectsCount);
				data.setTesd(testingdefectsCount);
				data.setDeld(deliverydefectsCount);
				data.setTotal_inprocess_defects(totalInprocessDefects);
				data.setTotal_review_defects(totalReviewDefectsCount);
				data.setInpd(inprocessDefects);
				data.setReview_effort(String.valueOf(reviewEffort));

				data.setTotal_review_effort(String.valueOf(totalReviewEffort));
				data.setTest_design_review_effort(effortsData.get(d).getTotalUnitTestDesignReviewEffort());
				data.setTotal_test_design_defects(totalTestDesignDefectsCount);
				data.setTotal_testable(effortsData.get(d).getTestableCount());
				data.setD(performanceMetrics);
				
				
				
				metricsData.put(d, data);
			}

		} catch (Throwable ex) {
			logger.error(ex.getMessage());
		}
		return metricsData;
	}
}
package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.TestCase;


@Service
public class TestcaseMetricsService {
    @Autowired
    private AtlassianHostRestClients restClients;
    @Autowired
    ErrorService es;
    @Autowired
    JiraCustomFieldMap jcfm;
    @Value("${development_effectiveness_testing}")
    String developmentEffectivenessTesting;
    @Value("${manual_test_req}")
    String manualTestReq;
    @Value("${testingTask}")
    String testingTask;
    @Value("${automation_test_req}")
    String automationTestReq;
    @Value("${test_cases_planned_to_design}")
    String testCasesPlannedToDesign;
    @Value("${test_cases_designed}")
    String testCasesDesigned;
    @Value("${test_cases_planned_for_execution}")
    String testCasesPlannedForExecution;
    @Value("${test_cases_actually_executed}")
    String testCasesActuallyExecuted;
    @Value("${test_cases_passed}")
    String testCasesPassed;
    @Value("${test_cases_failed}")
    String testCasesFailed;
    @Value("${testable_requirements_for_which_tests_are_executed}")
    String testableRequirementsForWhichTestsAreExecuted;
    @Value("${ftr_by_client}")
    String ftrByClient;
    @Value("${reported_date}")
    String reportedDate;
    
    Logger logger = LoggerFactory.getLogger(TestcaseMetricsService.class);

    public CompletableFuture<TestCase> getTestingMetrics(String p, int monthsDifference,boolean isSprintWise, AtlassianHostUser hostUser,
            MetricsData performanceList,HttpSession session){
	TestCase data = new TestCase();
	Map<String, String> customfieldTesting = jcfm.getJiraFieldList(hostUser, session) ;
	String url = "";
	String response = "";
	int startAt = 0;
	int total = 0;
	JSONArray jsonArray = new JSONArray();
	try {
		do{
			if(isSprintWise) {
				url = "/rest/api/2/search?jql=Sprint= {sprintId}  AND (issuetype={development_effectiveness_testing} OR  issuetype={manual_test_req}OR  issuetype={testing_task} OR issuetype={automation_test_req})&startAt={startAt}&maxResults=100";
				response = restClients.authenticatedAs(hostUser)
				        .getForObject(
				                UriComponentsBuilder.fromUriString(url)
				                        .buildAndExpand(monthsDifference,"\"" + developmentEffectivenessTesting + "\"","\"" + manualTestReq + "\"","\"" + testingTask + "\"","\"" + automationTestReq + "\"",startAt).toUri(),String.class);
			data.setMonth(performanceList.getMonth());
			}else {
				url = "/rest/api/2/search?jql=project={p} AND (issuetype={development_effectiveness_testing} OR  issuetype={manual_test_req} OR issuetype={automation_test_req}) AND 'Reported Date'>=startOfMonth({i}) AND 'Reported Date'<=endOfMonth({i})&startAt={startAt}&maxResults=100";
				response = restClients.authenticatedAs(hostUser)
			            .getForObject(UriComponentsBuilder.fromUriString(url)
			                    .buildAndExpand("\"" + p + "\"", "\"" + developmentEffectivenessTesting + "\"",
			                            "\"" + manualTestReq + "\"", "\"" + automationTestReq + "\"", monthsDifference,
			                            monthsDifference,startAt)
			                    .toUri(), String.class);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
			    String month = (YearMonth.now().minusMonths(Math.abs(monthsDifference))).format(formatter);
			    data.setMonth(month);
			}
			JSONObject jsonObj = new JSONObject(response);
			startAt =  startAt + 100;
			total = Integer.parseInt(jsonObj.get("total").toString());
			JSONArray jsonArray1 = jsonObj.getJSONArray("issues");
			for (int k = 0; k < jsonArray1.length(); k++) {
				JSONObject objects = jsonArray1.getJSONObject(k);
				jsonArray.put(objects);
			}
			}while(startAt < total);
	    int totalTestCasesPlannedToDesign = 0;
	    int totalTestCasesDesigned = 0;
	    int totalTestCasesPassed = 0;
	    int totalTestCasesFailed = 0;
	    int totalTestCasesPlannedForExecution = 0;
	    int totalTestCasesActuallyExecuted = 0;
	    int totalTestableRequirementsForWhichTestsAreExecuted = 0;
	    int totalFtr=0;
	    double testDesignReviewEffort = 0;
	    if (jsonArray != null && jsonArray.length() > 0) {
		for (int i = 0; i < jsonArray.length(); i++) {
		    int testsPlannedToDesignD = 0;
		    int testsDesignedD = 0;
		    int testsPassedD = 0;
		    int testsFailedD = 0;
		    int testsPlannedForExecutionD = 0;
		    int testsActuallyExecutedD = 0;
		    int testableRequirementsForWhichTestsExecutedD = 0;
		    int ftr=0;
		    JSONObject issuesDataD = jsonArray.getJSONObject(i);
		    JSONObject fieldsDataD = issuesDataD.getJSONObject("fields");
		    try {
		    String plannedToDesign = fieldsDataD.get(customfieldTesting.get(testCasesPlannedToDesign))
		            .toString();
		    if (!plannedToDesign.equals("null") && !plannedToDesign.isEmpty()) {
			testsPlannedToDesignD = (int) Double.parseDouble(plannedToDesign);}
		    }catch(Exception e) {
		    	

		    	logger.error(e.getMessage());
		    }
		    String designed = fieldsDataD.get(customfieldTesting.get(testCasesDesigned)).toString();
		    if (!designed.equals("null") && !designed.isEmpty()) {
			testsDesignedD = (int) Double.parseDouble(designed);}

		    String passed = fieldsDataD.get(customfieldTesting.get(testCasesPassed)).toString();
		    if (!passed.equals("null") && !passed.isEmpty()) {
			testsPassedD = (int) Double.parseDouble(passed);}
		    String failed = fieldsDataD.get(customfieldTesting.get(testCasesFailed)).toString();
		    if (!failed.equals("null") && !failed.isEmpty()) {
			testsFailedD = (int) Double.parseDouble(failed);}
		    String plannedForExecution = fieldsDataD.get(customfieldTesting.get(testCasesPlannedForExecution))
		            .toString();
		    if (!plannedForExecution.equals("null") && !plannedForExecution.isEmpty()) {
			testsPlannedForExecutionD = (int) Double.parseDouble(plannedForExecution);}
		    String actuallyExecuted = fieldsDataD.get(customfieldTesting.get(testCasesActuallyExecuted))
		            .toString();
		    if (!actuallyExecuted.equals("null") && !actuallyExecuted.isEmpty()) {
			testsActuallyExecutedD = (int) Double.parseDouble(actuallyExecuted);}
		    String executedRequirements = fieldsDataD
		            .get(customfieldTesting.get(testableRequirementsForWhichTestsAreExecuted)).toString();
		    if (!executedRequirements.equals("null") && !executedRequirements.isEmpty()) {
			testableRequirementsForWhichTestsExecutedD = (int) Double.parseDouble(executedRequirements);}
		    try {
		    	
		    	String ftrClient = fieldsDataD.get(customfieldTesting.get(ftrByClient)).toString();
		    
		    if (!ftrClient.equals("null") && !ftrClient.isEmpty()) {
			ftr = (int) Double.parseDouble(ftrClient);}
		    }catch(Exception e) {
		    	logger.error(e.getMessage());
		    }
		    totalTestCasesPlannedToDesign = totalTestCasesPlannedToDesign + testsPlannedToDesignD;
		    totalTestCasesDesigned = totalTestCasesDesigned + testsDesignedD;
		    totalTestCasesPassed = totalTestCasesPassed + testsPassedD;
		    totalTestCasesFailed = totalTestCasesFailed + testsFailedD;
		    totalTestCasesPlannedForExecution = totalTestCasesPlannedForExecution
		            + testsPlannedForExecutionD;
		    totalTestCasesActuallyExecuted = totalTestCasesActuallyExecuted + testsActuallyExecutedD;
		    totalTestableRequirementsForWhichTestsAreExecuted = totalTestableRequirementsForWhichTestsAreExecuted
		            + testableRequirementsForWhichTestsExecutedD;
		    totalFtr=totalFtr+ftr;
		}

	    }
             
	    data.setTestdesign_planned(totalTestCasesPlannedToDesign);
	    data.setTestdesign_developed(totalTestCasesDesigned);
	    data.setTest_cases_passed(totalTestCasesPassed);
	    data.setTest_cases_failed(totalTestCasesFailed);
	    testDesignReviewEffort = performanceList.getTest_design_review_effort();
	    data.setTest_design_review_effort(testDesignReviewEffort);
	    int totalTestableRequirements = (int) performanceList.getTotal_testable();
	    int totalDeliveredDefects = performanceList.getDeld();
	    data.setTotal_testable_requirements(totalTestableRequirements);
	    int firstTimeRight = totalTestableRequirements - totalDeliveredDefects;
	    if(firstTimeRight < 0) {
	    	firstTimeRight = 0;
	    }
	    if(totalTestableRequirements<=0) {
		data.setSrf("0.0");}
	    else {
	    double srf=((double)firstTimeRight /totalTestableRequirements)*100;
	    double srfun = Math.round(srf * 100.0) / 100.0;
	    data.setSrf(String.valueOf(srfun));
	    }
	    
	    data.setTest_cases_planned_for_execution(totalTestCasesPlannedForExecution);
	    data.setTest_cases_actually_executed(totalTestCasesActuallyExecuted);
	    int totalDefectsInTestDesign = performanceList.getTotal_test_design_defects();

	    data.setTotaltestdefects(totalDefectsInTestDesign);
	    if (totalTestCasesPlannedForExecution == 0) {
		data.setTestexecution("0.0");}
	    else {
		double testsexec = (totalTestCasesActuallyExecuted / (double)totalTestCasesPlannedForExecution) * 100;
		double testsexecut = Math.round(testsexec * 100.0) / 100.0;
		data.setTestexecution(String.valueOf(testsexecut));
	    }
	    if (totalTestCasesDesigned == 0) {
		data.setTestdesignquality("0.0");}
	    else {
		double tdq = totalDefectsInTestDesign / (double)totalTestCasesDesigned;
		double tdquality = Math.round(tdq * 100.0) / 100.0;
		data.setTestdesignquality(String.valueOf(tdquality));
	    }
	    data.setTestable_requirements_executed(totalTestableRequirementsForWhichTestsAreExecuted);
	    if (performanceList.getTotal_testable() == 0) {
		data.setTestcoverage("0.0");}
	    else {
		double testcov = ((double)totalTestableRequirementsForWhichTestsAreExecuted
		        / performanceList.getTotal_testable()) * 100;
		double testcoverage = Math.round(testcov * 100.0) / 100.0;
		data.setTestcoverage(String.valueOf(testcoverage));
	    }
	    data.setFirst_time_right(firstTimeRight);
	    data.setTotalSoftwareRequirementsDelivered(totalTestableRequirements);
	    
	} catch (Exception e) {
	    logger.error(e.getMessage());
	}
	return CompletableFuture.completedFuture(data);
    }

    @Autowired JiraCustomFieldMap j;
    Map<String,String> customfields = new HashMap<>();
    public Map<String,TestCase> getTestingMetricsData(String p, String sDate, String eDate, List<String> dates,
            Map<String,MetricsData> performanceList, AtlassianHostUser hostUser,HttpSession session) throws IOException {
    	LinkedHashMap<String,TestCase> testCaseData = new LinkedHashMap<>();
    	for(String d: dates){
    		TestCase tc = new TestCase();
    		testCaseData.put(d, tc);
    	}
    	try {
    		
    	    Map<String, String> customfieldsTest = j.getJiraFieldList(hostUser, session);
    	    String url = "/rest/api/2/search?jql=project={p} AND (issuetype={development_effectiveness_testing} OR  issuetype={manual_test_req} OR issuetype={automation_test_req}) AND 'Reported Date' >= {sDate} AND 'Reported Date'<={eDate}";
    	    String response = restClients.authenticatedAs(hostUser)
    	            .getForObject(UriComponentsBuilder.fromUriString(url)
    	                    .buildAndExpand("\"" + p + "\"", "\"" + developmentEffectivenessTesting + "\"",
    	                            "\"" + manualTestReq + "\"", "\"" + automationTestReq + "\"", "\"" + sDate + "\"", "\"" + eDate + "\"")
    	                    .toUri(), String.class);
    	    JSONObject jsonObj = new JSONObject(response);
    	    JSONArray jsonArray = jsonObj.getJSONArray("issues");
    	    double testDesignReviewEffort = 0;
    	    int length = jsonArray.length();
    		for (int i = 0; i < length ; i++) {
    		    int testsPlannedToDesign = 0;
    		    int testsDesigned = 0;
    		    int testsPassed = 0;
    		    int testsFailed = 0;
    		    int testsPlannedForExecution = 0;
    		    int testsActuallyExecuted = 0;
    		    int testableRequirementsForWhichTestsExecuted = 0;
    		    int ftr=0;
    		    JSONObject issuesData = jsonArray.getJSONObject(i);
    		    JSONObject fieldsData = issuesData.getJSONObject("fields");
    		    String date = fieldsData.get(customfieldsTest.get(reportedDate)).toString();
    		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
   			 	Date rdate = formatter.parse(date);
   			 	String key = new SimpleDateFormat("MMM-yy").format(rdate);
    		    TestCase data = testCaseData.get(key);
    		    String plannedToDesign = fieldsData.get(customfieldsTest.get(testCasesPlannedToDesign))
    		            .toString();
    		    if (!plannedToDesign.equals("null") && !plannedToDesign.isEmpty()) {
    			testsPlannedToDesign = (int) Double.parseDouble(plannedToDesign);}
    		    String designed = fieldsData.get(customfieldsTest.get(testCasesDesigned)).toString();
    		    if (!designed.equals("null") && !designed.isEmpty()) {
    			testsDesigned = (int) Double.parseDouble(designed);}

    		    String passed = fieldsData.get(customfieldsTest.get(testCasesPassed)).toString();
    		    if (!passed.equals("null") && !passed.isEmpty()) {
    			testsPassed = (int) Double.parseDouble(passed);}
    		    String failed = fieldsData.get(customfieldsTest.get(testCasesFailed)).toString();
    		    if (!failed.equals("null") && !failed.isEmpty()) {
    			testsFailed = (int) Double.parseDouble(failed);}
    		    String plannedForExecution = fieldsData.get(customfieldsTest.get(testCasesPlannedForExecution))
    		            .toString();
    		    if (!plannedForExecution.equals("null") && !plannedForExecution.isEmpty()) {
    			testsPlannedForExecution = (int) Double.parseDouble(plannedForExecution);}
    		    String actuallyExecuted = fieldsData.get(customfieldsTest.get(testCasesActuallyExecuted))
    		            .toString();
    		    if (!actuallyExecuted.equals("null") && !actuallyExecuted.isEmpty()) {
    			testsActuallyExecuted = (int) Double.parseDouble(actuallyExecuted);}
    		    String executedRequirements = fieldsData
    		            .get(customfieldsTest.get(testableRequirementsForWhichTestsAreExecuted)).toString();
    		    if (!executedRequirements.equals("null") && !executedRequirements.isEmpty()) {
    			testableRequirementsForWhichTestsExecuted = (int) Double.parseDouble(executedRequirements);}
    		    String ftrClient = fieldsData
    		            .get(customfieldsTest.get(ftrByClient)).toString();
    		    if (!ftrClient.equals("null") && !ftrClient.isEmpty()) {
    			ftr = (int) Double.parseDouble(ftrClient);}
    		    data.setTestdesign_planned(data.getTestdesign_planned() + testsPlannedToDesign);
    		    data.setTestdesign_developed(data.getTestdesign_developed() + testsDesigned);
    		    data.setTest_cases_passed(data.getTest_cases_passed() + testsPassed);
    		    data.setTest_cases_failed(data.getTest_cases_failed() + testsFailed);
    		    data.setTest_cases_planned_for_execution(data.getTest_cases_planned_for_execution() + testsPlannedForExecution );
    		    data.setTest_cases_actually_executed(data.getTest_cases_actually_executed() + testsActuallyExecuted);
    		    data.setTestable_requirements_executed(data.getTestable_requirements_executed() + testableRequirementsForWhichTestsExecuted);
    		    data.setFirst_time_right(data.getFirst_time_right() + ftr);
       		}
    	    for(String d: dates){
    	    TestCase data = testCaseData.get(d);	
    	    testDesignReviewEffort = performanceList.get(d).getTest_design_review_effort();
    	    
    	    data.setTest_design_review_effort(testDesignReviewEffort);
    	    int totalTestableRequirements = (int) performanceList.get(d).getTotal_testable();
    	    data.setTotal_testable_requirements(totalTestableRequirements);
    	    if(totalTestableRequirements==0) {
    		data.setSrf("0.0");}
    	    else {
    	    double srf=(data.getFirst_time_right()/totalTestableRequirements)*100;
    	    double srfun = Math.round(srf * 100.0) / 100.0;
    	    data.setSrf(String.valueOf(srfun));
    	    }
    	    
    	    int totalDefectsInTestDesign = performanceList.get(d).getTotal_test_design_defects();

    	    data.setTotaltestdefects(totalDefectsInTestDesign);
    	    if (data.getTest_cases_planned_for_execution() == 0) {
    		data.setTestexecution("0.0");}
    	    else {
    		double testsexec = (data.getTest_cases_actually_executed() / (double)data.getTest_cases_planned_for_execution()) * 100;
    		double testsexecut = Math.round(testsexec * 100.0) / 100.0;
    		data.setTestexecution(String.valueOf(testsexecut));
    	    }
    	    if (data.getTestdesign_developed() == 0) {
    		data.setTestdesignquality("0.0");}
    	    else {
    		double tdq = totalDefectsInTestDesign / (double)data.getTestdesign_developed();
    		double tdquality = Math.round(tdq * 100.0) / 100.0;
    		data.setTestdesignquality(String.valueOf(tdquality));
    	    }
    	    
    	    if (performanceList.get(d).getTotal_testable() == 0) {
    		data.setTestcoverage("0.0");}
    	    else {
    		double testcov = ((double)data.getTestable_requirements_executed()
    		        / performanceList.get(d).getTotal_testable()) * 100;
    		double testcoverage = Math.round(testcov * 100.0) / 100.0;
    		data.setTestcoverage(String.valueOf(testcoverage));
    	    }
    	    }
    	} catch (Exception e) {
    	    es.writeException(e, p);
    	}
    	return testCaseData;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
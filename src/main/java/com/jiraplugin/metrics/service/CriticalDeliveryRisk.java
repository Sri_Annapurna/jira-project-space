package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskOccurrenceData;
import com.jiraplugin.metrics.model.RiskSummaryData;

@Service
public class CriticalDeliveryRisk {

	@Autowired
	CriticalDeliveryRiskAsyn criticalDeliveryRiskAsyn;
	
	Logger logger = LoggerFactory.getLogger(CriticalDeliveryRisk.class);

	@Async
	public CompletableFuture<Map<String, ArrayList<RiskSummaryData>>> startThreads(AtlassianHostUser hostUser, String riskUrl, HttpSession session) throws InterruptedException, ExecutionException {

		CompletableFuture<ArrayList<RiskSummaryData>> page1 = criticalDeliveryRiskAsyn.findCount(hostUser, -1, riskUrl, session);

		CompletableFuture<ArrayList<RiskSummaryData>> page2 = criticalDeliveryRiskAsyn.findCount(hostUser, -2, riskUrl, session);

		CompletableFuture<ArrayList<RiskSummaryData>> page3 = criticalDeliveryRiskAsyn.findCount(hostUser, -3, riskUrl, session);

		CompletableFuture<ArrayList<RiskSummaryData>> page4 = criticalDeliveryRiskAsyn.findCount(hostUser, -4, riskUrl, session);

		CompletableFuture.allOf(page1, page2, page3, page4).join();
		
		
		Map<String, ArrayList<RiskSummaryData>> actualResult = new LinkedHashMap<>();
		ArrayList<ArrayList<RiskSummaryData>> resultArrayData = new ArrayList<>();
	/*	int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };*/
		
		resultArrayData.add(page4.get());
		resultArrayData.add(page3.get());
		resultArrayData.add(page2.get());
		resultArrayData.add(page1.get());
		

		String[] months = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		int j=0;
		for (int i = 4; i >= 1; i--) {

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;
			
			actualResult.put(months[j], resultArrayData.get(j));
			j++;
		}

		return CompletableFuture.completedFuture(actualResult);

	}
	
	@Async
	public CompletableFuture<Map<String, ResultRangeData>> startExposureRatingThreads(AtlassianHostUser hostUser, String riskUrl, HttpSession session) throws InterruptedException, ExecutionException {	
		CompletableFuture<ResultRangeData> page1 = criticalDeliveryRiskAsyn.getExposureRatingData(hostUser, riskUrl, -1, session);

		CompletableFuture<ResultRangeData> page2 = criticalDeliveryRiskAsyn.getExposureRatingData(hostUser, riskUrl, -2, session);

		CompletableFuture<ResultRangeData> page3 = criticalDeliveryRiskAsyn.getExposureRatingData(hostUser, riskUrl, -3, session);

		CompletableFuture<ResultRangeData> page4 = criticalDeliveryRiskAsyn.getExposureRatingData(hostUser, riskUrl, -4, session);

		CompletableFuture.allOf(page1, page2, page3, page4).join();
		
		
		Map<String, ResultRangeData> actualResult = new LinkedHashMap<>();
		ArrayList<ResultRangeData> resultArrayData = new ArrayList<>();
	/*	int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };*/
		
		resultArrayData.add(page4.get());
		resultArrayData.add(page3.get());
		resultArrayData.add(page2.get());
		resultArrayData.add(page1.get());
		

		String[] months = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		int j=0;
		for (int i = 4; i >= 1; i--) {

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;
			resultArrayData.get(j).setMonth(months[j]);
			actualResult.put(months[j], resultArrayData.get(j));
			j++;
		}

		return CompletableFuture.completedFuture(actualResult);

	}
	
	

	@Async
	public CompletableFuture<Map<String, ArrayList<RiskOccurrenceData>>> ocurrenceThreads(AtlassianHostUser hostUser, String riskUrl, Map<String,String> fieldList) throws InterruptedException, ExecutionException {

		CompletableFuture<ArrayList<RiskOccurrenceData>> page1 = criticalDeliveryRiskAsyn.getRiskOccurrenceData(hostUser, -1, riskUrl,fieldList);

		CompletableFuture<ArrayList<RiskOccurrenceData>> page2 = criticalDeliveryRiskAsyn.getRiskOccurrenceData(hostUser, -2, riskUrl,fieldList);

		CompletableFuture<ArrayList<RiskOccurrenceData>> page3 = criticalDeliveryRiskAsyn.getRiskOccurrenceData(hostUser, -3, riskUrl,fieldList);

		CompletableFuture<ArrayList<RiskOccurrenceData>> page4 = criticalDeliveryRiskAsyn.getRiskOccurrenceData(hostUser, -4, riskUrl,fieldList);

		CompletableFuture.allOf(page1, page2, page3, page4).join();
		Map<String, ArrayList<RiskOccurrenceData>> resultMap = new LinkedHashMap<>();
		try {
		ArrayList<ArrayList<RiskOccurrenceData>> resultArrayList = new ArrayList<>();
		resultArrayList.add(0,page4.get());
		resultArrayList.add(1,page3.get());
		resultArrayList.add(2,page2.get());
		resultArrayList.add(3,page1.get());
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		int j=0;
		for (int i = 4; i >= 1; i--) {
			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			System.out.println("into the risk month" + month);
			resultMap.put(month, resultArrayList.get(j));
			j++;
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return CompletableFuture.completedFuture(resultMap);

	}

	/*@Async
	public CompletableFuture<ArrayList<ArrayList<String>>> riskRecurrenceThreads(AtlassianHostUser hostUser,
			String selectedProject, String promain) throws InterruptedException, ExecutionException {

		ArrayList<ArrayList<String>> riskAcceptableResult = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultArray;

		String riskUrl = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project='" + selectedProject
				+ "' AND issuetype = 'Reccurence' AND 'Reported Date' >= startOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Client Name' ~ {promain}";

		long start = System.currentTimeMillis();

		Calendar cal = Calendar.getInstance();

		int monthCount = cal.get(Calendar.MONTH);

		int backMonth1 = 0;
		int backMonth2 = 0;
		int backMonth3 = 0;
		int backMonth4 = 0;

		switch (monthCount) {

		case 0:
			backMonth1 = 0;
			backMonth2 = -12;
			backMonth3 = -12;
			backMonth4 = -12;
			break;

		case 1:
			backMonth1 = -1;
			backMonth2 = -1;
			backMonth3 = -13;
			backMonth4 = -13;

			break;
		case 2:
			backMonth1 = -2;
			backMonth2 = -2;
			backMonth3 = -2;
			backMonth4 = -14;
			break;

		default:
			backMonth1 = -monthCount;
			backMonth2 = -monthCount;
			backMonth3 = -monthCount;
			backMonth4 = -monthCount;
			break;

		}

		CompletableFuture<Integer> page1 = criticalDeliveryRiskAsyn.findCount(hostUser, -1, backMonth1, riskUrl,
				promain);

		CompletableFuture<Integer> page2 = criticalDeliveryRiskAsyn.findCount(hostUser, -2, backMonth2, riskUrl,
				promain);

		CompletableFuture<Integer> page3 = criticalDeliveryRiskAsyn.findCount(hostUser, -3, backMonth3, riskUrl,
				promain);

		CompletableFuture<Integer> page4 = criticalDeliveryRiskAsyn.findCount(hostUser, -4, backMonth4, riskUrl,
				promain);

		CompletableFuture.allOf(page1, page2, page3, page4).join();

		int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {

			resultArray = new ArrayList<String>();

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.add(months[j]);
			resultArray.add(String.valueOf(resultString[j]));

			riskAcceptableResult.add(resultArray);
		}

		return CompletableFuture.completedFuture(riskAcceptableResult);

	}
*/
	@Autowired
	private AboutProject aboutproject;

	@Autowired
	private ProjectService jiraFieldList;

	public Map<String, String> riskOpenIssuesMethod(String sce, AtlassianHostUser hostUser)
			throws JSONException, NullPointerException {

		Map<String, String> mapFieldsMain = new LinkedHashMap<String, String>();

		Map<String, String> fieldList = jiraFieldList.customFieldIDMap;

		JSONObject boj = aboutproject.getFieldKeys(sce, hostUser);

		JSONArray jsonArray = boj.getJSONArray("issues");

		try {

			JSONObject dataObject = jsonArray.getJSONObject(0);

			JSONObject real = dataObject.getJSONObject("fields");

			mapFieldsMain.put("Client Name", real.getString(fieldList.get("Client Name")));
			mapFieldsMain.put("Scope", real.getString(fieldList.get("Scope")));
			mapFieldsMain.put("Project Start Date", real.getString(fieldList.get("Project Start Date")));
			mapFieldsMain.put("Current Status", real.getString(fieldList.get("Current Status")));
			int KO = (real.getInt(fieldList.get("Team Size")));
			mapFieldsMain.put("Team Size", String.valueOf(KO));
			/*
			 * mapFieldsMain.put("Project End Date",
			 * real.getString(fieldList.get("Project End Date")));
			 */
		} catch (Exception e) {
			logger.error(e.getMessage());
			

		}

		return mapFieldsMain;

	}

	@Autowired
	private RiskService riskService;

	public Map<Integer, List<String>> riskOpenItems(String sce, AtlassianHostUser hostUser)
			throws JSONException, ParseException {

		int errorFlag = 0;

		Map<Integer, List<String>> mapFieldsMain = new LinkedHashMap<Integer, List<String>>();

		List<String> fnm;

		String tds = "-";
		String ads = "-";

		SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

		try {

			Map<String, String> fieldList = jiraFieldList.customFieldIDMap;

			JSONArray jsonArray = riskService.riskOpenIssues(sce);

			if (jsonArray.length() != 0) {

				for (int j = 0; j < jsonArray.length(); j++) {

					fnm = new ArrayList<String>();
					// fnm.removeAll(fnm);

					JSONObject dataObject = jsonArray.getJSONObject(j);

					JSONObject real = dataObject.getJSONObject("fields");
					if (real.get(fieldList.get("Risk Description")) == null) {
						System.out.println("empty issue");

					} else {

						if (real.get(fieldList.get("Target Date")) != null) {

							Date td = new SimpleDateFormat("yyyy-MM-dd")
									.parse(real.getString(fieldList.get("Target Date")));

							tds = dt1.format(td);

						}
						if (real.get(fieldList.get("Actual Date")) != null) {

							Date ad = new SimpleDateFormat("yyyy-MM-dd")
									.parse(real.getString(fieldList.get("Actual Date")));

							ads = dt1.format(ad);
						}

						fnm.add(real.get(fieldList.get("Risk Description")).toString());
						fnm.add(real.get(fieldList.get("Risk Treatment Action / Control")).toString());
						fnm.add(tds);
						fnm.add(ads);
						String statusObj = real.get("status").toString();
						JSONObject jobj = new JSONObject(statusObj);
						String statusString = jobj.getString("name");

						fnm.add(statusString);

						fnm.replaceAll(s -> s.equals("null") ? "Yet To Plan" : s);
						mapFieldsMain.put(j, fnm);


					}

				}
			}


		} catch (Exception e) {
			logger.error(e.getMessage());
			errorFlag = 1;

		}

		if (errorFlag == 1) {

			return null;

		} else {

			return mapFieldsMain;

		}

	}

}

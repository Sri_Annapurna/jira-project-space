package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.fasterxml.jackson.core.JsonParseException;

@Service
public class WebHookHandler {

	@Autowired
	JiraCustomFieldMap jiraCustomFieldMap;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private ProjectService projectService;
	
	
	@Value("${riskIdentifiedValue}")
	String riskIdentifedStatus;

	@Value("${SnapUserName}")
	String userName;
	
	@Value("${PassWord}")
	String passWord;
	
	@Value("${DomainSuffix}")
	String doMain;
	
	@Value("${snapShotProject}")
	String snapProjName;
	
	@Value("${riskIdentifiedIssueValue}")
	String riskIdentifedIssueType;
	
	@Value("${Exposure_IssueType}")
	String exposureIssueType;

	public String updateIssue(String json, AtlassianHostUser hostUser, HttpSession session)
			throws JSONException, JsonParseException {

		String jiraBaseURL = hostUser.getHost().getBaseUrl() + "/rest/api/2/issue/";

		int RiskValueFlag = 0;
		int RiskOwnerFlag = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d");
		Date Currentdate = new Date();
		System.out.println(formatter.format(Currentdate));

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);

		Map<String, String> customField = jiraCustomFieldMap.getJiraFieldList(hostUser, session);

		JSONObject receivedObject = new JSONObject(json);

		JSONObject changeLog = receivedObject.getJSONObject("changelog");

		JSONArray items = changeLog.getJSONArray("items");

		JSONObject issues = receivedObject.getJSONObject("issue");

		String Issuekey = issues.getString("key");

		JSONObject fields = issues.getJSONObject("fields");

		String createIssueJSON = null;

		String newUrl = jiraBaseURL + Issuekey;

		String WebHookEvent = receivedObject.getString("issue_event_type_name");

		JSONObject user = receivedObject.getJSONObject("user");

		String UserNameData = user.getString("displayName");

		for (int i = 0; i < items.length(); i++) {

			JSONObject dataObject = items.getJSONObject(i);

			if (dataObject.getString("fieldId").equalsIgnoreCase(customField.get("Risk Value"))
					|| dataObject.getString("fieldId").equalsIgnoreCase(customField.get("Business Impact (BI)"))
					|| dataObject.getString("fieldId").equalsIgnoreCase(customField.get("Risk Probability (RP)"))) {

				int result = 0;

				String rp = fields.getJSONObject(customField.get("Risk Probability (RP)")).getString("value");

				String bi = fields.getJSONObject(customField.get("Business Impact (BI)")).getString("value");

				result = Integer.parseInt(rp) * Integer.parseInt(bi);

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Risk Value") + "\" : " + result + "}}";

				RiskValueFlag = 1;

			} else if (dataObject.getString("fieldId")
					.equalsIgnoreCase(customField.get("Revised Risk Probability (RRP)"))
					|| dataObject.getString("fieldId")
							.equalsIgnoreCase(customField.get("Revised Business Impact (RBI)"))
					|| dataObject.getString("fieldId").equalsIgnoreCase(customField.get("Residual Risk Value"))) {

				int result = 0;

				String rp = fields.getJSONObject(customField.get("Revised Risk Probability (RRP)")).getString("value");

				String bi = fields.getJSONObject(customField.get("Revised Business Impact (RBI)")).getString("value");

				result = Integer.parseInt(rp) * Integer.parseInt(bi);

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Residual Risk Value") + "\" : " + result
						+ "}}";

				RiskValueFlag = 1;

			} else if (WebHookEvent.contentEquals("issue_moved") && !UserNameData.contentEquals("admin")) {

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Risk Owner") + "\" :{\"name\":\""
						+ UserNameData + "\"}}}";

				RiskOwnerFlag = 1;

				break;

			}

		}

		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);

		HttpEntity<String> requestEntity = new HttpEntity<String>(createIssueJSON, headers);

		if (RiskValueFlag == 1 || RiskOwnerFlag == 1) {
			try {
				System.out.println("comes here????");
				ResponseEntity<String> responseEntity = restClients.authenticatedAsHostActor().exchange(newUrl,
						HttpMethod.PUT, requestEntity, String.class);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;

	}

}

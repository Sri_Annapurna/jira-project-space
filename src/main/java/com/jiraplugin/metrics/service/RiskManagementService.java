package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskManagementData;
import com.jiraplugin.metrics.model.RiskSummaryData;

@Service
public class RiskManagementService {

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	

	@Value("${snapShotProject}")
	String snapProject;

	@Autowired
	ProjectService projectService;

	@Autowired
	RiskManagementDateRangeService riskDateRangeService;

	public RiskManagementData getRiskRangeData(AtlassianHostUser hostUser, String projectChosen,
			Map<String, String> userDateRange, HttpSession session) {

		Map<String, String> projkeys = projectService.getProjectsKeys();

		String pkey2 = projkeys.get(projectChosen);

		CompletableFuture<Map<String, String>> acceptableLevelResults = riskDateRangeService
				.getAcceptablePercentage(hostUser, projectChosen, userDateRange, session);

		CompletableFuture<Map<String, ArrayList<RiskSummaryData>>> criticalDeliveryIdentified = riskDateRangeService
				.getCriticalDeliveryIntified(hostUser, projectChosen, userDateRange, session);

		CompletableFuture<Map<String, String>> riskOccurrenceResults = riskDateRangeService
				.getRiskOccurenceDetails(hostUser, userDateRange, projectChosen, session);

		CompletableFuture<Map<String, ResultRangeData>> exposureRatingResults = riskDateRangeService
				.getExposureRatingValues(hostUser, projectChosen, userDateRange, session);

		CompletableFuture
				.allOf(acceptableLevelResults, criticalDeliveryIdentified, riskOccurrenceResults, exposureRatingResults)
				.join();

		RiskManagementData riskManageData = new RiskManagementData();

		try {
			riskManageData.setCriticalRiskCount(criticalDeliveryIdentified.get());
			riskManageData.setRiskAcceptablePercentage(acceptableLevelResults.get());
			riskManageData.setRiskOccurenceCount(riskOccurrenceResults.get());
			riskManageData.setExposureValues(exposureRatingResults.get());
		} catch (Exception e) {
			System.out.println(e);
		}

		return riskManageData;
	}

}

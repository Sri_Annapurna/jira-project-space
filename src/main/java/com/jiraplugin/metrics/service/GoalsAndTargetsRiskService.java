package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.GoalsAndTargetsData;

@Service
public class GoalsAndTargetsRiskService {
	@Autowired JiraCustomFieldMap j;
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	Map<String, String> fieldList = new HashMap<String, String>();
	public String getGoalsAndTargetsRisk(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		fieldList = j.getJiraFieldList(hostUser, session);
	//	ArrayList<GoalsAndTargetsData> targets = new ArrayList<GoalsAndTargetsData>();
		String Risktarget=new String();
		String project = "Project Goals and Targets";
		String RiskTarget="Improve Delivery Management Effectiveness";	
	try{
		String url3="/rest/api/2/search?jql=project={p} AND (issuetype={RiskTarget})";
		String response3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url3).buildAndExpand("\"" + p + "\"", "\"" + RiskTarget + "\"").toUri(), String.class);
		JSONObject response4 = new JSONObject(response3);
		int count3 = response4.getInt("total");  
		if(count3 != 0) {
			
			String riskExpoTarget = fieldList.get("Delivery Risk Exposure Rating – After Risk Treatment(<=)");
			JSONArray jsonArray = response4.getJSONArray("issues");
				JSONObject j = jsonArray.getJSONObject(0);
				JSONObject fields = j.getJSONObject("fields");
				JSONObject category = fields.getJSONObject((riskExpoTarget));
				Risktarget= category.getString("value");
			//	targets.add(goalsandtargetData);
				
			}
		else{
			String pro_url3="/rest/api/2/search?jql=project={project} AND (issuetype={RiskTarget})";
			String pro_response3 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(pro_url3).buildAndExpand("\"" + project + "\"", "\"" + RiskTarget + "\"").toUri(), String.class);
			JSONObject pro_response4 = new JSONObject(pro_response3);
			int pro_count3 = pro_response4.getInt("total");  
			if(pro_count3 != 0) {
				String riskExpoTarget = fieldList.get("Delivery Risk Exposure Rating – After Risk Treatment(<=)");
				JSONArray jsonArray = pro_response4.getJSONArray("issues");

					JSONObject j = jsonArray.getJSONObject(0);
					JSONObject fields = j.getJSONObject("fields");
					JSONObject category = fields.getJSONObject((riskExpoTarget));
					Risktarget= category.getString("value");
			//		targets.add(goalsandtargetData);
				}
		}
		
		}catch(Exception e) {
			es.writeException(e,"error");
	}
return Risktarget;
}
}

package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskRangeData;
import com.jiraplugin.metrics.model.RiskRating;
import com.jiraplugin.metrics.model.RiskSummaryData;

@Service
public class RiskManagementDateRangeService extends RiskService {
	@Value("${riskAssessmentValue}")
	String riskAsessValue;

	@Value("${Treated_RiskValue}")
	String filterTreatedRiskValue;

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	

	@Value("${Total_Treated_value}")
	String filterTotalTreatedValue;



	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	final static String Response_Issue = "issues";
	final static String Response_Field = "fields";

	@Autowired
	private AtlassianHostRestClients restClients;

	
	Logger logger = LoggerFactory.getLogger(RiskManagementDateRangeService.class);

	@Async
	public CompletableFuture<Map<String, String>> getAcceptablePercentage(AtlassianHostUser hostUser,
			String snapProject, Map<String, String> userDateRange, HttpSession session) {
		
		
	

		String totalTreatedUrl = searchIdentifiedUrl.concat(filterTotalTreatedValue);

	

		JSONArray initialArray = new JSONArray();

		int initialTotal = 0;
		int count;
		int startAtCount = 0;

		do {
			String totalTreatedRiskResponse = restClients.authenticatedAs(hostUser)
					.getForObject(UriComponentsBuilder.fromUriString(totalTreatedUrl)
							.buildAndExpand("\"" + snapProject + "\"", "\"" + riskAsessValue + "\"",
									"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"",
									 "\"" + startAtCount + "\"")
							.toUri(), String.class);
			
			System.out.println(totalTreatedRiskResponse);

			JSONObject reponseObj = new JSONObject(totalTreatedRiskResponse);
			count = reponseObj.getInt("total");
			if (count != 0) {
				int issueMaxResult = reponseObj.getInt("maxResults");
				JSONArray tempReponseArray = reponseObj.getJSONArray("issues");
				for (int j = 0; j < tempReponseArray.length(); j++) {
					JSONObject objects = tempReponseArray.getJSONObject(j);
					initialArray.put(objects);
				}

				initialTotal = initialTotal + issueMaxResult;
				startAtCount = initialTotal;

			}
		} while (initialTotal < count);

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);

		Map<String, Double> totalTreatedMonthMap = new LinkedHashMap<>(initialMonthMap);

	
		
		

		for (int i = 0; i < initialArray.length(); i++) {

			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);

			String reponseDate = fields.getString(fieldList.get("Reported Date"));
			int riskValue = 0;
			
			try {
				
				riskValue = fields.getInt(fieldList.get("Residual Risk Value"));
			}catch(Exception e) {
				
				logger.error(e.getMessage());
			}
			
			
			
			String dateFormate = getConvertDate(reponseDate);

			if (riskValue >= 1 && riskValue <= 4) {
				initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
			} else {
				totalTreatedMonthMap.put(dateFormate, totalTreatedMonthMap.get(dateFormate) + 1);
			}

		}

		Map<String, String> tempMonthMap = new LinkedHashMap<>();

		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);

		for (int i = 0; i < initialMonthMap.size(); i++) {
			System.out.println(initialMonthMap.size());

			try {
				Double treatedV = (initialMonthMap.get(listForm.get(i)));

				Double totalTreatedV = initialMonthMap.get(listForm.get(i)) + totalTreatedMonthMap.get(listForm.get(i));

				Double result = (treatedV / totalTreatedV);

				Double resultValue = Math.floor(result * 100);

				String actualValue = "";

				if (resultValue.isNaN() || resultValue.equals(0.0)) {
					actualValue = "0";
				} else {

					actualValue = resultValue.toString();
				}

				tempMonthMap.put(listForm.get(i), actualValue);

			} catch (Exception e) {
				System.out.println(e + "0/0");
			}
		}

		return CompletableFuture.completedFuture(tempMonthMap);
	}

	private String getConvertDate(String reponseDate) {

		String sdate = "";
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date responsd = formatter.parse(reponseDate);

			SimpleDateFormat formatter1 = new SimpleDateFormat("MMM-yy");
			sdate = formatter1.format(responsd);

		} catch (Exception e) {

			logger.error(e.getMessage());

		}

		return sdate;
	}

	public Map<String, Double> getMonthListFromDate(Map<String, String> userDateRange) {

		Map<String, Double> dates = new LinkedHashMap<>();

		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Date sdate = formatter.parse(userDateRange.get("d1"));
			Date edate = formatter.parse(userDateRange.get("d2"));
			Date start = sdate;
			Date end = edate;

			while (start.compareTo(end) <= 0) {
				dates.put(new SimpleDateFormat("MMM-yy").format(start), 0.0);
				start = DateUtils.addMonths(start, 1);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return dates;
	}

	public ArrayList<String> getMonthListFromDateListForm(Map<String, String> userDateRange) {

		ArrayList<String> listDates = new ArrayList<>();

		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Date sdate = formatter.parse(userDateRange.get("d1"));
			Date edate = formatter.parse(userDateRange.get("d2"));
			Date start = sdate;
			Date end = edate;

			while (start.compareTo(end) <= 0) {
				listDates.add(new SimpleDateFormat("MMM-yy").format(start));
				start = DateUtils.addMonths(start, 1);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return listDates;
	}

	@Value("${filter_Identified_Url}")
	String filterIdentifiedUrl;

	@Async
	public CompletableFuture<Map<String, ArrayList<RiskSummaryData>>> getCriticalDeliveryIntified(AtlassianHostUser hostUser,
			String snapProject, Map<String, String> userDateRange, HttpSession session) {
		
	

		
		String identifiedUrl = searchIdentifiedUrl.concat(filterIdentifiedUrl);

		String totalIndetifiedResponse = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder.fromUriString(identifiedUrl)
								.buildAndExpand("\"" + snapProject + "\"", "\"" + userDateRange.get("d1") + "\"",
										"\"" + userDateRange.get("d2") + "\"")
								.toUri(),
						String.class);

		return CompletableFuture
				.completedFuture(getCriticalCountMethod(totalIndetifiedResponse, userDateRange, hostUser, session));
	}

	private Map<String, ArrayList<RiskSummaryData>> getCriticalCountMethod(String totalIndetifiedResponse,
			Map<String, String> userDateRange, AtlassianHostUser hostUser, HttpSession session) {
		
		System.out.println(totalIndetifiedResponse);

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);
		
		RiskSummaryData riskSummaryData;
		ArrayList<RiskSummaryData> riskSummaryArray;
		Map<String, ArrayList<RiskSummaryData>> actualResult = new LinkedHashMap<>();

		JSONObject initialResponse = new JSONObject(totalIndetifiedResponse);
		JSONArray initialArray = initialResponse.getJSONArray(Response_Issue);
		
		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		
		for(int i = listForm.size() - 1; i >= 0; i--) {
			
			actualResult.put(listForm.get(i), new ArrayList<RiskSummaryData>());
		}
		
		for (int i = 0; i < initialArray.length(); i++) {

			riskSummaryData = new RiskSummaryData();
			riskSummaryArray = new ArrayList<>();
			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);
			
			

			String reponseDate = fields.getString(fieldList.get("Reported Date"));
	
			
			String issueKeyValue = issueArray.getString("key");
			
			
			
			
			String riskProbabilityValue = 	getJsonValue(fields, fieldList, "Risk Probability (RP)").toString();
		
	
		
			int checkNum = Integer.parseInt(riskProbabilityValue);
			String dateFormate = getConvertDate(reponseDate);

			if (checkNum >= 3) {
				
				initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
				
				riskSummaryData.setIssueKey(issueKeyValue);	
				riskSummaryData.setRiskDescription(getJsonValue(fields, fieldList, "Risk Description").toString());
				riskSummaryData.setRiskProbability(riskProbabilityValue);
				riskSummaryData.setBusinessImpact(getJsonValue(fields, fieldList, "Business Impact (BI)").toString());
				riskSummaryData.setRiskValue(getJsonValue(fields, fieldList, "Risk Value").toString());
				riskSummaryData.setBusinessImpactDes(getJsonValue(fields, fieldList, "Business Impact Description").toString());
				riskSummaryData.setRiskTreatmentOp(getJsonValue(fields, fieldList, "Risk Treatment Option").toString());
				riskSummaryData.setRiskAction(getJsonValue(fields, fieldList, "Risk Treatment Action / Control").toString());
				
				if(actualResult.get(dateFormate) != null) {
					riskSummaryArray = actualResult.get(dateFormate);
					riskSummaryArray.add(initialMonthMap.get(dateFormate).intValue() -1, riskSummaryData);
				}else {
					riskSummaryArray.add(initialMonthMap.get(dateFormate).intValue() -1, riskSummaryData);
				}
				
				
				actualResult.put(dateFormate, riskSummaryArray);
			
			}
			
		
			
		}
		

		return actualResult;
	}

	private Object getJsonValue(JSONObject fields, Map<String, String> fieldList, String tarValue) {
		
			String resultString = "0";
			int resultNumber = 0;
			int flag = 0;
		try {
			
			Object obj = fields.opt(fieldList.get(tarValue));
			if(obj instanceof JSONObject ) {
				resultString =  fields.getJSONObject(fieldList.get(tarValue)).get("value").toString();
			}else if(obj instanceof String) {
				resultString =  fields.opt(fieldList.get(tarValue)).toString();
			}else if(obj instanceof Integer || obj instanceof Double) {
				resultNumber = fields.optInt(fieldList.get(tarValue));
				flag = 1;
			}			
		}catch(NullPointerException en) {
			logger.error(en.getMessage());			
		}
		if(flag == 1) {
	
		return resultNumber;
		}else {
			return resultString;
		}
	}

	@Value("${Filter_Risk_Occurence}")
	String filterOccurence;

	@Async
	public CompletableFuture<Map<String, String>> getRiskOccurenceDetails(AtlassianHostUser hostUser,
			Map<String, String> userDateRange, String projectChosen, HttpSession session) {

		
		
		String occurenceUrl = searchIdentifiedUrl.concat(filterOccurence);

		String occurenceResponse = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(occurenceUrl).buildAndExpand("\"" + projectChosen + "\"",
						"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"").toUri(),
				String.class);

		return CompletableFuture.completedFuture(getOccurenceMethod(occurenceResponse, userDateRange, hostUser, session));
	}

	private Map<String, String> getOccurenceMethod(String occurenceResponse, Map<String, String> userDateRange,
			AtlassianHostUser hostUser, HttpSession session) {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);

		JSONObject initialResponse = new JSONObject(occurenceResponse);
		JSONArray initialArray = initialResponse.getJSONArray(Response_Issue);
		for (int i = 0; i < initialArray.length(); i++) {

			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);

			String reponseDate = fields.getString(fieldList.get("Risk Occurrence Date"));

			String dateFormate = getConvertDate(reponseDate);

			initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
		}
		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		Map<String, String> tempMonthMap = new LinkedHashMap<>();

		for (int i = 0; i < initialMonthMap.size(); i++) {

			String actualValue = "";

			if (initialMonthMap.get(listForm.get(i)).isNaN() || initialMonthMap.get(listForm.get(i)).equals(0.0)) {
				actualValue = "0";
			} else {

				Integer conVert = initialMonthMap.get(listForm.get(i)).intValue();
				actualValue = conVert.toString();
			}

			tempMonthMap.put(listForm.get(i), actualValue);
		}

		return tempMonthMap;
	}
	

	@Async
	public CompletableFuture<Map<String, ResultRangeData>> getExposureRatingValues(AtlassianHostUser hostUser,
			String snapProject, Map<String, String> userDateRange, HttpSession session) {

		String rValue = "", residualRValue = "";

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		

		String url ="/rest/api/2/search?jql=project={project} AND issuetype='" + riskAsessValue + "'  AND 'Actual Date' != null AND 'Reported Date'>= {date} & 'Reported Date'<= {date}&startAt=0&maxResults=100";
		String response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url)
						.buildAndExpand("\"" + snapProject + "\"",
								"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"")
						.toUri(),
				String.class);
		JSONObject jsonObj = new JSONObject(response);
		JSONArray jsonArray = jsonObj.getJSONArray("issues");
		int count = jsonObj.getInt("total");
		int maxResults = jsonObj.getInt("maxResults");
		if (count > maxResults) {
			url ="/rest/api/2/search?jql=project={project} AND issuetype='" + riskAsessValue + "'  AND 'Actual Date' != null AND 'Reported Date'>= {date} & 'Reported Date'<= {date}&startAt=100&maxResults=100";
			String response1 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url)
					.buildAndExpand("\"" + snapProject + "\"",
							"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"")
					.toUri(), String.class);
			parseArray(jsonArray, response1);
		}
		
		ArrayList<RiskRangeData> riskData = new ArrayList<>();
		RiskRating fielData = new RiskRating();
		
		try {
			parseAvgRiskValue(fieldList, jsonArray, riskData, fielData);
		} catch (ParseException e) {
			logger.error(e.getMessage());
			
		}
		
		
		Map<String, ResultRangeData> result;

		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		result = calAverage(fielData, listForm);
		return CompletableFuture.completedFuture(result);

	}

	public Map<String, ResultRangeData> calAverage(RiskRating data, List<String> listForm) {

		List<String> tempMapValues = listForm;
		ArrayList<ResultRangeData> result = new ArrayList<>();
		List<RiskRangeData> fieldsData;
		Map<String, ResultRangeData> actualResultValue = new LinkedHashMap<>();
		String month;

		fieldsData = data.getFields();

		for (int j = 0; j < tempMapValues.size(); j++) {
			
			month = tempMapValues.get(j);
			ResultRangeData avgresult = getAveragesRiskValue(fieldsData, month);
			actualResultValue.put(month, avgresult);
			result.add(avgresult);
		}
		return actualResultValue;
	}


}

package com.jiraplugin.metrics.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ErrorService {
	Logger logger = LoggerFactory.getLogger(ErrorService.class);
	public void writeException(Exception e, String project) throws IOException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();  
		String userDir = System.getProperty("user.dir");
		File file = new File(userDir+ "\\src\\main\\resources\\log"+ df.format(now)+ ".txt");
		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bw = new BufferedWriter(fw);
		try {
		String errorContent = "Error/Exception for selecting project "+ project + " at " + dtf.format(now)+ ":";
		bw.write("===================================== " + errorContent + " =================================");
		bw.newLine();
		PrintWriter pw = new PrintWriter(bw);
		e.printStackTrace(pw);
		pw.close();
		}catch(Exception ex) {
			logger.error(ex.getMessage());
		}finally {
			if(bw != null) {
				bw.close();
			}
		}
	}
}

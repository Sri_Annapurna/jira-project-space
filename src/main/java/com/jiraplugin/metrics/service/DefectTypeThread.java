package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DefectTypeData;
import com.jiraplugin.metrics.model.EffectivenessData;

@Service
public class DefectTypeThread {
	@Autowired 
	IndividualDefectCount t;
	@Async
	public CompletableFuture<DefectTypeData> getTypeCount(String p, String type,int sDate,@AuthenticationPrincipal AtlassianHostUser hostUser) throws JSONException, ParseException, InterruptedException, ExecutionException {
		DefectTypeData data = new DefectTypeData();
		CompletableFuture<Integer> t0 = t.getValues(p,type, sDate, "High", hostUser);
		
		CompletableFuture<Integer> t1 = t.getValues(p,type, sDate, "Medium", hostUser);
		
		CompletableFuture<Integer> t2 = t.getValues(p,type, sDate, "Low", hostUser);
		
		CompletableFuture.allOf(t0,t1,t2).join();
		
	
		data.sethCount(t0.get());
		data.setmCount(t1.get());
		data.setlCount(t2.get());
		data.settCount(t0.get() + t1.get() + t2.get());
		return CompletableFuture.completedFuture(data);
	}

}

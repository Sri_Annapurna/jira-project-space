package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
@Service
public class RequestComplianceService {
	Logger logger = LoggerFactory.getLogger(RequestComplianceService.class);
	@Value("${individual_issue_date_range_url}")
	String url;
	@Value("${miscellaneousTask}")
	String mTask;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired
	JiraCustomFieldMap j;
	@Async
	public CompletableFuture<Map<String, Integer>> getComplianceData(String p,String type,String sDate, String eDate, @AuthenticationPrincipal AtlassianHostUser hostUser, List<String> dates,HttpSession session) throws JSONException, ParseException {
		Map<String, Integer> map = new HashMap<String, Integer>();
		Map<String, String> fieldMap = j.getJiraFieldList(hostUser, session);
		String url1 = "";
		String response = "";
		try{
			if(type.equals("Task")) {
				url1 = "/rest/api/2/search?jql=project={p} AND  (issuetype={type} OR issuetype={type}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate})";
			    response = restClients.authenticatedAs(hostUser).getForObject(
						UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"","\"" + type + "\"","\"" + mTask + "\"", "\"" + sDate + "\"","\"" + eDate + "\"").toUri(), String.class);
			    }
			else {
				url1 = "/rest/api/2/search?jql=project={p} AND  (issuetype={type}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate})";
				response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"","\"" + type + "\"", "\"" + sDate + "\"","\"" + eDate + "\"").toUri(), String.class);
			}
		JSONObject response1 = new JSONObject(response);
		JSONArray issues = response1.getJSONArray("issues");
		
		for(String d: dates) {
			map.put(d, 0);
		}
		int length = issues.length();
		for(int i = 0; i < length; i++) {
			JSONObject data = issues.getJSONObject(i);
			JSONObject fields = data.getJSONObject("fields");
			String date = fields.get(fieldMap.get("Reported Date")).toString();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
			Date rdate = formatter.parse(date);
			String key = new SimpleDateFormat("MMM-yy").format(rdate); 
			map.put( key , map.get(key) + 1 );
		}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return CompletableFuture.completedFuture(map);
	}
	
	@Autowired ClientSatisfactionThread cst;

	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	@Value("${corrective_improvement_actions}")
	String creative_issue;
	@Async
	public CompletableFuture<LinkedHashMap<String,ArrayList<ClientSatisfactionFA>>> getCidiData(String p,String type,String sDate, String eDate, @AuthenticationPrincipal AtlassianHostUser hostUser, ArrayList<String> dates, HttpSession session) throws JSONException, ParseException {
		LinkedHashMap<String, ArrayList<ClientSatisfactionFA>> map = new LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>();
		System.out.println("sDate-" + sDate + "endDAte" + eDate);
		Map<String, String> fieldMap = jiraFieldList.getJiraFieldList(hostUser, session);
		try {
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		String url = "/rest/api/2/search?jql=project={p} AND  (issuetype={issuetype}) AND 'Category (CI/DI)'={type} AND ('Reported Date'>= {sDate} AND 'Reported Date'<= {eDate})";
		String response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"","\"" + creative_issue + "\"","\"" + type + "\"", "\"" + sDate + "\"","\"" + eDate + "\"").toUri(), String.class);
		JSONObject response1 = new JSONObject(response);
		JSONArray issues = response1.getJSONArray("issues");
		
		for(String d: dates) {
			ArrayList<ClientSatisfactionFA> data = new ArrayList<ClientSatisfactionFA>();
			map.put(d, data);
		}
		int length = issues.length();
		for(int i = 0; i < length; i++) {
			JSONObject data = issues.getJSONObject(i);
			JSONObject fields = data.getJSONObject("fields");
			ClientSatisfactionFA cs = cst.getParsedData(fieldList, data);
			String date = fields.get(fieldMap.get("Reported Date")).toString();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
			Date rdate = formatter.parse(date);
			String key = new SimpleDateFormat("MMM-yy").format(rdate);
			ArrayList<ClientSatisfactionFA> c = map.get(key);
			c.add(cs);
			map.put( key , c);
		}
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
		return CompletableFuture.completedFuture(map);
	}
	
}

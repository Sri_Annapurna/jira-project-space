package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.FieldCustomID;
import com.jiraplugin.metrics.model.ProjectData;
import com.jiraplugin.metrics.model.ProjectsData;

@Service
public class ProjectService {

	static int FiledCutFlag = 0;
	static Map<String, String> customFieldIDMap = new LinkedHashMap<String, String>();


	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private JiraCustomFieldMap jiraCust;

	public ArrayList<String> getProjects(AtlassianHostUser hostUser) {

		ProjectsData[] response = restClients.authenticatedAs(hostUser).getForObject("/rest/api/2/project",
				ProjectsData[].class);
                ArrayList<String> list =new ArrayList<String>();
		List<String> plist = new ArrayList<String>();
		for (int i = 0; i < response.length; i++) {

			plist = Arrays.asList(response[i].getName());
			list.addAll(plist);
		}

		list.remove("COMMON RISKS (DELIVERY)");
		Collections.sort(plist);
		return list;

	}

	public Map<String, String> getProjectsKeys() {

		ProjectsData[] response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		return projectkeysMap;

	}
	
	public Map<String, String> getProjectsKeysAsync(AtlassianHostUser hostUser) {

		ProjectsData[] response = restClients.authenticatedAs(hostUser).getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		return projectkeysMap;

	}
	

	@Async
	public CompletableFuture<Integer> getFieldKeys(AtlassianHostUser hostUser) {

		String FieldJsonURL = "/rest/api/2/field";

		FieldCustomID[] Result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,
				FieldCustomID[].class);

		for (int i = 0; i < Result.length; i++) {

			customFieldIDMap.put(Result[i].getName(), Result[i].getKey());

		}

		FiledCutFlag = 1;

		return CompletableFuture.completedFuture(Integer.valueOf(1));

	}

	
	
	@Value("${SnapUserName}")
	String userName;
	
	@Value("${PassWord}")
	String passWord;
	
	@Value("${DomainSuffix}")
	String doMain;
	
	
	
	public Map<String, String> getProjectWebhook(AtlassianHostUser hostUser) {

		
		// https://seneca-global.atlassian.net/rest/api/2/project

		ProjectsData[] response = restClients.authenticatedAs(hostUser).getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		return projectkeysMap;

	}

	public Map<String, Integer> getProjectsIds() {

		ProjectsData[] response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, Integer> projectIdsMap = new LinkedHashMap<String, Integer>();

		for (int i = 0; i < response.length; i++) {

			projectIdsMap.put(response[i].getName(), Integer.parseInt(response[i].getId()));

		}
		return projectIdsMap;

	}

	public Map<String, String> getProjectModes() throws JSONException {

		Map<String, String> modes = new LinkedHashMap<String, String>();
		String type;
		String response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project", String.class);
		JSONArray response1 = new JSONArray(response);
		for (int i = 0; i < response1.length(); i++) {
			JSONObject jobj = response1.getJSONObject(i);
			String id = jobj.getString("id");
			try {
				JSONObject pct = jobj.getJSONObject("projectCategory");
				type = pct.getString("name");
			} catch (Exception e) {
				type = "none";
			}
			modes.put(id, type);
		}
		return modes;
	}

	/* @Scheduled(fixedRate = 5000) */
	public Map<Integer, Integer> getBoardIds() throws JSONException {
		int initialTotal = 0;
		int startAtCount = 0;
		int total=0;
		JSONArray finalJSONArray = new JSONArray();
		do{
		String response = restClients.authenticatedAsHostActor().getForObject(
                UriComponentsBuilder.fromUriString("/rest/agile/1.0/board?startAt={startAt}")
                .buildAndExpand(startAtCount).toUri(),String.class);
		 
		JSONObject jobj = new JSONObject(response);
		total = jobj.getInt("total"); 
		JSONArray jarr = jobj.getJSONArray("values");
		for (int i = 0; i < jarr.length(); i++) {
		    finalJSONArray.put(jarr.getJSONObject(i));
			}
		
		int issueMaxResult = jobj.getInt("maxResults");
		initialTotal = initialTotal + issueMaxResult;
		startAtCount=initialTotal;
		}while(initialTotal < total);
		
		Integer projectId;
		Map<Integer, Integer> projectBoardIds = new LinkedHashMap<Integer, Integer>();

		for (int i = 0; i < finalJSONArray.length(); i++) {
			JSONObject j = finalJSONArray.getJSONObject(i);
			Integer id = j.getInt("id");

			try {
				JSONObject location = j.getJSONObject("location");
				projectId = location.getInt("projectId");
			} catch (Exception e) {
				projectId = 0;
			}
			if (projectBoardIds.containsKey(projectId) == false) {
				projectBoardIds.put(projectId, id);
			}
		}
		return projectBoardIds;
	}

	/*public Map<String, ArrayList<ProjectData>> getProjectData() throws JSONException, IOException {
		Map<String, ArrayList<ProjectData>> userProjectDetails = new HashMap<>();
		String userDir = System.getProperty("user.dir");
		String response = "[{'name': '557058:d4904e9e-68aa-4659-9ad3-f5a0b7ddb842', 'projects': [{'projectName': 'Software Development Project', 'id': '10104','category': 'Software Development'},{'projectName': 'Software Maintenance Release Project','id': '10004','category': 'Software Maintenance Release Mode'},{'projectName': 'Software Maintenance Work Request Project','id': '10100','category': 'Software Maintenance Work Request Mode'}]}]";
		JSONArray jarr = new JSONArray(response);
		for (int i = 0; i < jarr.length(); i++) {
			ArrayList<ProjectData> pd = new ArrayList<ProjectData>();
			JSONObject j = jarr.getJSONObject(i);
			String uName = j.get("name").toString();
			JSONArray projectList = j.getJSONArray("projects");
			int projectListSize = projectList.length();
			for(int k = 0; k < projectListSize; k++) {
				JSONObject jobj = projectList.getJSONObject(k);
				String projectName = jobj.get("projectName").toString();
				String id = jobj.get("id").toString();
				String category = jobj.get("category").toString();
				ProjectData pData = new ProjectData();
				pData.setId(id);
				pData.setCategory(category);
				pData.setName(projectName);
				pd.add(pData);
			}
			userProjectDetails.put(uName, pd);
		}
		return userProjectDetails;
	}

}*/
	
	public  ArrayList<ProjectData> getProjectData() throws JSONException, IOException {
		String response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project", String.class);
		JSONArray jarr = new JSONArray(response);
		ArrayList<ProjectData> userProjectDetails = new ArrayList<ProjectData>();
		int length = jarr.length();
		for (int i = 0; i < length; i++) {
				ProjectData projectData = new ProjectData();
				JSONObject jobj = jarr.getJSONObject(i);
				String projectName = jobj.get("name").toString();
				
				if(!(projectName.equals("COMMON RISKS")) && !(projectName.equals("Project Goals and Targets")) && !(projectName.equals("SNAPSHOT"))) {
				projectData.setName(projectName);				
				String id = jobj.get("id").toString();
				projectData.setId(id);
				String projectCategory = "-";
				if(jobj.has("projectCategory")){
			//	try {
					JSONObject categoryJsonObj = jobj.getJSONObject("projectCategory");
				projectCategory = categoryJsonObj.get("name").toString();
				}
				
				/*catch(Exception e) {
					e.printStackTrace();
				}
			*/	projectData.setCategory(projectCategory);
				userProjectDetails.add(projectData);
				}
			}
		return userProjectDetails;
	}

}
	
	
	

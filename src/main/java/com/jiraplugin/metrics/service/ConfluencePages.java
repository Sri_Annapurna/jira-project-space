package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ConfluencePagesData;

@Service
public class ConfluencePages {
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	@Value("${confluence_pages}")  String confluence;
		public CompletableFuture<ConfluencePagesData> getConfluencePages(AtlassianHostUser hostUser,String p, HttpSession session) throws IOException{
			ConfluencePagesData cpd = new ConfluencePagesData();
			try{
				JSONObject JsonObject=new JSONObject();
				
				String url="/rest/api/2/search?jql=project={p} AND (issuetype={confluence})";
				String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + confluence + "\"").toUri(), String.class);
				JSONObject response1 = new JSONObject(response);
				int count = response1.getInt("total"); 
				if(count!=0){
					JSONArray jsonArray = response1.getJSONArray("issues");
					JsonObject=jsonArray.getJSONObject(0);
					String key=JsonObject.get("key").toString();
					
					System.out.println("KEY OF THE CONFLUENCE PAGE ISSUE"+key);
					
					String confluenceURL = "/rest/api/latest/issue/" + key +"/remotelink";
					String newResponse = restClients.authenticatedAs(hostUser).getForObject(confluenceURL, String.class);
					JSONArray newResponse1 = new JSONArray(newResponse);
				//	System.out.println(newResponse1);
					
					for(int i=0;i<newResponse1.length();i++){
						JSONObject j=newResponse1.getJSONObject(i);
						JSONObject object = j.getJSONObject("object");
						if(((object.get("title").toString()).toLowerCase()).contains("about")){
							cpd.setAtpURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("technology")){
							cpd.setTeURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("client")){
							cpd.setClientURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("network")){
							cpd.setNetworkURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("configuration")){
							cpd.setConfigurationURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("process")){
							cpd.setProcessURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("retro")){
							cpd.setRetroURL(object.get("url").toString());
						}
						else if(((object.get("title").toString()).toLowerCase()).contains("mom")){
							cpd.setMomURL(object.get("url").toString());
						}
						
						
					}
					
					
				}
				
			}
			catch(Exception e){
				System.out.println(e);
			}
			
			return CompletableFuture.completedFuture(cpd);
		}
}

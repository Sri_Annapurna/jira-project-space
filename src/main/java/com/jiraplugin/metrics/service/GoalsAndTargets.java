package com.jiraplugin.metrics.service;

import java.util.ArrayList;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AllGoalsData;
import com.jiraplugin.metrics.model.GoalsAndTargetsData;
@Service
public class GoalsAndTargets {
	
	@Autowired
	public GoalsAndTargetsService goalsAndtargetsService;
	public CompletableFuture<AllGoalsData> getgoalsandtargets(AtlassianHostUser hostUser,String p, HttpSession session) throws Exception{
		AllGoalsData goalsandtargets =new AllGoalsData();
		
		CompletableFuture<ArrayList<Double>> csat = goalsAndtargetsService.getGoalsAndTargetsCSAT(hostUser, p, session);
		
		CompletableFuture<GoalsAndTargetsData> de= goalsAndtargetsService.getGoalsAndTargetsData(hostUser, p, session);
		
		CompletableFuture<ArrayList<Double>> deliveryManagement= goalsAndtargetsService.getGoalsAndTargetsRisk(hostUser, p, session);
		
		CompletableFuture<Double> cidi= goalsAndtargetsService.getGoalsAndTargetsCIDI(hostUser, p, session);
		
		CompletableFuture.allOf(csat, de, deliveryManagement,cidi);
		
		goalsandtargets.setCsatRatingTarget(csat.get().get(0));
		goalsandtargets.setClientComplaintsTarget(csat.get().get(1));
		goalsandtargets.setRecurrenceComplaintTarget(csat.get().get(2));
		goalsandtargets.setDevelopmentEffectivenessTarget(de.get());
		goalsandtargets.setRiskTarget(deliveryManagement.get().get(0));
		goalsandtargets.setReqDelOnTimeTarget(deliveryManagement.get().get(1));
		goalsandtargets.setCidiTarget(cidi.get());
		
		
	
		return CompletableFuture.completedFuture(goalsandtargets);
		
	}
}
package com.jiraplugin.metrics.service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.JsonData;


@Service
public class JiraLookupService {

	@Autowired
	private AtlassianHostRestClients restClients;


	
	@Value("${riskAssessmentValue}")
	String riskAssessmentType;
	

	
	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	
	
	@Async
	public CompletableFuture<Double> findCount(AtlassianHostUser hostUser, String sce, int dce, HttpSession session)
			throws InterruptedException {
		Map<String, Integer> riskAcceptableMap = new LinkedHashMap<String, Integer>();
		Double dRTAL;
		try {
		String riskValue ="/rest/api/2/search?jql=project={p} AND issuetype= {riskAssessmentType} AND 'Residual Risk Value'>=1 AND 'Residual Risk Value'<=4 AND status was not 'Not Applicable' on endOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Actual Date' != null";
		String riskIndentified ="/rest/api/2/search?jql=project={p} AND issuetype= {riskAssessmentType} AND status was not 'Not Applicable' on endOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Actual Date' != null";

		
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		String p;
		String urlString = "";
		String urlName = "";
		int date;
		

		p = sce;
		date = dce;
		for (int i = 0; i < 2; i++) {

			if (i == 0) {
				urlString = riskValue;
				urlName = "riskValue";
			} else if (i == 1) {
				urlString = riskIndentified;
				urlName = "riskIndentified";
			}


			System.out.println("before call...!!!");

			JsonData responseEntity = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(urlString)
					.buildAndExpand("\"" + p + "\"", "\"" + riskAssessmentType + "\"", "\"" + date + "\"", "\"" + date + "\"")
					.toUri(), JsonData.class);
						
			System.out.println("after call...!!!" + urlName);

			riskAcceptableMap.put(urlName, responseEntity.getTotal());

		}

		System.out.println(riskAcceptableMap);
		}catch(Exception e) {
			e.printStackTrace();
		}
		dRTAL = (riskAcceptableMap.get("riskValue").doubleValue()
				/ riskAcceptableMap.get("riskIndentified").doubleValue()) * 100;
		if (dRTAL.isNaN()) {
			dRTAL = 0.0;
		}

		return CompletableFuture.completedFuture(dRTAL);
	}

}

package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ClientSatisfactionData;

@Service
public class ClientSatisfactionDateService {

	private static final String VALUE = "value";

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Value("${ClientSatisfactionSingle_IssueType}")
	String clientSatisfactionIT;

	@Value("${Quarter_Start_Date}")
	String qsd;

	@Value("${Client_SAT}")
	String clientSatisfactionFilterUrl;

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	Logger logger = LoggerFactory.getLogger(ClientSatisfactionDateService.class);

	public Map<String, ClientSatisfactionData> getClientRating(AtlassianHostUser hostUser, String projectChosen,
			Map<String, String> userDateRange, HttpSession session) {

		List<ArrayList<String>> dateYearLink = getClientQuarter(userDateRange);

		String actualClientUrl = searchIdentifiedUrl.concat(clientSatisfactionFilterUrl);

		ArrayList<ClientSatisfactionData> cSATdata = new ArrayList<>();

		ClientSatisfactionData clientSatisfactionData;

		Map<String, ClientSatisfactionData> resultArray = new LinkedHashMap<>();

		int initialTotal = 0;
		int count = 1;
		int startAtCount = 0;

		while (initialTotal < count) {

			String response2 = restClients.authenticatedAs(hostUser)
					.getForObject(UriComponentsBuilder.fromUriString(actualClientUrl)
							.buildAndExpand("\"" + projectChosen + "\"", "\"" + clientSatisfactionIT + "\"",
									"\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + startAtCount + "\"")
							.toUri(), String.class);

			JSONObject response = new JSONObject(response2);
			count = response.getInt("total");
			int issueMaxResult = response.getInt("maxResults");

			try {
				cSATdata = parseMethod2(hostUser, response, session);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			initialTotal = initialTotal + issueMaxResult;

		}

		for (int i = 0; i < dateYearLink.size()-1; i++)   	//remove "-1" if we want 4 periods(including current period)
		{       

			int flag = 0;
			int lastElement = 0;

			for (int j = 0; j < cSATdata.size(); j++) 
			{ 
				lastElement++;

				try {

					if (dateYearLink.get(i).get(1).equals(cSATdata.get(j).getQuarter_Start_Date())
							&& cSATdata.get(j).getYear().contains(dateYearLink.get(i).get(0))) {

						flag = 1;

						resultArray.put(dateYearLink.get(i).get(1) + " " + dateYearLink.get(i).get(0), cSATdata.get(j));

						break;
					}

				} catch (Exception e) {
					logger.error(e.getMessage());

				}

			}
			if (flag == 0) {

				clientSatisfactionData = new ClientSatisfactionData();

				clientSatisfactionData.setCSAT_Q6("-");
				cSATdata.add(clientSatisfactionData);
				resultArray.put(dateYearLink.get(i).get(1) + " " + dateYearLink.get(i).get(0),
						cSATdata.get(lastElement));

			}

		}

		return resultArray;
	}

	public List<ArrayList<String>> getClientQuarter(Map<String, String> userDateRange)

	{

		ArrayList<ArrayList<String>> mainQuarters = new ArrayList<>();
		ArrayList<String> quarters;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat dfYY = new SimpleDateFormat("yyyy");

		Map<Integer, String> quaStr = new LinkedHashMap<>();
		quaStr.put(1, "Jan-Apr");
		quaStr.put(2, "May-Aug");
		quaStr.put(3, "Sep-Dec");

		try {
			Calendar cal = Calendar.getInstance();
			System.out.println(new JSONObject(userDateRange));
			cal.setTime(df.parse(userDateRange.get("d1")));

			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(df.parse(userDateRange.get("d2")));

			while (cal1.getTime().after(cal.getTime()) || cal1.getTime().equals(cal.getTime())) {
				int month = cal.get(Calendar.MONTH) + 1;

				int quarter = month % 4 == 0 ? (month / 4) : (month / 4) + 1;

				quarters = new ArrayList<>();

		//		quarters.add("Q" + quarter + "(" + quaStr.get(quarter) + ")"); // Q2(May-Aug)
				quarters.add(dfYY.format(cal.getTime()));
				quarters.add(quaStr.get(quarter));

				mainQuarters.add(quarters);

				cal.add(Calendar.MONTH, 4);
			}

		

		} catch (Exception e) {
			logger.error(e.getMessage());

		}

		return mainQuarters;
	}

	@Value("${CSAT_Value_Q1}")
	String cSATQD1;
	@Value("${CSAT_Value_Q2}")
	String cSATQD2;
	@Value("${CSAT_Value_Q3}")
	String cSATQD3;
	@Value("${CSAT_Value_Q4}")
	String cSATQD4;
	@Value("${CSAT_Value_Q5}")
	String cSATQD5;
	@Value("${CSAT_Value_Q6}")
	String cSATQD6;
	@Value("${CSAT_Value_Q7}")
	String cSATQD7;
	@Value("${CSAT_Value_Q8}")
	String cSATQD8;

	ArrayList<ClientSatisfactionData> parseMethod2(AtlassianHostUser hostUser, JSONObject response,
			HttpSession session) {
		ClientSatisfactionData clientSatisfactionData;
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);

		ArrayList<ClientSatisfactionData> cSATData = new ArrayList<>();
		String cn = fieldList.get("Client Name");
		String sd = fieldList.get("Sent Date");
		String rd = fieldList.get("Received Date");
		String quarterValue = fieldList.get("Period");
		String qed = fieldList.get("Year");
		String internalCsatRating = fieldList.get("Internal Client Satisfaction Rating");

		String[] queArray = { fieldList.get(cSATQD1), fieldList.get(cSATQD2), fieldList.get(cSATQD3),
				fieldList.get(cSATQD4), fieldList.get(cSATQD5), fieldList.get(cSATQD6), fieldList.get(cSATQD7),
				fieldList.get(cSATQD8) };

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			clientSatisfactionData = new ClientSatisfactionData();
			JSONObject fields = j.getJSONObject("fields");

			clientSatisfactionData.setClient_Name(fields.get(cn).toString());
			try {
				Date date1 = formatter.parse(fields.get(sd).toString());
				String finalDate1 = newFormat.format(date1);
				clientSatisfactionData.setSent_Date(finalDate1);
			} catch (Exception e) {
				clientSatisfactionData.setSent_Date("-");
			}
			try {
				Date date2 = formatter.parse(fields.get(rd).toString());
				String finalDate2 = newFormat.format(date2);
				clientSatisfactionData.setReceived_Date(finalDate2);
			} catch (Exception e) {
				clientSatisfactionData.setReceived_Date("-");
			}
			clientSatisfactionData.setQuarter_Start_Date(fields.getJSONObject(quarterValue).getString(VALUE));
			clientSatisfactionData.setYear(fields.get(qed).toString());

			clientSatisfactionData.setCSAT_Q1(checkException(fields, queArray[0]));

			clientSatisfactionData.setCSAT_Q2(checkException(fields, queArray[1]));

			clientSatisfactionData.setCSAT_Q3(checkException(fields, queArray[2]));

			clientSatisfactionData.setCSAT_Q4(checkException(fields, queArray[3]));

			clientSatisfactionData.setCSAT_Q5(checkException(fields, queArray[4]));

			clientSatisfactionData.setCSAT_Q6(checkExceptionForCsatRating(fields, queArray[5],internalCsatRating));

			try {clientSatisfactionData.setCSAT_Q7(fields.get(queArray[6]).toString());}catch(Exception e){clientSatisfactionData.setCSAT_Q7("null");}
			
			try {clientSatisfactionData.setCSAT_Q8(fields.get(queArray[7]).toString());}catch(Exception e){clientSatisfactionData.setCSAT_Q8("null");}

			checkNull(clientSatisfactionData);

			cSATData.add(clientSatisfactionData);

		}
		return cSATData;
	}

	public String checkException(JSONObject fie, String ar) {

		String result = "";
		try {

			result = fie.getJSONObject(ar).getString(VALUE).substring(0, 1);

		} catch (Exception e) {
			logger.error(e.getMessage());
			result = "null";
		}

		return result;
	}
	
	public String checkExceptionForCsatRating(JSONObject fie, String ar, String rating) {

		String result = "";
		try {

			result = fie.getJSONObject(ar).getString(VALUE).substring(0, 1);

		} catch (Exception e) {
			logger.error(e.getMessage());
			try{
				result = fie.getJSONObject(rating).getString(VALUE).substring(0, 1);
				result = result.concat("*");
			}catch(Exception e1){
				logger.error(e1.getMessage());
				result = "null";	
			}
			
		}

		return result;
	}

	private void checkNull(ClientSatisfactionData clientSatisfactionData) {

		String respondentSkipped = "Respondent skipped this question";

		if (clientSatisfactionData.getCSAT_Q1().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q1(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q2().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q2(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q3().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q3(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q4().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q4(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q5().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q5(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q6().trim().contentEquals("null")) {

			clientSatisfactionData.setCSAT_Q6(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q7().trim().contentEquals("null")
				|| clientSatisfactionData.getCSAT_Q7().trim().contentEquals("")) {

			clientSatisfactionData.setCSAT_Q7(respondentSkipped);
		}
		if (clientSatisfactionData.getCSAT_Q8().trim().contentEquals("null")
				|| clientSatisfactionData.getCSAT_Q8().trim().contentEquals("")) {
			clientSatisfactionData.setCSAT_Q8(respondentSkipped);
		}
	}

}

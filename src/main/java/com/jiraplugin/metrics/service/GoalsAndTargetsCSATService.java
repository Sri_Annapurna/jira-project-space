package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.GoalsAndTargetsData;

@Service
public class GoalsAndTargetsCSATService {
	@Autowired JiraCustomFieldMap j;
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	Map<String, String> fieldList = new HashMap<String, String>();
	public Double getGoalsAndTargetsCSAT(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException{
		fieldList = j.getJiraFieldList(hostUser, session);
	//	ArrayList<GoalsAndTargetsData> targets = new ArrayList<GoalsAndTargetsData>();
		Double CSATtarget = null;
	//	GoalsAndTargetsData goalsandtargetData = new GoalsAndTargetsData();
		String project = "Project Goals and Targets";
		String CSATTarget="Improve Client Satisfaction";
		try{
			String url2="/rest/api/2/search?jql=project={p} AND (issuetype={CSATTarget})";
			String response2 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url2).buildAndExpand("\"" + p + "\"", "\"" + CSATTarget + "\"").toUri(), String.class);
			JSONObject response3 = new JSONObject(response2);
			int count2 = response3.getInt("total");  
			if(count2 != 0) {
				String csatTarget = fieldList.get("Overall Client Satisfaction Rating(>=)");
				JSONArray jsonArray = response3.getJSONArray("issues");
			//	for(int i = 0; i < jsonArray.length(); i++) {
					JSONObject j = jsonArray.getJSONObject(0);
					JSONObject fields = j.getJSONObject("fields");
					CSATtarget= ((double)(fields.get(csatTarget)));
			//		targets.add(goalsandtargetData);
			//		}
				}
			else{
				String pro_url2="/rest/api/2/search?jql=project={project} AND (issuetype={CSATTarget})";
				String pro_response2 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(pro_url2).buildAndExpand("\"" + project + "\"", "\"" + CSATTarget + "\"").toUri(), String.class);
				JSONObject pro_response3 = new JSONObject(pro_response2);
				int pro_count2 = pro_response3.getInt("total");  
				if(pro_count2 != 0) {
					String csatTarget = fieldList.get("Overall Client Satisfaction Rating(>=)");
					JSONArray jsonArray = pro_response3.getJSONArray("issues");
				//	for(int i = 0; i < jsonArray.length(); i++) {
						JSONObject j = jsonArray.getJSONObject(0);
						JSONObject fields = j.getJSONObject("fields");
						CSATtarget=((double)(fields.get(csatTarget)));
				//		targets.add(goalsandtargetData);
					//	}
					}
			}
			
			}catch(Exception e) {
				es.writeException(e,"error");
		}
		return CSATtarget;
		
	}		
}
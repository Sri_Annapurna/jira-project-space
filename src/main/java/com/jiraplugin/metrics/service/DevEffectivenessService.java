package com.jiraplugin.metrics.service;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.MetricsData;

@Service
public class DevEffectivenessService {

	@Autowired
	private DefectsService defectsService;
	@Autowired
	private MonthlyIssues monthlyIssues;
	@Autowired
	ErrorService es;

	@Async
	public CompletableFuture<MetricsData> getMonthlyDefectsData(String project, int i,boolean isSprintWise, String sprintCompleteDate, AtlassianHostUser hostUser,HttpSession session)
			throws Exception {
		MetricsData data = new MetricsData();
		if(isSprintWise) {
			data.setMonth(project);
		}else {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		String month = (YearMonth.now().minusMonths(Math.abs(i))).format(formatter);
		data.setMonth(month);
		}
		try {
			CompletableFuture<List<Double>> reviewEffort1 = monthlyIssues.getMonthlyIssues(project, i,isSprintWise, hostUser,session);
			CompletableFuture<DEffectivenessData> defects = defectsService.defectsCount(project, i,isSprintWise,sprintCompleteDate, hostUser,session);
			CompletableFuture.allOf(reviewEffort1, defects);
			
			double totalReviewEffort = reviewEffort1.get().get(0);
			double reviewEffort = reviewEffort1.get().get(1);
			double testDesignReviewEffort = reviewEffort1.get().get(2) ;
			double totalTestable = reviewEffort1.get().get(3);
			
			
			double reviewEfficiency = 0;
			double revEffect = 0;
			double reviewEffectiveAdc = 0;
			double testEffect = 0;
			double dRE = 0;
			DEffectivenessData performanceMetrics = defects.get();
			
			
			int reviewdefectsCountDev = performanceMetrics.getRd_adc_count();
			data.setReviewDefectsAdc(reviewdefectsCountDev);
			int totalReviewDefectsCountDev = performanceMetrics.getrTCount();
			int testingdefectsCountDev = performanceMetrics.gettTCount();
			int totalTestDesignDefectsCountDev = performanceMetrics.getDesign_count();
			int deliverydefectsCountDev = performanceMetrics.getdTCount();
			int totalInprocessDefectsDev = totalReviewDefectsCountDev + testingdefectsCountDev;
			int inprocessDefectsDev = reviewdefectsCountDev + testingdefectsCountDev;
			
			if (inprocessDefectsDev != 0) {
				double reeffnssDev = (totalReviewDefectsCountDev * 100.0) / totalInprocessDefectsDev;
				revEffect = Math.round(reeffnssDev * 100.0) / 100.0;
				data.setRevef(String.valueOf(revEffect));
				double reviewEffectivenessAdcDev = (reviewdefectsCountDev * 100.0) / inprocessDefectsDev;
				reviewEffectiveAdc = Math.round(reviewEffectivenessAdcDev * 100.0) / 100.0;
				data.setReview_effectiveness_adc(String.valueOf(reviewEffectiveAdc));
				double teDev = (testingdefectsCountDev * 100.0) / totalInprocessDefectsDev;
				testEffect = Math.round(teDev * 100.0) / 100.0;
				data.setTesteff(String.valueOf(testEffect));
				double dreDev = (totalInprocessDefectsDev * 100.0) / (totalInprocessDefectsDev + deliverydefectsCountDev);
				dRE = Math.round(dreDev * 100.0) / 100.0;
				data.setDreff(String.valueOf(dRE));
			} else {
				data.setRevef("0.0");
				data.setReview_effectiveness_adc("0.0");
				data.setTesteff("0.0");
				data.setDreff("0.0");
				data.setReview_efficiency("0.0");
				if (deliverydefectsCountDev == 0)
					data.setDreff("0.0");
			}
			 if (totalReviewEffort == 0 || totalReviewDefectsCountDev == 0) {
				data.setTotal_review_efficiency("0.0");
			 }
			else {
				reviewEfficiency = totalReviewDefectsCountDev / totalReviewEffort;

				data.setTotal_review_efficiency(String.format("%.2f", reviewEfficiency));
			}
			if (reviewEffort == 0 ||reviewdefectsCountDev == 0)
				data.setReview_efficiency("0.0");
			else {
				reviewEfficiency = reviewdefectsCountDev / reviewEffort;

				data.setReview_efficiency(String.format("%.2f", reviewEfficiency));
			}
			data.setRevd(reviewdefectsCountDev);
			data.setTesd(testingdefectsCountDev);
			data.setDeld(deliverydefectsCountDev);
			data.setTotal_inprocess_defects(totalInprocessDefectsDev);
			data.setTotal_review_defects(totalReviewDefectsCountDev);
			data.setInpd(inprocessDefectsDev);
			data.setReview_effort(String.valueOf(reviewEffort));

			data.setTotal_review_effort(String.valueOf(totalReviewEffort));
			data.setTest_design_review_effort(testDesignReviewEffort);
			data.setTotal_test_design_defects(totalTestDesignDefectsCountDev);
			data.setTotal_testable(totalTestable);
			data.setD(performanceMetrics);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			es.writeException(e, project);
		}
		return CompletableFuture.completedFuture(data);

	}
}
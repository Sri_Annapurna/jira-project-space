package com.jiraplugin.metrics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CIDIData;

public class AspectService {
	@Value("${cidi_date_url}")
	String url;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Async
	public String createAspectThread(AtlassianHostUser hostUser,String project, String type, String category, String sDate, String eDate) {
		String response = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder
								.fromUriString(url).buildAndExpand("\"" + project + "\"",
										"\"" + type + "\"", "\"" + category + "\"", "\"" + sDate + "\"", "\"" + eDate + "\"")
								.toUri(),
						String.class);

		return response;
	}

}

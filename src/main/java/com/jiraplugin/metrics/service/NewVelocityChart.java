package com.jiraplugin.metrics.service;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.VelocityData;

import antlr.collections.List;

@Service
public class NewVelocityChart {
	@Autowired ProjectService ps;
	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;	
	
	
	
	@Async
	public CompletableFuture<VelocityData> getStoryPoints(int sprintid, String sprintName, String closedOn,String goal, AtlassianHostUser hostUser,HttpSession session){
		VelocityData velocityData = new VelocityData();
		int committedCount = 0;
		int completedCount=0;
		int initialTotal = 0;
		int startAtCount = 0;
		int committedStoryPoints=0;
		int completedStoryPoints=0;
		int committedTasks=0;
		int countAsCommitted=0;
	//	int completedTasks =0;int closedOnTime=0;
		int completedWithDueDate =0;
	
		JSONArray finalJSONArray = new JSONArray();
		JSONArray finalJSONArrayofClosedIssues = new JSONArray();
		JSONArray JSONArrayofClosedAndResolved = new JSONArray();
		try{
			
			SimpleDateFormat closedDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
	        Date closedDate = closedDateFormat.parse(closedOn);
			
			Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
			String storyPointsFieldID = fieldList.get("Story Points");
			String issueStatus;
		do{
		String url = "/rest/api/2/search?jql=Sprint={sprint_id}&startAt={startAt}&maxResults=100";
		String response = restClients.authenticatedAs(hostUser)
		        .getForObject(
		                UriComponentsBuilder.fromUriString(url)
		                        .buildAndExpand(sprintid,startAtCount).toUri(),String.class);	
		JSONObject response1 = new JSONObject(response);
		committedCount = response1.getInt("total");  
		
		JSONArray jsonArrayOfIssues = response1.getJSONArray("issues");
		for (int i = 0; i < jsonArrayOfIssues.length(); i++) {
		    finalJSONArray.put(jsonArrayOfIssues.getJSONObject(i));
		}
		
		int issueMaxResult = response1.getInt("maxResults");
		initialTotal = initialTotal + issueMaxResult;
		startAtCount=initialTotal;
		}while(initialTotal < committedCount);
		
		if(committedCount!=0){
			for(int i=0;i<finalJSONArray.length();i++){
				JSONObject j = finalJSONArray.getJSONObject(i);
				JSONObject fields = j.getJSONObject("fields");
				if(!fields.get("duedate").toString().equals("null"))
					{
						String due = fields.getString("duedate");
						SimpleDateFormat dueDateFormat = new SimpleDateFormat("yyyy-MM-dd");
						Date dueDate = dueDateFormat.parse(due);
						if(compareDates(dueDate,closedDate) && !fields.get(storyPointsFieldID).toString().equals("null")){
								committedStoryPoints+= fields.getInt(storyPointsFieldID);
								committedTasks++;
								
							}
					}
			}
		}
		
		
		JSONArray closedIssues = getResponseJSON("/rest/api/2/search?jql=sprint={sprint_id} AND status was 'Closed' on {closedOn}&startAt={startAt}&maxResults=100", hostUser, sprintid, closedOn);
		int cI = closedIssues.length();
		JSONArray resolvedIssues = getResponseJSON("/rest/api/2/search?jql=sprint={sprint_id} AND status was 'Resolved'on {closedOn}&startAt={startAt}&maxResults=100", hostUser, sprintid, closedOn);
		int R = resolvedIssues.length();
		
		if(resolvedIssues.length()!=0){
		for (int i = 0; i < resolvedIssues.length(); i++) {
			closedIssues.put(resolvedIssues.getJSONObject(i));
		}
		}
		
		int c = closedIssues.length();
		if(closedIssues.length()!=0){
			for(int i=0;i<closedIssues.length();i++){
				JSONObject j = closedIssues.getJSONObject(i);
				JSONObject fields = j.getJSONObject("fields");
				if(!fields.get(storyPointsFieldID).toString().equals("null")){
						completedStoryPoints+= fields.getInt(storyPointsFieldID);
					//	completedTasks++;
						if(!fields.get("duedate").toString().equals("null")){
							String due = fields.getString("duedate");
							SimpleDateFormat dueDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date dueDate = dueDateFormat.parse(due);
							if(!compareDates(dueDate,closedDate))
							{
								countAsCommitted++;
							}
							else{
								completedWithDueDate++;
							}
						}	
				}
			}
		}
	
	}catch(Exception e){
		System.out.println(e);
		}
	
		int reqMoved = (committedTasks-completedWithDueDate);
		countAsCommitted += committedTasks;
		int sprintGoal = 0;
		try {
		sprintGoal = Integer.parseInt(goal);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		velocityData.setCommittedStoryPoints(committedStoryPoints);
		velocityData.setCompletedStoryPoints(completedStoryPoints);
		velocityData.setSprintName(sprintName);
		velocityData.setReqMoved(reqMoved); 
		velocityData.setSprintGoal(sprintGoal);
		
		return CompletableFuture.completedFuture(velocityData);
	}
	
	public boolean compareDates(Date date1, Date date2) throws ParseException{
			if(date1.compareTo(date2) <= 0)
		            return true;
			 else
				 	return false;
        
	}
	
	
	public JSONArray getResponseJSON(String url,AtlassianHostUser hostUser,int sprintid,String closedOn){
		JSONArray finalJSONArrayofClosedIssues = new JSONArray();
		int completedCount=0;
		int initialTotal = 0;
		int startAtCount = 0;
		do{
			String response2 = restClients.authenticatedAs(hostUser)
			        .getForObject(
			                UriComponentsBuilder.fromUriString(url)
			                        .buildAndExpand(sprintid, "\"" + closedOn + "\"",startAtCount).toUri(),String.class);	
			JSONObject response3 = new JSONObject(response2);
			
			completedCount = response3.getInt("total");  
			
			if(completedCount!=0){
			JSONArray jsonArrayofClosedIssues = response3.getJSONArray("issues");
			for (int i = 0; i < jsonArrayofClosedIssues.length(); i++) {
			    finalJSONArrayofClosedIssues.put(jsonArrayofClosedIssues.getJSONObject(i));
			}
			}
			
			int issueMaxResult = response3.getInt("maxResults");
			initialTotal = initialTotal + issueMaxResult;
			startAtCount=initialTotal;
			}while(initialTotal < completedCount);
		
		return finalJSONArrayofClosedIssues;
	}
	
	
	
}

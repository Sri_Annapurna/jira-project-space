package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.jiraplugin.metrics.service.TimeAndEffort;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.MeanCycleTimeData;

@Service
public class RequestTypeThread {
	
	@Autowired
	TimeAndEffort te;
	
	Logger logger = LoggerFactory.getLogger(RequestTypeThread.class);
	
	@Async
	public CompletableFuture<ArrayList<MeanCycleTimeData>> runThread(String p, int sDate,@AuthenticationPrincipal AtlassianHostUser hostUser,HttpSession session) throws JSONException, ParseException, InterruptedException, ExecutionException {
		MeanCycleTimeData meanCycleTime = new MeanCycleTimeData();
	//	MeanCycleTimeData meanEffort = new MeanCycleTimeData();
		ArrayList<MeanCycleTimeData> m = new ArrayList<MeanCycleTimeData>();
		
		try {
			CompletableFuture<ArrayList<String>> t1 = te.getCycleTime(p, sDate, "High", hostUser,session);
			
			CompletableFuture<ArrayList<String>> t2 = te.getCycleTime(p, sDate, "Medium", hostUser,session);
			
			CompletableFuture<ArrayList<String>> t3 = te.getCycleTime(p, sDate, "Low", hostUser,session);
			
			CompletableFuture.allOf(t1,t2,t3).join();
			
			meanCycleTime.setMinHighTime(t1.get().get(0));
			meanCycleTime.setMaxHighTime(t1.get().get(1));
			meanCycleTime.setAvgHighTime(t1.get().get(2));
			
			meanCycleTime.setMinMedTime(t2.get().get(0));
			meanCycleTime.setMaxMedTime(t2.get().get(1));
			meanCycleTime.setAvgMedTime(t2.get().get(2));
			
			meanCycleTime.setMinLowTime(t3.get().get(0));
			meanCycleTime.setMaxLowTime(t3.get().get(1));
			meanCycleTime.setAvgLowTime(t3.get().get(2));
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
			String month = (YearMonth.now().minusMonths(Math.abs(sDate))).format(formatter);
			meanCycleTime.setMonth(month);
			
			//meanEffort
			
			/*meanEffort.setMinHighTime(t1.get().get(3));
			meanEffort.setMaxHighTime(t1.get().get(4));
			meanEffort.setAvgHighTime(t1.get().get(5));
			
			meanEffort.setMinMedTime(t2.get().get(3));
			meanEffort.setMaxMedTime(t2.get().get(4));
			meanEffort.setAvgMedTime(t2.get().get(5));
			
			meanEffort.setMinLowTime(t3.get().get(3));
			meanEffort.setMaxLowTime(t3.get().get(4));
			meanEffort.setAvgLowTime(t3.get().get(5));
			
			meanEffort.setMonth(month);*/
			
			m.add(meanCycleTime);
		//	m.add(meanEffort);
			
			
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return CompletableFuture.completedFuture(m);
		
		
	}
	
		
}

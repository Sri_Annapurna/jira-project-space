package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskOccurrenceData;
import com.jiraplugin.metrics.model.RiskSummaryData;

@Service
public class CriticalDeliveryRiskAsyn {

	@Autowired
	private AtlassianHostRestClients restClients;
	
	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Value("${responsibility_risk}")
	String responsibility_risk;
	@Value("${key}")
	String key;
	@Value("${linkUrl}")
	String linkUrl;
	@Value("${riskOccurrenceDate}")
	String riskOccurrenceDate;
	@Value("${riskEventResponseAction}")
	String riskEventResponseAction;
	@Value("${riskDescription}")
	String riskDescription;
	@Value("${remarks}")
	String remarks;
	@Value("${status}")
	String status;
	@Value("${riskAssessmentValue}")
	String riskAssessment;
	

	@Async
	public CompletableFuture<ArrayList<RiskSummaryData>> findCount(AtlassianHostUser hostUser, int start, String riskUrl, HttpSession session) throws InterruptedException {

		String responseEntity = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(riskUrl)
						.buildAndExpand("\"" + start + "\"","\"" + start + "\"").toUri(),
				String.class);
	
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
	
		RiskSummaryData riskSummaryData;
		ArrayList<RiskSummaryData> riskSummaryArray = new ArrayList<>();

		JSONObject initialResponse = new JSONObject(responseEntity);
		JSONArray initialArray = initialResponse.getJSONArray("issues");
		try {
	for (int i = 0; i < initialArray.length(); i++) {

			riskSummaryData = new RiskSummaryData();
			
			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject("fields");

			String reponseDate = fields.getString(fieldList.get("Reported Date"));
			String riskDec = fieldList.get("Risk Description");
			String busiImpact = fieldList.get("Business Impact (BI)");
			String riskValue = fieldList.get("Risk Value");
			String busiImpaDes = fieldList.get("Business Impact Description");
			String riskTreatmentOption = fieldList.get("Risk Treatment Option");
			String issueKeyValue = issueArray.getString("key");
			String responsibility = fieldList.get("Responsibility - Risk Treatment Action Plan Implementation");
			String riskAct = fieldList.get("Risk Treatment Action / Control");
			String statusId = fieldList.get(status);
			String riskProbabilityValue = "-";
			try {
				riskProbabilityValue = fields.getJSONObject(fieldList.get("Risk Probability (RP)")).getString("value");
			}catch(Exception e) {
				e.printStackTrace();
			}
			String businessImpact = "-";
			try {
				businessImpact = fields.getJSONObject(busiImpact).get("value").toString();
			}catch(Exception e) {
				e.printStackTrace();
			}
			String riskTreatmentOptions = "-";
			try {
				riskTreatmentOptions = fields.getJSONObject(riskTreatmentOption).get("value").toString();
			}catch(Exception e) {
				e.printStackTrace();
			}
			riskSummaryData.setIssueKey(issueKeyValue);	
			riskSummaryData.setIssueLink(hostUser.getHost().getBaseUrl()+linkUrl+issueKeyValue);
			riskSummaryData.setRiskDescription(fields.get(riskDec).toString().equals("null") ? "-" : fields.get(riskDec).toString() );
			riskSummaryData.setRiskProbability(riskProbabilityValue);
			riskSummaryData.setBusinessImpact(businessImpact);
			String riskValueChange = fields.get(riskValue).toString().equals("null")? "-" : fields.get(riskValue).toString();
			String responsibilityValue = fields.get(responsibility).toString().equals("null")? "-": fields.get(responsibility).toString();
			riskSummaryData.setResponsibility(responsibilityValue);
			riskSummaryData.setRiskValue(riskValueChange);
			riskSummaryData.setBusinessImpactDes(fields.get(busiImpaDes).toString().equals("null") ? "-" : fields.get(busiImpaDes).toString());
			riskSummaryData.setRiskTreatmentOp(riskTreatmentOptions);
			riskSummaryData.setRiskAction(fields.get(riskAct).toString().equals("null") ? "-" : fields.get(riskAct).toString());
			JSONObject statusData = fields.getJSONObject(statusId);
			riskSummaryData.setStatus( statusData.get("name").toString());
			riskSummaryArray.add(riskSummaryData);
	
		}	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	
		return CompletableFuture.completedFuture(riskSummaryArray);

	}
	
	
	

	@Async
	public CompletableFuture<ArrayList<RiskOccurrenceData>> getRiskOccurrenceData(AtlassianHostUser hostUser, int dce, String riskUrl,Map<String,String> fieldList)
			throws InterruptedException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
		ArrayList<RiskOccurrenceData> riskOccurrenceData = new ArrayList<>();
		String domain = hostUser.getHost().getBaseUrl();
		int startAt = 0;
		int total = 0;
		String responsibilityId = fieldList.get(responsibility_risk);
		String riskOccurrenceDateId = fieldList.get(riskOccurrenceDate);
		String riskEventResponseActionId = fieldList.get(riskEventResponseAction);
		String riskDescriptionId = fieldList.get(riskDescription);
		String remarksId = fieldList.get(remarks);
		String statusId = fieldList.get(status);
		JSONArray jsonArray = new JSONArray();
		do{
			String responseEntity = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder
					.fromUriString(riskUrl).buildAndExpand("\"" + dce + "\"", "\"" + dce + "\"").toUri(), String.class);
			JSONObject jsonObj = new JSONObject(responseEntity);
			startAt =  startAt + 100;
			total = Integer.parseInt(jsonObj.get("total").toString());
			JSONArray jsonArray1 = jsonObj.getJSONArray("issues");
			for (int j = 0; j < jsonArray1.length(); j++) {
				JSONObject objects = jsonArray1.getJSONObject(j);
				jsonArray.put(objects);
			}
			}while(startAt < total);
			int jsonArrayLength = jsonArray.length();
			for(int i = 0; i < jsonArrayLength; i++) {
				JSONObject issueJsonObj = jsonArray.getJSONObject(i);
				JSONObject jsonObj = issueJsonObj.getJSONObject("fields");
				String responsibilityValue;
				String keyValue = issueJsonObj.get(key).toString();
				
				String issueLinkValue  = domain+linkUrl+keyValue;
				try {
					String resValue = jsonObj.get(responsibilityId).toString();
					responsibilityValue = resValue.equals("null") ? "-" : resValue;   
				}catch(Exception e) {
					responsibilityValue= "-";
				}
				String occurrenceDateValue;
				try {
					String occrDateValue = jsonObj.get(riskOccurrenceDateId).toString();
					Date occrdate = formatter.parse(occrDateValue);
					occurrenceDateValue = occrDateValue.equals("null") ? "-" : new SimpleDateFormat("dd-MMM-yyyy").format(occrdate);;
				}catch(Exception e) {
					occurrenceDateValue = "-";
				}
				String riskEventResponseActionValue;
				try {
					
				String respActionValue = jsonObj.get(riskEventResponseActionId).toString();
				riskEventResponseActionValue = respActionValue.equals("null") ? "-":respActionValue;
				}catch(Exception e) {
					riskEventResponseActionValue = "-";
				}
				String riskDescriptionValue;
				try {
				String riskDesValue = jsonObj.get(riskDescriptionId).toString();
				riskDescriptionValue = riskDesValue.equals("null")? "-": riskDesValue; 
				}catch(Exception e) {
					riskDescriptionValue = "-";
				}
				String remarksValue;
				try {
					String remarksVal = jsonObj.get(remarksId).toString(); 
				remarksValue= remarksVal.equals("null")?"-": remarksVal;
				}catch(Exception e) {
				remarksValue = "-";
				}
				String statusValue;
				try {
				String statusVal = jsonObj.getJSONObject(statusId).get("name").toString(); 
				statusValue = statusVal.equals("null")? "-":statusVal;
				}catch(Exception e){
					statusValue = "-";
				}
				RiskOccurrenceData riskOccurrenceJsonData = new RiskOccurrenceData();
				riskOccurrenceJsonData.setEventResponseAction(riskEventResponseActionValue);
				riskOccurrenceJsonData.setOccurrenceDate(occurrenceDateValue);
				riskOccurrenceJsonData.setRemarks(remarksValue);
				riskOccurrenceJsonData.setResponsibility(responsibilityValue);
				riskOccurrenceJsonData.setRiskDescription(riskDescriptionValue);
				riskOccurrenceJsonData.setStatus(statusValue);
				riskOccurrenceJsonData.setIssuelink(issueLinkValue);
				riskOccurrenceJsonData.setKey(keyValue);
				riskOccurrenceData.add(riskOccurrenceJsonData);
			}
		
		return CompletableFuture.completedFuture(riskOccurrenceData);

	}
	@Async
	public CompletableFuture<ResultRangeData> getExposureRatingData(AtlassianHostUser hostUser, String url, int month, HttpSession session) throws InterruptedException {
		ResultRangeData resultRangeData = new ResultRangeData();
		JSONArray finalJSONArray = new JSONArray();
		int initialTotal = 0;
		int issueMaxResults = 100;
		int startAtCount = 0;
		int total = 0;
		do{
			String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + month + "\"","\"" + month + "\"").toUri(),String.class);	
			JSONObject response1 = new JSONObject(response);
			total = response1.getInt("total");
			JSONArray jsonArrayOfIssues = response1.getJSONArray("issues");
			for (int i = 0; i < jsonArrayOfIssues.length(); i++) {
			    finalJSONArray.put(jsonArrayOfIssues.getJSONObject(i));
			}
			
			int issueMaxResult = response1.getInt("maxResults");
			initialTotal = initialTotal + issueMaxResult;
			startAtCount=initialTotal;
			}while(initialTotal < total);
		
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		String riskValue = fieldList.get("Risk Value");
		String residualRiskValue = fieldList.get("Residual Risk Value");
		int sumOfRiskValue = 0;
		int sumOfResidualRiskValue = 0;
	try {
		int length = finalJSONArray.length();
	for (int i = 0; i < length; i++) {			
			JSONObject issueArray = finalJSONArray.getJSONObject(i);
			JSONObject fields = issueArray.getJSONObject("fields");
			Double riskValueInDouble = Double.parseDouble(fields.get(riskValue).toString());
			sumOfRiskValue = sumOfRiskValue + (int) Math.round(riskValueInDouble);
			Double residulaRiskValueInDouble = Double.parseDouble(fields.get(residualRiskValue).toString());
			sumOfResidualRiskValue = sumOfResidualRiskValue + (int) Math.round(residulaRiskValueInDouble);
	}
	int avgRiskValue = 0;
	int avgResidualRiskValue = 0;
	if(length != 0) {
	avgRiskValue = sumOfRiskValue / length;
	avgResidualRiskValue = sumOfResidualRiskValue / length;
	}
	int riskRangeValue = riskRating(avgRiskValue);
	int residualRiskRangeValue = riskRating(avgResidualRiskValue);
	String riskBucketValue = bucketValueRange(avgRiskValue);
	String residualRiskBucketValue = bucketValueRange(avgResidualRiskValue);
	resultRangeData.setAvg_risk(riskRangeValue);
	resultRangeData.setAvg_residual(residualRiskRangeValue);
	resultRangeData.setAvg_risk_value(riskBucketValue);
	resultRangeData.setAvg_residual_value(residualRiskBucketValue);
	System.out.println(ReflectionToStringBuilder.toString(resultRangeData));
	}catch(Exception e) {
		e.printStackTrace();
	}
	return CompletableFuture.completedFuture(resultRangeData);
}
	public int riskRating(int value) {
		int rating = 0;
		if ((value < 1) || (value > 36)) {
			rating = 0;
		} else if ((value >= 1) && (value <= 4)) {
			rating = 1;
		} else if ((value >= 5) && (value <= 9)) {
			rating = 2;
		} else if ((value >= 10) && (value <= 17)) {
			rating = 3;
		} else if ((value >= 18) && (value <= 28)) {
			rating = 4;
		} else if ((value >= 29) && (value <= 36)) {
			rating = 5;
		}
		return rating;
	}
	public String bucketValueRange(int value) {
		String range = "";
		if ((value < 1) || (value > 36)) {
			range = "-";
		} else if ((value >= 1) && (value <= 4)) {
			range = "1-Low";
		} else if ((value >= 5) && (value <= 9)) {
			range = "2-Medium";
		} else if ((value >= 10) && (value <= 17)) {
			range = "3-High";
		} else if ((value >= 18) && (value <= 28)) {
			range = "4-Very High";
		} else if ((value >= 29) && (value <= 36)) {
			range = "5-Critical";
		}
		return range;
	}
}

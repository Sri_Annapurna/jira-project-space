package com.jiraplugin.metrics.service;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.UserController;
import com.jiraplugin.metrics.model.FieldCustomID;

@Service
public class JiraCustomFieldMap {

	private AtlassianHostRestClients restClients;
	
	@Autowired
	private UserController userController;
	
	

	Map<String, String> customFieldIDMap = new LinkedHashMap<String, String>();

	@Autowired
	public JiraCustomFieldMap(AtlassianHostRestClients theRestClients) {
		this.restClients = theRestClients;
	}

	// Returns the list of fields in the jira instance with field keys
	public Map<String, String> getJiraFieldList(AtlassianHostUser hostUser, HttpSession session) {

		String FieldJsonURL = "/rest/api/2/field";
		
		FieldCustomID[] result = null;
		
		if(userController.getJiraFieldListFromSession(session) != null){
			
			customFieldIDMap = userController.getJiraFieldListFromSession(session);
			
		}
			else {
				try {
				result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,
						FieldCustomID[].class);

				for (int i = 0; i < result.length; i++) {

					customFieldIDMap.put(result[i].getName(), result[i].getKey());
				}	
				}catch(Exception e) {
					e.printStackTrace();
				}
				
		}
	return customFieldIDMap;

	}

}

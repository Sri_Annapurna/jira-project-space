package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffectivenessData;
import com.jiraplugin.metrics.model.InitiativeData;
@Service
public class IntiativeThreadService {
	@Autowired InitiativeService t;
	public ArrayList<InitiativeData> runInitiativeThreads(String p,AtlassianHostUser hostUser, HttpSession session) throws Exception {   
		ArrayList<InitiativeData> data = new ArrayList<InitiativeData>();
		CompletableFuture<ArrayList<InitiativeData>> t0 = t.getInitiatives(p, "open",hostUser, session);
		
		CompletableFuture<ArrayList<InitiativeData>> t1 = t.getInitiatives(p,"In Progress",hostUser, session);
		
		CompletableFuture<ArrayList<InitiativeData>> t2 = t.getClosedData(p,-1,hostUser, session);
		
		CompletableFuture<ArrayList<InitiativeData>> t3 = t.getClosedData(p,0,hostUser, session);
		
		CompletableFuture.allOf(t0,t1,t2,t3).join();
		data.addAll(t0.get());
		data.addAll(t1.get());
		data.addAll(t2.get());
		data.addAll(t3.get());
		System.out.println("Ïnto the project");
		for(InitiativeData e: data) {
			System.out.println(ReflectionToStringBuilder.toString(e));
		}
		return data;
	}
}

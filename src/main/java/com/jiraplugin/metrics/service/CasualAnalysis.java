package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;
@Service
public class CasualAnalysis {
	
	@Autowired
	public CorrImprActionService corrimpActionService;
	public List<CreativeIdea> getcasualanalysis( AtlassianHostUser hostUser,String p, int monthsDifference, HttpSession session) throws Exception{
		ArrayList<CreativeIdea> cialist=new ArrayList<>();
		
		CompletableFuture<List<CreativeIdea>> ciaopen = corrimpActionService.getCIAOpen( hostUser,p, session);

		CompletableFuture<List<CreativeIdea>> ciaclosed1month = corrimpActionService.getCIAClosedOneMonthback(hostUser,p,monthsDifference, session);
		
		CompletableFuture.allOf(ciaopen, ciaclosed1month);
		cialist.addAll(ciaopen.get());
		cialist.addAll(ciaclosed1month.get());


	
		return cialist;
		
	}
}
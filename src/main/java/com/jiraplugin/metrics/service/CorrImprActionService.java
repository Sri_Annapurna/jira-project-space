package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;

@Service
public class CorrImprActionService {
    private static final String VALUE = "value";
	private static final String EXPDATEFORM = "dd-MMM-yyyy";
	private static final String RECDATEFORM = "yyyy-MM-dd";
	@Autowired
    private AtlassianHostRestClients restClients;
    @Autowired
    ErrorService es;
    @Autowired
    ProjectService jcfm;
    @Value("${delivered_defect}")
    String deliveredDefectCorr;
    @Value("${corrective_improvement_actions}")
    String correctiveImprovementActionsCorr;
    @Value("${reported_date}")
    String reportedDateCorr;
    @Value("${category_corrective_improvement_actions}")
    String categoryCorrectiveImprovementActionsCorr;
    @Value("${issue_description}")
    String issueDescriptionCorr;
    @Value("${defect_type}")
    String defectTypeCorr;
    @Value("${priority}")
    String priorityCorr;
    @Value("${root_cause}")
    String rootCauseCorr;
    @Value("${action}")
    String actionCorr;
    @Value("${action_type}")
    String actionTypeCorr;
    @Value("${responsibility}")
    String responsibilityCorr;
    @Value("${actual_date}")
    String actualDateCorr;
    @Value("${target_date}")
    String targetDateCorr;

    @Async
    public CompletableFuture<List<CreativeIdea>> getCIAOpen(AtlassianHostUser hostUser, String p, HttpSession session) throws IOException
      {
	List<CreativeIdea> cialist = new ArrayList<>();

	try {
	    String url = "/rest/api/2/search?jql=project= {p} AND (issuetype= {corrective_improvement_actions} OR issuetype= {delivered_defect} )  AND status!='closed'";
	    String response = restClients.authenticatedAs(hostUser)
	            .getForObject(UriComponentsBuilder
	                    .fromUriString(url).buildAndExpand("\"" + p + "\"",
	                            "\"" + correctiveImprovementActionsCorr + "\"", "\"" + deliveredDefectCorr + "\"")
	                    .toUri(), String.class);
	    JSONObject response1 = new JSONObject(response);
	    int count = response1.getInt("total");
	    if (count != 0) {
		cialist = getParsedData(response1, hostUser, session);
	    }

	} catch (Exception e) {
	    es.writeException(e, p);
	}
	return CompletableFuture.completedFuture(cialist);
    }

    // Corrective Improvement Actions last month and before last month
    @Async
    public CompletableFuture<List<CreativeIdea>> getCIAClosedOneMonthback(AtlassianHostUser hostUser, String p,
            int monthsDifference, HttpSession session) throws IOException {
	List<CreativeIdea> cialist = new ArrayList<>();
	try {
	    int i = -monthsDifference - 1;
	    int j = -monthsDifference - 2;
	    String url = "/rest/api/2/search?jql=project= {p} AND (issuetype= {corrective_improvement_actions} OR issuetype= {delivery_defect} )AND 'Issue Closed Date'>=startOfMonth({j}) AND 'Issue Closed Date'<=endOfMonth({i}) AND status= 'closed'";

	    String response = restClients.authenticatedAs(hostUser)
	            .getForObject(UriComponentsBuilder
	                    .fromUriString(url).buildAndExpand("\"" + p + "\"",
	                            "\"" + correctiveImprovementActionsCorr + "\"", "\"" + deliveredDefectCorr + "\"", j, i)
	                    .toUri(), String.class);
	    JSONObject response1 = new JSONObject(response);

	    int count = response1.getInt("total");
	    if (count != 0) {
		cialist = getParsedData(response1, hostUser, session);
	    }
	} catch (Exception e) {
	    es.writeException(e, p);
	}
	return CompletableFuture.completedFuture(cialist);
    }
    
    // Corrective Improvement Actions before last month
    
    @Autowired JiraCustomFieldMap jmap;

    public List<CreativeIdea> getParsedData(JSONObject response, AtlassianHostUser hostUser, HttpSession session) throws ParseException{
	ArrayList<CreativeIdea> cialist = new ArrayList<>();
	
	Map<String, String> map = jmap.getJiraFieldList(hostUser, session);
	JSONArray jsonArray = response.getJSONArray("issues");

	if (jsonArray != null) {
	    for (int i = 0; i < jsonArray.length(); i++) {
		JSONObject issuesData = jsonArray.getJSONObject(i);
		JSONObject fieldsData = issuesData.getJSONObject("fields");
		CreativeIdea cia = new CreativeIdea();
		JSONObject issueType = fieldsData.getJSONObject("issuetype");
		String issuetype = issueType.getString("name");
		String repdate = fieldsData.getString(map.get(reportedDateCorr));
		Date rep = new SimpleDateFormat(RECDATEFORM).parse(repdate);
		String repdt = new SimpleDateFormat(EXPDATEFORM).format(rep);
		cia.setDate(repdt);
		cia.setStatus(fieldsData.getJSONObject("status").getString("name"));
		if (issuetype.equals(deliveredDefectCorr))
		    cia.setCategory("Delivered Defect");
		else {
		    JSONObject category = fieldsData.getJSONObject(map.get(categoryCorrectiveImprovementActionsCorr));
		    cia.setCategory(category.getString(VALUE));
		}
		cia.setDescription(fieldsData.getString(map.get(issueDescriptionCorr)));

		if (issuetype.equals(deliveredDefectCorr))
		    cia.setDefect_type("Delivery");
		else {
		    JSONObject defecttype = fieldsData.getJSONObject(map.get(defectTypeCorr));

		    cia.setDefect_type(defecttype.getString(VALUE));
		}
		JSONObject prio = fieldsData.getJSONObject(map.get("Severity of the Issue"));
		cia.setPriority(prio.getString(VALUE));
		cia.setRoot_cause(fieldsData.getString(map.get(rootCauseCorr)));
		cia.setAction(fieldsData.getString(map.get(actionCorr)));
		
		
		getResult(map, fieldsData, cia, issuetype); 
		
		cialist.add(cia);

	    }
	}

	return cialist;
    }

	private void getResult(Map<String, String> map, JSONObject fieldsData, CreativeIdea cia, String issuetype)
			throws ParseException {
		if (issuetype.equals(deliveredDefectCorr))
		    cia.setAction_type("Corrective Action");
		else {
		    JSONObject actionTypeObject = fieldsData.getJSONObject(map.get(actionTypeCorr));
		    cia.setAction_type(actionTypeObject.getString(VALUE));
		}

		JSONArray robject = fieldsData.getJSONArray(map.get(responsibilityCorr));
		StringBuilder resp = new StringBuilder();
		boolean flag = false;
		for (int k = 0; k < robject.length(); k++) {
		    if (flag) {
			resp.append(",");
		    }
		    JSONObject jo = robject.getJSONObject(k);
		    resp.append(jo.getString("displayName"));
		    flag = true;
		}
		cia.setResponsibility(resp.toString());
		String tgd = fieldsData.getString(map.get(targetDateCorr));
		if (!tgd.equals("null") && !tgd.isEmpty()) {
		Date tdate = new SimpleDateFormat(RECDATEFORM).parse(tgd);
		String td = new SimpleDateFormat(EXPDATEFORM).format(tdate);
		cia.setTarget_date(td);
		}
		String acd = fieldsData.get(map.get(actualDateCorr)).toString();
		if (!acd.equals("null") && !acd.isEmpty()) {
		    Date ad = new SimpleDateFormat(RECDATEFORM).parse(acd);
		    String adt = new SimpleDateFormat(EXPDATEFORM).format(ad);
		    cia.setActual_date(adt);
		}
		else
		    cia.setActual_date("-");
	}
}

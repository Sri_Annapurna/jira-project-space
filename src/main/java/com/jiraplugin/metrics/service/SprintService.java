package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.EffectivenessData;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.TestCase;
import com.jiraplugin.metrics.model.VelocityData;

@Service
public class SprintService {
	@Autowired ProjectService ps;
	@Autowired
	private AtlassianHostRestClients restClients;
	/*@Cacheable("Defect Data")*/
	/*@CacheEvict(value="Defect Data", allEntries=true)*/
	@Autowired DevEffectivenessService deveffService;
	@Autowired IssueTypeThread issueService;
	@Autowired NewVelocityChart newVelocityChart;

	//return type - [sprintID={"sprintName","completedDate","goal"]
	public LinkedHashMap<Integer, ArrayList<String>> getSprintsData(int id, AtlassianHostUser hostUser) throws Exception{
		Map<Integer, Integer> boardIds = ps.getBoardIds();
		LinkedHashMap<Integer, ArrayList<String>> sprintData = new LinkedHashMap<>();
		
		try {
			int boardId = boardIds.get(id); 
			int startAt = 0;
			int maxResults = 0;
			boolean isLast = false;
			JSONArray jsonArray = new JSONArray();
		while(isLast == false) {
			   String url = "/rest/agile/1.0/board/"+ boardId + "/sprint?state=closed&startAt="+startAt;
			   String response = restClients.authenticatedAs(hostUser).getForObject(url , String.class);
				JSONObject jsonObj1 = new JSONObject(response);
				maxResults = jsonObj1.getInt("maxResults"); 
				isLast = jsonObj1.getBoolean("isLast");
				startAt = startAt + maxResults;
				if(isLast == true) {
				JSONArray jsonArray1 = jsonObj1.getJSONArray("values");
				int length = jsonArray1.length()-1;
				int arraySize = jsonArray1.length() -4;
				for (int i = length; i >= arraySize; i--) {
					JSONObject object = jsonArray1.getJSONObject(i);
					ArrayList<String> sprintArray = getSprintArray(object);
					sprintData.put(object.getInt("id"), sprintArray);
				}
				}
		}
		}catch(Exception e) {
			System.out.println(e);
		}
		return sprintData;
	}
	
	public ArrayList<String> getSprintArray(JSONObject object) throws Exception{
		
		ArrayList<String> sprintArray = new ArrayList<>();
		sprintArray.add(object.getString("name"));
		String inputDate = object.getString("completeDate");
		String sprintGoal = "0";
		if(!object.getString("goal").isEmpty())
			sprintGoal = object.getString("goal");
		TimeZone utc = TimeZone.getTimeZone("UTC");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		f.setTimeZone(utc);
		GregorianCalendar cal = new GregorianCalendar(utc);
		cal.setTime(f.parse(inputDate));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		SimpleDateFormat dateIncludingSeconds = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sprintArray.add(sdf.format(cal.getTime()));
		sprintArray.add(sprintGoal);
		sprintArray.add(dateIncludingSeconds.format(cal.getTime()));
		
		return sprintArray;
	}
	
	public ArrayList<VelocityData> getVelocity(int id, AtlassianHostUser hostUser,HttpSession session) throws Exception{
		ArrayList<VelocityData> velocityData = new ArrayList<>();
		LinkedHashMap<Integer, ArrayList<String>> sprintData = getSprintsData(id,hostUser);
		CompletableFuture<VelocityData>[] array = new CompletableFuture[sprintData.size()];
		int j=0;
		for(Map.Entry<Integer, ArrayList<String>> i: sprintData.entrySet()) {
			try {
				array[j] = (CompletableFuture<VelocityData>) newVelocityChart.getStoryPoints(i.getKey(), i.getValue().get(0),i.getValue().get(1),i.getValue().get(2), hostUser,session);
				j++;
				}catch(Exception e) {
					System.out.println(e);
				}
		}
		
		CompletableFuture.allOf(array).join();
		for(int i = 0; i < sprintData.size(); i++) {
			velocityData.add(array[i].get());
			//System.out.println(ReflectionToStringBuilder.toString(array[i].get()));
		}
		
		return velocityData;
	}
	
	
	public ArrayList<MetricsData> getSprintsMonthly(int id, AtlassianHostUser hostUser,HttpSession session) throws Exception{
		ArrayList<MetricsData> data = new ArrayList<MetricsData>();
		LinkedHashMap<Integer, ArrayList<String>> sprintData= getSprintsData(id, hostUser); 
		CompletableFuture<MetricsData>[] array = (CompletableFuture<MetricsData>[]) new CompletableFuture[sprintData.size()];
		int j = 0;
		for(Map.Entry<Integer, ArrayList<String>> i: sprintData.entrySet()) {
			try {
				System.out.println("Started threads");
			array[j] = (CompletableFuture<MetricsData>) deveffService.getMonthlyDefectsData(i.getValue().get(0),i.getKey(),true, i.getValue().get(3), hostUser,session);
			j++;
			}catch(Exception e) {
				System.out.println(e);
			}
		}
		CompletableFuture.allOf(array).join();
		for(int i = 0; i < sprintData.size(); i++) {
			data.add(array[i].get());
			//System.out.println(ReflectionToStringBuilder.toString(array[i].get()));
		}
		
		return data;
}
	public ArrayList<EffectivenessData> getSprintsIReport(int id, AtlassianHostUser hostUser) throws Exception{
		ArrayList<EffectivenessData> data = new ArrayList<EffectivenessData>();
		LinkedHashMap<Integer, ArrayList<String>> sprint= getSprintsData(id, hostUser);
		CompletableFuture<EffectivenessData>[] array = (CompletableFuture<EffectivenessData>[]) new CompletableFuture[sprint.size()];
		int j = 0;
		for(Map.Entry<Integer, ArrayList<String>> i: sprint.entrySet()) {
			try {
			array[j] = (CompletableFuture<EffectivenessData>) issueService.runTasks(i.getValue().get(0),i.getKey(),true, hostUser);
			j++;
			}catch(Exception e) {
				System.out.println(e);
			}
		}
		CompletableFuture.allOf(array).join();
		for(int i = 0; i < sprint.size(); i++) {
			data.add(array[i].get());
			//System.out.println(ReflectionToStringBuilder.toString(array[i].get()));
		}
		
		return data;
	}
	@Autowired
	TestcaseMetricsService td;
	public ArrayList<TestCase> getTestMetricsData(int id, AtlassianHostUser hostUser,HttpSession session,ArrayList<MetricsData> performanceList) throws Exception{
		LinkedHashMap<Integer, ArrayList<String>> sprintData = getSprintsData(id,hostUser);
		ArrayList<TestCase> data = new ArrayList<TestCase>();
		try {
	CompletableFuture<TestCase>[] array = (CompletableFuture<TestCase>[]) new CompletableFuture[sprintData.size()];
	int j = 0;
	for(Map.Entry<Integer, ArrayList<String>> i: sprintData.entrySet()) {
		
		array[j] = (CompletableFuture<TestCase>) td.getTestingMetrics(i.getValue().get(0), i.getKey(), true, hostUser, performanceList.get(j),session);
		j++;
	}
	
	CompletableFuture.allOf(array).join();
	for(int i = 0; i < sprintData.size(); i++) {
		data.add(array[i].get());
		//System.out.println(ReflectionToStringBuilder.toString(array[i].get()));
	}
	}catch(Exception e) {
		System.out.println(e);

	}	
	return data;
}
}
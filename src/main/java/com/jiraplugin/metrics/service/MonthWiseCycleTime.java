package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.jiraplugin.metrics.service.RequestTypeThread;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.MeanCycleTimeData;

@Service
public class MonthWiseCycleTime {
	
	@Autowired
	RequestTypeThread t;
	
	Logger logger = LoggerFactory.getLogger(MonthWiseCycleTime.class);
	public ArrayList<ArrayList<MeanCycleTimeData>> CycleTimeMonthThreads(String args,@AuthenticationPrincipal AtlassianHostUser hostUser,HttpSession session) throws Exception {   
		
		String p = args;
		
		ArrayList<ArrayList<MeanCycleTimeData>> cycletime = new ArrayList<ArrayList<MeanCycleTimeData>>();
		
		try {
		CompletableFuture<ArrayList<MeanCycleTimeData>> t1 = t.runThread(p,-1, hostUser,session);
		
		CompletableFuture<ArrayList<MeanCycleTimeData>> t2 = t.runThread(p,-2, hostUser,session);
		
		CompletableFuture<ArrayList<MeanCycleTimeData>> t3 = t.runThread(p,-3, hostUser,session);
		
		CompletableFuture<ArrayList<MeanCycleTimeData>> t4 = t.runThread(p,-4, hostUser,session);
		
		CompletableFuture.allOf(t1,t2,t3,t4).join();
		cycletime.add(t1.get());
		cycletime.add(t2.get());
		cycletime.add(t3.get());
		cycletime.add(t4.get());
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
		return cycletime;
		
	
	}
	}
package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class CountThread {

	@Value("${miscellaneousTask}")
	String mTask;
	
	@Autowired
	private AtlassianHostRestClients restClients;
	
	@Async
	public CompletableFuture<ArrayList<Integer>> getValues(String p,String name, int sDate,boolean isSprintWise,AtlassianHostUser hostUser) throws JSONException, ParseException {
		ArrayList<Integer> a = new ArrayList<>();
		int total = 0;
		int noOfReqCompletedOnTime=0;
		String url = "";
		String response = "";
		JSONObject response1=new JSONObject();
		try {
		if(isSprintWise){
		
		if(name.equals("Task")) {
			url = "/rest/api/2/search?jql=Sprint= {sprintId} AND  (issuetype= {name} OR (issuetype= {name}))&startAt=0&maxResults=100";
			response = restClients.authenticatedAs(hostUser).getForObject(
					UriComponentsBuilder.fromUriString(url).buildAndExpand(sDate, "\""+ name + "\"", "\""+ mTask +"\"").toUri(), String.class);
			 response1 = new JSONObject(response);
		}else {
			url = "/rest/api/2/search?jql=Sprint= {sprintId} AND  (issuetype= {name})&startAt=0&maxResults=100";
			response = restClients.authenticatedAs(hostUser).getForObject(
					UriComponentsBuilder.fromUriString(url).buildAndExpand(sDate, "\""+ name + "\"").toUri(), String.class);
			 response1 = new JSONObject(response);
		}
		
	/*	JSONObject response1 = new JSONObject(response);
		 total = response1.getInt("total");*/
		}else{
			
			if(name.equals("Task")) {
				url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {name} OR issuetype= {name} ) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({eDate}))&startAt=0&maxResults=100";
				response = restClients.authenticatedAs(hostUser).getForObject(
						UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\""+ name + "\"","\""+ mTask + "\"",sDate,sDate).toUri(), String.class);
				 response1 = new JSONObject(response);
			}else {
				url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {name}) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({eDate}))&startAt=0&maxResults=100";
				response = restClients.authenticatedAs(hostUser).getForObject(
						UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\""+ name + "\"",sDate,sDate).toUri(), String.class);
				response1 = new JSONObject(response);
			}
				
		}
		
		 noOfReqCompletedOnTime= noOfReqCompletedOnTime(response1);
		 total = response1.getInt("total");
		 a.add(total);
		 a.add( noOfReqCompletedOnTime);
		
		}catch(Exception e) {
			System.out.println(e);
		}
		return CompletableFuture.completedFuture(a);
	}
	
	
	public int noOfReqCompletedOnTime(JSONObject res){
		int reqCompletedOnTime=0;
		try{
		JSONArray issuesJSONArray = res.getJSONArray("issues");
		for(int i=0;i<issuesJSONArray.length();i++){
			JSONObject j = issuesJSONArray.getJSONObject(i);
			JSONObject fields = j.getJSONObject("fields");
			if(!fields.get("resolutiondate").toString().equals("null") && !fields.get("duedate").toString().equals("null")){
				String due = fields.getString("duedate");
				SimpleDateFormat dueDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date dueDate = dueDateFormat.parse(due);
				String resolved=fields.get("resolutiondate").toString();
				Date resolvedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(resolved);
				SimpleDateFormat dateWithoutTime = new SimpleDateFormat("dd/MM/yyyy");
				Date resolvedDateWithoutTime = dateWithoutTime.parse(dateWithoutTime.format(resolvedDate));
				if(resolvedDateWithoutTime.compareTo(dueDate) <= 0){
					reqCompletedOnTime++;
				}
			}
			}
		
		
		}catch(Exception e){
			System.out.println(e);
		}
		
		return reqCompletedOnTime;
	}
	
}

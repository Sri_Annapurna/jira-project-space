package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffectivenessData;



@Service
public class ComplianceThreadsService {
	@Value("${story}") String storyComp;
	@Value("${task}") String taskComp;
	@Value("${new_requirement}") String newRequirementComp;
	@Value("${change_request}") String changeRequestComp;
	@Value("${production_issue_support}") String productionIssueSupportComp;
	@Value("${improvement}") String improvementComp;
	@Autowired
	RequestComplianceService r;
	
	Logger logger = LoggerFactory.getLogger(ComplianceThreadsService.class);
	
	public List<EffectivenessData> createComplianceThreads(String project,String sDate, String eDate,AtlassianHostUser hostUser, List<String> dates,HttpSession session) {
		ArrayList<EffectivenessData> data = new ArrayList<>();
		try {
		
		
		CompletableFuture<Map<String, Integer>> t1 = r.getComplianceData(project, storyComp, sDate, eDate, hostUser, dates,session);
		
		CompletableFuture<Map<String, Integer>> t2 = r.getComplianceData(project, taskComp, sDate, eDate, hostUser, dates,session);
		
		CompletableFuture<Map<String, Integer>> t3 = r.getComplianceData(project, newRequirementComp, sDate, eDate, hostUser, dates,session);
		
		CompletableFuture<Map<String, Integer>> t4 = r.getComplianceData(project, changeRequestComp, sDate, eDate, hostUser, dates,session);		
		
		CompletableFuture<Map<String, Integer>> t5 = r.getComplianceData(project, productionIssueSupportComp, sDate, eDate, hostUser, dates,session);
		
		CompletableFuture<Map<String, Integer>> t6 = r.getComplianceData(project, improvementComp, sDate, eDate, hostUser, dates,session);
			
		CompletableFuture.allOf(t1,t2,t3,t4, t5, t6).join();
		
		for(String d: dates) {
			EffectivenessData e = new EffectivenessData();
			e.setMonth(d);
			e.setsCount(t1.get().get(d));
			e.settCount(t2.get().get(d));
			e.setNrCount(t3.get().get(d));
			e.setCrCount(t4.get().get(d));
			e.setPisCount(t5.get().get(d));
			e.setiCount(t6.get().get(d));
			data.add(e);
		}
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
		return data;
	}
}

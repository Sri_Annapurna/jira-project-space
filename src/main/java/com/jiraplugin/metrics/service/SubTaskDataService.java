package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffortData;

@Service
public class SubTaskDataService {
	@Autowired
	IssueService is;
	@Autowired
	ProjectService p;
	@Autowired
	JiraCustomFieldMap jmap;

	@Value("${analysis_review_effort}")
	String analysisReviewEffortFieldSub;
	@Value("${design_review_effort}")
	String designReviewEffortFieldSub;
	@Value("${code_review_effort}")
	String codeReviewEffortFieldSub;
	@Value("${unit_test_design_review_effort}")
	String unitTestDesignReviewEffortFieldSub;
	@Value("${review_effort_of_test_cases_written_by_developer}")
	String reviewEffortOfTestCasesWrittenByDeveloperFieldSub;
	@Value("${review_effort_of_test_cases_written_by_QA}")
	String reviewEffortOfTestCasesWrittenByQAFieldSub;
	@Value("${is_it_testable}")
	String isItTestableSub;
	@Autowired
	private AtlassianHostRestClients restClients;

	Logger logger = LoggerFactory.getLogger(SubTaskDataService.class);

	@Async
	public CompletableFuture<LinkedHashMap<String, EffortData>> getMetricsData(String p, String type, String sDate,
			String eDate, List<String> dates, AtlassianHostUser hostUser, HttpSession session) throws ParseException {
		String url1 = "/rest/api/2/search?jql=project={p} AND  (issuetype={type}) AND ('Reported Date'>={sDate} AND 'Reported Date'<={eDate})";
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url1)
				.buildAndExpand("\"" + p + "\"", "\"" + type + "\"", "\"" + sDate + "\"", "\"" + eDate + "\"").toUri(),
				String.class);
		JSONObject response1 = new JSONObject(response);
		Map<String, String> map = jmap.getJiraFieldList(hostUser, session);
		LinkedHashMap<String, EffortData> effortData = new LinkedHashMap<>();
		for (String d : dates) {
			EffortData ed = new EffortData();
			ed.setAnalysis_review_effort(0.0);
			ed.setCode_review_effort(0.0);
			ed.setDesign_review_effort(0.0);
			ed.setRe_by_developer(0.0);
			ed.setRe_by_qa(0.0);
			ed.setTestable_count(0.0);
			ed.setUnit_test_design_review_effort(0.0);
			effortData.put(d, ed);
		}
		JSONArray issuesArray = response1.getJSONArray("issues");
		int length = issuesArray.length();
		for (int i = 0; i < length; i++) {
			JSONObject jobj = issuesArray.getJSONObject(i);
			JSONObject fieldsObj = jobj.getJSONObject("fields");
			String date = fieldsObj.get(map.get("Reported Date")).toString();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date rdate = formatter.parse(date);
			String key = new SimpleDateFormat("MMM-yy").format(rdate);
			EffortData ed = effortData.get(key);
			double var = 0;

			String are =  getResultValue(fieldsObj, map, analysisReviewEffortFieldSub).toString();
			if (!are.equals("null") && !are.isEmpty()) {
				var = ed.getAnalysis_review_effort() + Double.parseDouble(are);
				ed.setAnalysis_review_effort(var);
			}

			String dre =  getResultValue(fieldsObj, map, designReviewEffortFieldSub).toString();
			if (!dre.equals("null") && !dre.isEmpty()) {
				var = ed.getDesign_review_effort() + Double.parseDouble(dre);
				ed.setDesign_review_effort(var);
			}

			String cre =  getResultValue(fieldsObj, map, codeReviewEffortFieldSub).toString();
			if (!cre.equals("null") && !cre.isEmpty()) {
				var = ed.getCode_review_effort() + Double.parseDouble(cre);
				ed.setCode_review_effort(var);
			}

			String ure =  getResultValue(fieldsObj, map, unitTestDesignReviewEffortFieldSub).toString();
			if (!ure.equals("null") && !ure.isEmpty()) {
				var = ed.getUnit_test_design_review_effort() + Double.parseDouble(ure);
				ed.setUnit_test_design_review_effort(var);
			}

			String red =  getResultValue(fieldsObj, map, reviewEffortOfTestCasesWrittenByDeveloperFieldSub).toString();
			if (!red.equals("null") && !red.isEmpty()) {
				var = ed.getRe_by_developer() + Double.parseDouble(red);
				ed.setRe_by_developer(var);
			}

			String req =  getResultValue(fieldsObj, map, reviewEffortOfTestCasesWrittenByQAFieldSub).toString();
			if (!req.equals("null") && !req.isEmpty()) {
				var = ed.getRe_by_qa() + Double.parseDouble(req);
				ed.setRe_by_developer(var);
			}

			JSONObject testableObj = (JSONObject) getResultValue(fieldsObj, map, isItTestableSub);
			String testable = testableObj.getString("value");
			if (testable.equals("Yes")) {
				double var1 = ed.getTestable_count() + 1;
				ed.setTestable_count(var1);
			}

			effortData.put(key, ed);
		}

		return CompletableFuture.completedFuture(effortData);
	}

	public Object getResultValue(JSONObject fieldsObj, Map<String, String> map, String value) {
		Object result = "null";
		try {

			if (fieldsObj.get(map.get(value)) != null) {

				result = fieldsObj.get(map.get(value));

			}

		} catch (Exception e) {

			logger.error(e.getMessage());
			result = "null";
		}

		return result;

	}

}

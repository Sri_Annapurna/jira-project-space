package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffectivenessData;

@Component
@Service
public class IssueTypeThread {
	@Value("${story}") String story;
	@Value("${task}") String task;
	@Value("${new_requirement}") String new_requirement;
	@Value("${change_request}") String change_request;
	@Value("${production_issue_support}") String production_issue_support;
	@Value("${improvement}") String improvement;
	@Autowired
	CountThread t;
	
	@Async
	public CompletableFuture<EffectivenessData> runTasks(String p, int sDate,boolean isSprintWise, AtlassianHostUser hostUser) throws JSONException, ParseException, InterruptedException, ExecutionException {
		EffectivenessData data = new EffectivenessData();
		int totalReqCompletedOnTime =0, totalReq = 0;
		double reqDel = 0.0, reqDelOnTime=0.0;
		long start = System.currentTimeMillis();
		CompletableFuture<ArrayList<Integer>> t0 = t.getValues(p,story, sDate,isSprintWise, hostUser);
		
		CompletableFuture<ArrayList<Integer>> t1 = t.getValues(p,task, sDate,isSprintWise, hostUser);
		
		CompletableFuture<ArrayList<Integer>> t2 = t.getValues(p,new_requirement, sDate,isSprintWise, hostUser);
		
		CompletableFuture<ArrayList<Integer>> t3 = t.getValues(p,change_request, sDate,isSprintWise, hostUser);
		
		CompletableFuture<ArrayList<Integer>> t4 = t.getValues(p,production_issue_support, sDate,isSprintWise, hostUser);	
		
		CompletableFuture<ArrayList<Integer>> t5 = t.getValues(p,improvement, sDate,isSprintWise, hostUser);
	
		
		CompletableFuture.allOf(t0,t1,t2,t3,t4, t5).join();
		
		data.setsCount(t0.get().get(0));
		data.settCount(t1.get().get(0));
		data.setNrCount(t2.get().get(0));
		data.setCrCount(t3.get().get(0));
		data.setPisCount(t4.get().get(0));
		data.setiCount(t5.get().get(0));
		
		totalReq = t0.get().get(0)+t1.get().get(0)+t2.get().get(0)+t3.get().get(0)+t4.get().get(0)+t5.get().get(0);
		totalReqCompletedOnTime = t0.get().get(1)+t1.get().get(1)+t2.get().get(1)+t3.get().get(1)+t4.get().get(1)+t5.get().get(1);
		
		if(totalReq != 0)
		{
				reqDel= (double)totalReqCompletedOnTime/totalReq*100.0;
		}
		reqDelOnTime = Math.round(reqDel*100.0)/100.0;
		
		data.setReqDelOnTime(reqDelOnTime);
		
	//	System.out.println("From Req Compliance chart -"+ p +  "Req Completed On Time"+ totalReqCompletedOnTime + " | Total Req Count ="+ totalReq);
		
		if(isSprintWise) {
			data.setMonth(p);
		}
		else{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		String m = (YearMonth.now().minusMonths(Math.abs(sDate))).format(formatter);
		data.setMonth(m);
		
		}
		return CompletableFuture.completedFuture(data);
	}
}

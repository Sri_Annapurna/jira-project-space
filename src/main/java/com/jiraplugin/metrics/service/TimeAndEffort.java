package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class TimeAndEffort {
	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	Map<String, String> fieldList = new HashMap<String, String>();
	@Value("${change_request}") String change_request;
	@Value("${new_requirement}") String new_req;
	@Value("${improvement}") String imp;
	    
	
	 @Async
	 public CompletableFuture<ArrayList<String>> getCycleTime(String p,int sDate, String type, @AuthenticationPrincipal AtlassianHostUser hostUser,HttpSession session) throws IOException{
		 ArrayList<String> a = new ArrayList<>();
		 try{
			Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
			String actualStartDate = fieldList.get("Actual Start Date");
			String hold = fieldList.get("Hold Time");
			
			int initialTotal = 0,flag=0;
			int count;
			int total=0;
			int startAtCount = 0;
			double sumCycleTime=0;
			double avgCycleTime=0;
			double minCycleTime=Long.MAX_VALUE;
			double maxCycleTime=Long.MIN_VALUE;
			double sumEffort=0;
			double avgEffort=0;
			double minEffort=Long.MAX_VALUE;
			double maxEffort=Long.MIN_VALUE;
			double holdTime=0.0;
		//	double timeSpent=0.0;
			double timeSpentHrs=0.0;
			JSONArray jsonArray = new JSONArray();
			JSONArray finalJSONArray = new JSONArray();
			
			
			do{
			String url1="/rest/api/2/search?jql=project={p} AND (issuetype={change_request} OR issuetype={new_req} OR issuetype={imp}) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({sDate})) AND (status='Closed') AND ('Complexity'= {type})&startAt=0&maxResults=100";
			String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url1).buildAndExpand("\"" + p + "\"", "\""+ change_request + "\"" ,"\""+ new_req + "\"" ,"\""+ imp + "\"" ,"\""+ sDate + "\"" , "\""+ sDate + "\"", "\"" + type + "\"" ).toUri(), String.class);
			JSONObject response1 = new JSONObject(response);
			count = response1.getInt("total");  
				
				jsonArray = response1.getJSONArray("issues");
				for (int i = 0; i < jsonArray.length(); i++) {
				    finalJSONArray.put(jsonArray.getJSONObject(i));
				}
				
				int issueMaxResult = response1.getInt("maxResults");
				initialTotal = initialTotal + issueMaxResult;
				startAtCount=initialTotal;
			} while(initialTotal < count);
		
	if(count!=0){		
		for(int i=0;i<finalJSONArray.length();i++){
			JSONObject j = finalJSONArray.getJSONObject(i);
			JSONObject fields = j.getJSONObject("fields");
			if(!fields.get(hold).toString().equals("null")){
				holdTime=(double) fields.get(hold);
			}
			String actual= fields.get(actualStartDate).toString();
			Date actualDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(actual);

				total++;
				String resolved=fields.get("resolutiondate").toString();
				Date resolvedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(resolved);
			
				
				double d=(resolvedDate.getTime() - actualDate.getTime());
			
				double cycletime=(((double)(resolvedDate.getTime() - actualDate.getTime())/86400000)-holdTime);
				DecimalFormat df= new DecimalFormat("0.0");
		
				
			
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(resolvedDate);
				
				cal.add(Calendar.DATE, (int) -holdTime);
				Date after=cal.getTime();
				long diff = after.getTime() - actualDate.getTime();
			
				
			
				
				/*if(!(fields.get("aggregatetimespent")==null)){
						 timeSpent = fields.getDouble("aggregatetimespent");
						 timeSpentHrs = timeSpent/3600;
				}*/
				
				sumCycleTime+=cycletime;
		//		sumEffort+=timeSpentHrs;
				
				
				if(cycletime<minCycleTime){
					minCycleTime=cycletime;
				}
				if(cycletime>maxCycleTime){
					maxCycleTime=cycletime;
				}
				
			/*	if(timeSpentHrs<minEffort){
					minEffort= timeSpentHrs;
				}
				if(timeSpentHrs>maxEffort){
					maxEffort=timeSpentHrs;
				}*/
				
				
				
			
		  
		    
		
		}

		DecimalFormat df= new DecimalFormat("0.0");
		if(total != 0) {
		avgCycleTime=(sumCycleTime/total);
		}
		a.add(df.format(minCycleTime));
		a.add(df.format(maxCycleTime));
		a.add(df.format(avgCycleTime));
 
		
		
		/*avgEffort=(sumEffort/total);
		a.add(df.format(minEffort));
		a.add(df.format(maxEffort));
		a.add(df.format(avgEffort));*/

	}
	else{
		a.add("0.0"); a.add("0.0"); a.add("0.0");/*a.add("0.0"); a.add("0.0"); a.add("0.0");*/
	}

		}
				catch(Exception e) {
					es.writeException(e,"error");
			}
		 return CompletableFuture.completedFuture(a);
	 }	
}

package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.EffectivenessData;

@Service
public class CidiThreadsService {
	@Autowired
	RequestComplianceService r;
	@Value("${client_complaints}")
	String client_complaints;

	@Value("${csfa_Category}")
	String csf_aspect;
	@Value("${Project_Delivery_PI}")
	String production;
	@Value("${Process_Complaince_Issue}")
	String compliance;

	@Value("${Information_Security}")
	String security;

	@Value("${Information_Security_Event}")
	String security_event;

	@Value("${Risk_Event}")
	String risk_event;

	@Value("${Delivery_Improvement}")
	String improvement;

	@Value("${Creative_Idea}")
	String creative_idea;
	public Map<String, CIDIData> createCidiThreads(String project,String sDate, String eDate,AtlassianHostUser hostUser,ArrayList<String> dates, HttpSession session) throws Exception {   
		LinkedHashMap<String, CIDIData> data = new LinkedHashMap<String, CIDIData>();
		for(String d:dates) {
			CIDIData cd = new CIDIData();
			data.put(d, cd);
		}
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t1 = r.getCidiData(project, client_complaints, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t2 = r.getCidiData(project, csf_aspect, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t3 = r.getCidiData(project, production, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t4 = r.getCidiData(project, compliance, sDate, eDate, hostUser, dates, session);		
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t5 = r.getCidiData(project, security, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t6 = r.getCidiData(project, security_event, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t7 = r.getCidiData(project, risk_event, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t8 = r.getCidiData(project, improvement, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> t9 = r.getCidiData(project, creative_idea, sDate, eDate, hostUser, dates, session);
		
		CompletableFuture.allOf(t1,t2,t3,t4, t5, t6, t7, t8, t9).join();
		/*CompletableFuture.allOf(t1).join();*/
		for(String d: dates) {
			CIDIData cd = data.get(d);
			cd.setcCData(t1.get().get(d));
			cd.setcSFAData(t2.get().get(d));
			cd.setpDPIData(t3.get().get(d));
			cd.setpCIData(t4.get().get(d));
			cd.setiSWData(t5.get().get(d));
			cd.setiSEData(t6.get().get(d));
			cd.setrEData(t7.get().get(d));
			cd.setdIData(t8.get().get(d));
			cd.setcIData(t9.get().get(d));
			data.put(d,cd);
		}
		return data;
		
	}
}

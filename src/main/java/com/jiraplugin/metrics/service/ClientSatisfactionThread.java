package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;

@Service
public class ClientSatisfactionThread {

	private static final String TOTAL = "total";

	private static final String DESCRIPTION = "description";

	private static final String FIELDS2 = "fields";

	private static final String ACTUALDATE = "dd-MMM-yyyy";

	private static final String RECDATE = "yyyy-MM-dd";

	private static final String ISSUES = "issues";

	private static final String ACTION2 = "Action";

	private static final String REPORTED_DATE = "Reported Date";

	Logger logger = LoggerFactory.getLogger(ClientSatisfactionThread.class);
	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Value("${SnapUserName}")
	String userName;

	@Value("${PassWord}")
	String password;


	@Value("${corrective_improvement_actions}")
	String complaintIssueType;

	@Value("${client_complaints}")
	String category;

	

	@Async
	public CompletableFuture<ArrayList<ComplaintData>> findCount(AtlassianHostUser hostUser, String sce, String back,
			String front, HttpSession session) throws JSONException, ParseException{
		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Category (CI/DI)' = {Category} AND 'Reported Date' >= {back} AND 'Reported Date' <= {front} order by 'Reported Date' DESC";

		String response1 = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder.fromUriString(urlPara)
								.buildAndExpand("\"" + sce + "\"", "\"" + complaintIssueType + "\"",
										"\"" + category + "\"", "\"" + back + "\"", "\"" + front + "\"")
								.toUri(),
						String.class);
		JSONObject response = new JSONObject(response1);

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		ArrayList<ComplaintData> data = new ArrayList<>();
		String rDate = fieldList.get(REPORTED_DATE);
		String action = fieldList.get(ACTION2);
		String rFrom = fieldList.get("If Client Complaint, Received from");
		JSONArray jsonArray = response.getJSONArray(ISSUES);
		DateFormat formatter = new SimpleDateFormat(RECDATE);
		SimpleDateFormat newFormat = new SimpleDateFormat(ACTUALDATE);
		
		
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
			ComplaintData complaintData = new ComplaintData();
			JSONObject fields = j.getJSONObject(FIELDS2);
			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			complaintData.setMonth(new SimpleDateFormat("MMM-yy").format(date));
			complaintData.setrDate(finalDate);
			complaintData.setDescription(fields.get(DESCRIPTION).toString());
			String actionField = fields.get(action).toString();
			if(actionField.equals("null")) {
			complaintData.setAction("-");
			}else {
				complaintData.setAction(fields.get(action).toString());	
			}
			String recFrom;
			try {
				recFrom = fields.getString(rFrom);
			} catch (Exception e) {
				recFrom = "-";
			}
			complaintData.setrFrom(recFrom);
			complaintData.setContext(fields.get("summary").toString());
			String obj = fields.get("status").toString();
			JSONObject sta = new JSONObject(obj);
			complaintData.setStatus(sta.get("name").toString());
			data.add(complaintData);
		}
		
		
		
		return CompletableFuture.completedFuture(data);
	}

	@Value("${client_appreciation}")
	String clientAppreciationIT;

	@Async
	public CompletableFuture<ArrayList<AppreciationData>> findCountOfAppreciation(AtlassianHostUser hostUser,
			String sce, String back, String front, HttpSession session) throws  ParseException {

		RestTemplate restTemplate = new RestTemplate();

		DateFormat formatter = new SimpleDateFormat(RECDATE);
		SimpleDateFormat newFormat = new SimpleDateFormat(ACTUALDATE);

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName + "@senecaglobal.com", password));

	
		String url = "/rest/api/2/search?jql=project={sce} AND issuetype= {Client_Appreciation_IT} AND 'Received Date' >= {back} AND 'Received Date' <= {front} order by 'Received Date' DESC";


		String response2 = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + sce + "\"",
						"\"" + clientAppreciationIT + "\"", "\"" + back + "\"", "\"" + front + "\"").toUri(),
				String.class);

		JSONObject response = new JSONObject(response2);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		ArrayList<AppreciationData> data = new ArrayList<>();
		String rDate = fieldList.get("Received Date");
		String rFrom = fieldList.get("Received from");
		String context = fieldList.get("Context");
		String rFor = fieldList.get("Received for");
		String details = fieldList.get("Appreciation Details");
		

		JSONArray jsonArray = response.getJSONArray(ISSUES);
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			AppreciationData appreciationData = new AppreciationData();
			JSONObject fields = j.getJSONObject(FIELDS2);
			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			appreciationData.setMonth(new SimpleDateFormat("MMM-yy").format(date));
			appreciationData.setdReceived(finalDate);
			appreciationData.setrFrom(fields.get(rFrom).toString());
			appreciationData.setContext(fields.get(context).toString());
			appreciationData.setrFor(fields.get(rFor).toString());
			appreciationData.setDetails(fields.get(details).toString());
			data.add(appreciationData);
		}

		return CompletableFuture.completedFuture(data);
	}

	@Value("${csfa_Category}")
	String cSFACategory;

	ClientSatisfactionFA clientSatisfactionFA;

	@Async
	public CompletableFuture<ArrayList<ClientSatisfactionFA>> findCountCSFA(AtlassianHostUser hostUser, String sce,
			String back, String front, HttpSession session){
		ArrayList<ClientSatisfactionFA> data = new ArrayList<>();
		try {
		clientSatisfactionFA = new ClientSatisfactionFA();

		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Category (CI/DI)' = {Category} AND 'Reported Date' >= {back} AND 'Reported Date' <= {front} order by 'Reported Date' DESC";

		String response1 = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(urlPara)
						.buildAndExpand("\"" + sce + "\"", "\"" + complaintIssueType + "\"",
								"\"" + cSFACategory + "\"", "\"" + back + "\"", "\"" + front + "\"")
						.toUri(),
				String.class);

		

		

		JSONObject response = new JSONObject(response1);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		String rDate = fieldList.get(REPORTED_DATE);
		String priority = fieldList.get("Priority");
		String rootCause = fieldList.get("Root Cause");
		String actionType = fieldList.get("Action Type");
		String action = fieldList.get(ACTION2);
		String responsibility = fieldList.get("Responsibility");
		String actualDate = fieldList.get("Actual Date");
		String targetDate = fieldList.get("Target Date");
		String remarks = fieldList.get("Remarks");

		JSONArray jsonArray = response.getJSONArray(ISSUES);
		DateFormat formatter = new SimpleDateFormat(RECDATE);
		SimpleDateFormat newFormat = new SimpleDateFormat(ACTUALDATE);
		for (int i = 0; i < jsonArray.length(); i++) {
			
			clientSatisfactionFA = new ClientSatisfactionFA();

			JSONObject j = jsonArray.getJSONObject(i);

			JSONObject fields = j.getJSONObject(FIELDS2);
			try {
			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			clientSatisfactionFA.setReported_Date(finalDate);
			}catch(Exception e) {
				clientSatisfactionFA.setReported_Date("-");	
			}
			clientSatisfactionFA.setIssue_Description(fields.get(DESCRIPTION).toString());
			clientSatisfactionFA.setPriority(fields.getJSONObject(priority).get("name").toString());
			String rc = fields.get(rootCause).toString();
			if(!rc.equals("null") && !rc.isEmpty()) {
			clientSatisfactionFA.setRoot_Cause(rc);
			}else {
				clientSatisfactionFA.setRoot_Cause("-");
			}
			try {
				JSONObject jobj = new JSONObject(fields.get(actionType).toString());
				clientSatisfactionFA.setAction_Type(jobj.get("value").toString());
			}catch(Exception e) {
				clientSatisfactionFA.setAction_Type("-");
			}
			String a = fields.get(action).toString();
			if(!a.equals("null") && !a.isEmpty()) {
			clientSatisfactionFA.setAction(a);
			}else {
				clientSatisfactionFA.setAction("-");
			}
			String res = "";
			try {
				JSONArray resData = fields.getJSONArray(responsibility);
				int length = resData.length();
				for(int h = 0; h < length; h++) {
					if(h != 0 && length > 1) {
						res = res + ", ";
					}
					res = res + resData.getJSONObject(h).get("displayName").toString();
					}
				clientSatisfactionFA.setResponsibility(res);	
			} catch (Exception e) {

				clientSatisfactionFA.setResponsibility("-");
				logger.error(e.getMessage());
			}
			try {
			String ad = fields.get(actualDate).toString();
			Date adate = formatter.parse(ad);
			String afinalDates = newFormat.format(adate);
			if(!ad.equals("null") && !ad.isEmpty()) {
			clientSatisfactionFA.setActual_Date(afinalDates);
			}else {
				clientSatisfactionFA.setActual_Date("-");
			}
			}catch(Exception e) {
				clientSatisfactionFA.setActual_Date("-");
			}
			try {
			String td = fields.get(targetDate).toString();
			Date tdate = formatter.parse(td);
			String tfinalDates = newFormat.format(tdate);
			if(!td.equals("null") && !td.isEmpty()) {	
			clientSatisfactionFA.setTarget_Date(tfinalDates);
			}
			else {
				clientSatisfactionFA.setTarget_Date("-");
			}
			}catch(Exception e) {
				clientSatisfactionFA.setTarget_Date("-");
			}
			String rmData = fields.get(remarks).toString();
			if(!rmData.equals("null") && !rmData.isEmpty()) {
			clientSatisfactionFA.setRemarks(rmData);
			}else {
				clientSatisfactionFA.setRemarks("-");	
			}

			data.add(clientSatisfactionFA);

		}
		}
		catch(Exception e) {
			logger.error(e.getMessage());
		}

		return CompletableFuture.completedFuture(data);
	}

	@Value("${client_complaints}")
	String cC_S;

	@Value("${csfa_Category}")
	String cSFA_S;
	@Value("${Project_Delivery_PI}")
	String pDPI_S;
	@Value("${Process_Complaince_Issue}")
	String pCI_S;

	@Value("${Information_Security}")
	String iSW_S;

	@Value("${Information_Security_Event}")
	String iSE_S;

	@Value("${Risk_Event}")
	String rE_S;

	@Value("${Delivery_Improvement}")
	String dI_S;

	@Value("${Creative_Idea}")
	String cI_S;

	CIDIData cidiAspect(String response1, AtlassianHostUser hostUser, HttpSession session) throws Exception, Exception {
		int ccImplementedOnTime = 0;
		int csfaImplementedOnTime = 0;
		int pdpiImplementedOmTime = 0;
		int pciImplementedOnTime = 0;
		int iswImplementedOnTime = 0;
		int iseImplementedOnTime = 0;
		int reImplementedOnTime = 0;
		int diImplementedOnTime = 0;
		int ciImplementedOnTime = 0;
		ArrayList<ClientSatisfactionFA> ccData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> csfaData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> pdpiData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> pciData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> iswData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> iseData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> reData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> diData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> ciData = new ArrayList<ClientSatisfactionFA>();
		CIDIData cidiData = new CIDIData();
		JSONObject response = new JSONObject(response1);
		int count = response.getInt(TOTAL);
		System.out.println(count);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		JSONArray jsonArray = response.getJSONArray(ISSUES);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
		ClientSatisfactionFA clientSatisfactionFA = getParsedData(fieldList,j );
		boolean dateFlag = clientSatisfactionFA.isImplementedOnTime();
		
		String category = clientSatisfactionFA.getCategory();
			if (category.equals(cC_S)) {
				ccData.add(clientSatisfactionFA);
				if(dateFlag) {
					ccImplementedOnTime++;
				}

			} else if (category.equals(cSFA_S)) {
				csfaData.add(clientSatisfactionFA);
				if(dateFlag) {
					csfaImplementedOnTime++;
				}

			} else if (category.equals(pDPI_S)) {

				pdpiData.add(clientSatisfactionFA);
				if(dateFlag) {
					pdpiImplementedOmTime++;
				}

			} else if (category.equals(pCI_S)) {

				pciData.add(clientSatisfactionFA);
				if(dateFlag) {
					pciImplementedOnTime++;
				}

			} else if (category.equals(iSW_S)) {

				iswData.add(clientSatisfactionFA);
				if(dateFlag) {
					iswImplementedOnTime++;
				}

			} else if (category.equals(iSE_S)) {

				iseData.add(clientSatisfactionFA);
				if(dateFlag) {
					iseImplementedOnTime++;
				}

			} else if (category.equals(rE_S)) {

				reData.add(clientSatisfactionFA);
				if(dateFlag) {
					reImplementedOnTime++;
				}

			} else if (category.equals(dI_S)) {

				diData.add(clientSatisfactionFA);
				if(dateFlag) {
					diImplementedOnTime++;
				}

			} else if (category.equals(cI_S)) {

				ciData.add(clientSatisfactionFA);
				if(dateFlag) {
					ciImplementedOnTime++;
				}

			}

		}
		double ccpercent = getPercent(ccImplementedOnTime,ccData.size());
		cidiData.setCcPercent(ccpercent);
		cidiData.setcCData(ccData);
		double cipercent = getPercent(ciImplementedOnTime,ciData.size());
		cidiData.setCiPercent(cipercent);
		cidiData.setcIData(ciData);
		double csfapercent = getPercent(csfaImplementedOnTime,csfaData.size());
		cidiData.setCsfaPercent(csfapercent);
		cidiData.setcSFAData(csfaData);
		double dipercent =  getPercent(diImplementedOnTime,diData.size());
		cidiData.setDiPercent(dipercent);
		cidiData.setdIData(diData);
		double isepercent = getPercent(iseImplementedOnTime , iseData.size());
		cidiData.setIsePercent(isepercent);
		cidiData.setiSEData(iseData);
		double iswpercent = getPercent(iswImplementedOnTime,iswData.size());
		cidiData.setIswPercent(iswpercent);
		cidiData.setiSWData(iswData);
		double pcipercent = getPercent(pciImplementedOnTime,pciData.size());
		cidiData.setPciPercent(pcipercent);
		cidiData.setpCIData(pciData);
		double pdpipercent = getPercent(pdpiImplementedOmTime,pdpiData.size());
		cidiData.setPdpiPercent(pdpipercent);
		cidiData.setpDPIData(pdpiData);
		double repercent = getPercent(reImplementedOnTime,reData.size());
		cidiData.setRePercent(repercent);
		cidiData.setrEData(reData);
		return cidiData;

	}
	public double getPercent(int implementedOnTime, double size) {
		if(size != 0) {
			return (Math.round(((implementedOnTime/size)*100)) * 100d)/100d;
		}else {
			return 0;
		}
	}

	@Async
	public CompletableFuture<CIDIData> findCIDICount(AtlassianHostUser hostUser, String sce, int num, HttpSession session) throws Exception {

		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Reported Date' >= startOfMonth({num}) AND 'Reported Date' <= endOfMonth({num}) order by 'Reported Date' DESC";

		String response1 = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder
								.fromUriString(urlPara).buildAndExpand("\"" + sce + "\"",
										"\"" + complaintIssueType + "\"", "\"" + num + "\"", "\"" + num + "\"")
								.toUri(),
						String.class);

		CIDIData data = cidiAspect(response1, hostUser, session);

		return CompletableFuture.completedFuture(data);
	}

	public ClientSatisfactionFA getParsedData(Map<String, String> fieldList, JSONObject response) throws JSONException, ParseException {
		String rDate = fieldList.get(REPORTED_DATE);
		String categoryFiled = fieldList.get("Category (CI/DI)");
		String rFrom = fieldList.get("If Client Complaint, Received from");
		String priority = fieldList.get("Priority");
		String rootCause = fieldList.get("Root Cause");
		String defectType = fieldList.get("Defect Type");
		String actionType = fieldList.get("Action Type");
		String action = fieldList.get(ACTION2);
		String reponsibility = fieldList.get("Responsibility");
		String actualDate = fieldList.get("Actual Date");
		String targetDate = fieldList.get("Target Date");
		String remarks = fieldList.get("Remarks");
		String status = fieldList.get("Status");

		DateFormat formatter = new SimpleDateFormat(RECDATE);
		SimpleDateFormat newFormat = new SimpleDateFormat(ACTUALDATE);
			clientSatisfactionFA = new ClientSatisfactionFA();


			JSONObject fields = response.getJSONObject(FIELDS2);

			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);

			clientSatisfactionFA.setReported_Date(finalDate);

			clientSatisfactionFA.setCategory(fields.getJSONObject(categoryFiled).get("value").toString());
			clientSatisfactionFA.setIssue_Description(fields.get(DESCRIPTION).toString());
			clientSatisfactionFA.setReceived_From(fields.get(rFrom).toString());
			clientSatisfactionFA.setPriority(fields.getJSONObject(priority).get("name").toString());
			String statusValue = "-";
			try{
				statusValue = fields.getJSONObject(status).get("name").toString();
			}catch(Exception e) {
				e.printStackTrace();
			}
			clientSatisfactionFA.setStatus(statusValue);
			String rc = fields.get(rootCause).toString();
			if(!rc.equals("null") && !rc.isEmpty()) {
				clientSatisfactionFA.setRoot_Cause(rc);
			}else {
				clientSatisfactionFA.setRoot_Cause("-");
			}
			
			
			
			try {
			clientSatisfactionFA.setDefect_Type(fields.getJSONObject(defectType).get("value").toString());
			}catch(Exception e){
				clientSatisfactionFA.setDefect_Type("-");
			}
			try {
				JSONObject jobj = fields.getJSONObject(actionType);
				String at = jobj.get("value").toString();
				if(!at.equals("null") && !at.isEmpty()) {
				clientSatisfactionFA.setAction_Type(at);
				}else {
					clientSatisfactionFA.setAction_Type("-");	
				}
				
			}catch(Exception e) {
				clientSatisfactionFA.setAction_Type("-");
			}
			String actionValue = fields.get(action).toString();
			if(!actionValue.equals("null") && !actionValue.isEmpty()) {
				clientSatisfactionFA.setAction(actionValue);
				}else {
					clientSatisfactionFA.setAction("-");	
				}
			String resvalue = "";

			try {
				JSONArray resArray = fields.getJSONArray(reponsibility);

				int length = resArray.length();
				for (int h = 0; h < length; h++) {
					if (h != 0 && length > 1) {

						resvalue = resvalue + ", ";
					}
					resvalue = resvalue + resArray.getJSONObject(h).get("displayName").toString();

				}
			} catch (Exception e) {
				clientSatisfactionFA.setResponsibility("-");
			}

			clientSatisfactionFA.setResponsibility(resvalue);
			String aFinalDate = "-";
			Date adate = new Date();
			try {
			String ad = fields.get(actualDate).toString();
			adate = formatter.parse(ad);
			
			if(!ad.equals("null") && !ad.isEmpty()) {
				aFinalDate = newFormat.format(adate);
				
			}
			}catch(Exception e) {
				e.printStackTrace();
			}
			clientSatisfactionFA.setActual_Date(aFinalDate);
			String tFinalDate = "-";
			Date tdate = new Date();
			try {
			String td = fields.get(targetDate).toString();
			tdate = formatter.parse(td);
			if(!td.equals("null") && !td.isEmpty()) {
				tFinalDate = newFormat.format(tdate);
			}
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(!(aFinalDate.equals("-")) && !(tFinalDate.equals("-"))) {
			int isActualDateWithInTargetDate = adate.compareTo(tdate);
				if(isActualDateWithInTargetDate <= 0) {
					clientSatisfactionFA.setImplementedOnTime(true);
				}
			}
			clientSatisfactionFA.setTarget_Date(tFinalDate);
			clientSatisfactionFA.setRemarks(fields.get(remarks).toString());
			String rm = fields.get(remarks).toString();
			if (!rm.equals("null") && !rm.isEmpty() ) {
				clientSatisfactionFA.setRemarks(fields.get(remarks).toString());
			}else {
				clientSatisfactionFA.setRemarks("-");
			}

			String category = fields.getJSONObject(categoryFiled).get("value").toString();
			clientSatisfactionFA.setCategory(category);
		return clientSatisfactionFA;
}
}

package com.jiraplugin.metrics;

import java.util.concurrent.Executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAsync

public class MetricsApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(MetricsApplication.class, args);
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5000);
		executor.setMaxPoolSize(5000);
		executor.setQueueCapacity(10000);
		executor.setThreadNamePrefix("jiraLookup-");
		executor.initialize();
		return executor;
	}
}

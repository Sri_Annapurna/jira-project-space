package com.jiraplugin.metrics.model;

public class AllGoalsData {
		Double csatRatingTarget, clientComplaintsTarget, recurrenceComplaintTarget, riskTarget, cidiTarget, reqDelOnTimeTarget;
		public Double getReqDelOnTimeTarget() {
			return reqDelOnTimeTarget;
		}
		public void setReqDelOnTimeTarget(Double reqDelOnTimeTarget) {
			this.reqDelOnTimeTarget = reqDelOnTimeTarget;
		}
		String meanCycleTimeEffort;
		
		public String getMeanCycleTimeEffort() {
			return meanCycleTimeEffort;
		}
		public void setMeanCycleTimeEffort(String meanCycleTimeEffort) {
			this.meanCycleTimeEffort = meanCycleTimeEffort;
		}
		public Double getRecurrenceComplaintTarget() {
			return recurrenceComplaintTarget;
		}
		public void setRecurrenceComplaintTarget(Double recurrenceComplaintTarget) {
			this.recurrenceComplaintTarget = recurrenceComplaintTarget;
		}
		public Double getCsatRatingTarget() {
			return csatRatingTarget;
		}
		public void setCsatRatingTarget(Double csatRatingTarget) {
			this.csatRatingTarget = csatRatingTarget;
		}
		public Double getClientComplaintsTarget() {
			return clientComplaintsTarget;
		}
		public void setClientComplaintsTarget(Double clientComplaintsTarget) {
			this.clientComplaintsTarget = clientComplaintsTarget;
		}
		public Double getRiskTarget() {
			return riskTarget;
		}
		public void setRiskTarget(Double riskTarget) {
			this.riskTarget = riskTarget;
		}
		public Double getCidiTarget() {
			return cidiTarget;
		}
		public void setCidiTarget(Double cidiTarget) {
			this.cidiTarget = cidiTarget;
		}
		public GoalsAndTargetsData getDevelopmentEffectivenessTarget() {
			return developmentEffectivenessTarget;
		}
		public void setDevelopmentEffectivenessTarget(GoalsAndTargetsData developmentEffectivenessTarget) {
			this.developmentEffectivenessTarget = developmentEffectivenessTarget;
		}
		GoalsAndTargetsData developmentEffectivenessTarget; 
}

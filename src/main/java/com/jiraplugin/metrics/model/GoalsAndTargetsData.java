package com.jiraplugin.metrics.model;

public class GoalsAndTargetsData {
	double revEffTarget, dreTarget, srfTarget, delDefectHigh, delDefectMedium, delDefectLow;
	String inProcessDefects;
	
	public String getInProcessDefects() {
		return inProcessDefects;
	}
	public void setInProcessDefects(String inProcessDefects) {
		this.inProcessDefects = inProcessDefects;
	}
	public double getDelDefectHigh() {
		return delDefectHigh;
	}
	public void setDelDefectHigh(double delDefectHigh) {
		this.delDefectHigh = delDefectHigh;
	}
	public double getDelDefectMedium() {
		return delDefectMedium;
	}
	public void setDelDefectMedium(double delDefectMedium) {
		this.delDefectMedium = delDefectMedium;
	}
	public double getDelDefectLow() {
		return delDefectLow;
	}
	public void setDelDefectLow(double delDefectLow) {
		this.delDefectLow = delDefectLow;
	}
	public double getRevEffTarget() {
		return revEffTarget;
	}
	public void setRevEffTarget(double revEffTarget) {
		this.revEffTarget = revEffTarget;
	}
	public double getDreTarget() {
		return dreTarget;
	}
	public void setDreTarget(double dreTarget) {
		this.dreTarget = dreTarget;
	}
	public double getSrfTarget() {
		return srfTarget;
	}
	public void setSrfTarget(double srfTarget) {
		this.srfTarget = srfTarget;
	}
	
	@Override
	   public String toString() {
	        return ("ReviewEff Target:"+this.getRevEffTarget()+
	                    " DRE Target: "+ this.getDreTarget() +
	                    " SRF Target: "+ this.getSrfTarget());
	   }
		
}

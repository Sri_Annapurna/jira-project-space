package com.jiraplugin.metrics.model;

import java.util.ArrayList;

public class RiskRatingData {

	private RiskRatingColumnData columnNames;

	private ArrayList<RiskRatingColumnData> data;

	public RiskRatingColumnData getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(RiskRatingColumnData columnNames) {
		this.columnNames = columnNames;
	}

	public ArrayList<RiskRatingColumnData> getData() {
		return data;
	}

	public void setData(ArrayList<RiskRatingColumnData> data) {
		this.data = data;
	}
}

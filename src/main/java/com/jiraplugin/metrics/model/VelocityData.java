package com.jiraplugin.metrics.model;

public class VelocityData {
	int committedStoryPoints, completedStoryPoints, reqMoved, sprintGoal;

	public int getSprintGoal() {
		return sprintGoal;
	}

	public void setSprintGoal(int sprintGoal) {
		this.sprintGoal = sprintGoal;
	}

	public int getReqMoved() {
		return reqMoved;
	}

	public void setReqMoved(int reqMoved) {
		this.reqMoved = reqMoved;
	}

	String sprintName;

	public String getSprintName() {
		return sprintName;
	}

	public void setSprintName(String sprintName) {
		this.sprintName = sprintName;
	}

	public int getCommittedStoryPoints() {
		return committedStoryPoints;
	}

	public void setCommittedStoryPoints(int committedStoryPoints) {
		this.committedStoryPoints = committedStoryPoints;
	}

	public int getCompletedStoryPoints() {
		return completedStoryPoints;
	}

	public void setCompletedStoryPoints(int completedStoryPoints) {
		this.completedStoryPoints = completedStoryPoints;
	}
	
	
}

package com.jiraplugin.metrics.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FieldCustomID implements Serializable {

	@JsonProperty("key")
	String key;

	@JsonProperty("name")
	String name;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

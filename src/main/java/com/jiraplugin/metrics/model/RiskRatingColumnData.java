package com.jiraplugin.metrics.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiskRatingColumnData {

	@JsonProperty("C0")
	private String C0;

	@JsonProperty("C1")
	private String C1;

	@JsonProperty("C2")
	private String C2;

	@JsonProperty("C3")
	private String C3;

	public String getC0() {
		return C0;
	}

	public void setC0(String c0) {
		C0 = c0;
	}

	public String getC1() {
		return C1;
	}

	public void setC1(String c1) {
		C1 = c1;
	}

	public String getC2() {
		return C2;
	}

	public void setC2(String c2) {
		C2 = c2;
	}

	public String getC3() {
		return C3;
	}

	public void setC3(String c3) {
		C3 = c3;
	}

}

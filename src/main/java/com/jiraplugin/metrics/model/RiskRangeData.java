package com.jiraplugin.metrics.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiskRangeData {
	@JsonProperty("created")
	String created;
	float riskValue;
	float residualRiskValue;
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public float getRiskValue() {
		return riskValue;
	}
	public void setRiskValue(float riskValue) {
		this.riskValue = riskValue;
	}
	public float getResidualRiskValue() {
		return residualRiskValue;
	}
	public void setResidualRiskValue(float residualRiskValue) {
		this.residualRiskValue = residualRiskValue;
	}
	
	
}

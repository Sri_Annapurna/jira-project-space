package com.jiraplugin.metrics.model;

public class CumulativeData {

	  int rHCumuCount; 
	  int rMCumuCount; 
	  int rLCumuCount; 
	  int rTCumuCount; 
	  int tHCumuCount; 
	  int tMCumuCount; 
	  int tLCumuCount; 
	  int tTCumuCount; 
	  int iHCumuCount; 
	  int iMCumuCount; 
	  int iLCumuCount; 
	  int iTCumuCount; 
	  int dHCumuCount; 
	  int dMCumuCount; 
	  int dLCumuCount; 
	  int dTCumuCount;
		
	public int getrHCumuCount() {
		return rHCumuCount;
	}

	public void setrHCumuCount(int rHCount) {
		this.rHCumuCount = rHCount;
	}

	public int getrMCumuCount() {
		return rMCumuCount;
	}

	public void setrMCumuCount(int rMCount) {
		this.rMCumuCount = rMCount;
	}

	public int getrLCumuCount() {
		return rLCumuCount;
	}

	public void setrLCumuCount(int rLCount) {
		this.rLCumuCount = rLCount;
	}

	public int getrTCumuCount() {
		return rTCumuCount;
	}

	public void setrTCumuCount(int rTCount) {
		this.rTCumuCount = rTCount;
	}

	public int gettHCumuCount() {
		return tHCumuCount;
	}

	public void settHCumuCount(int tHCount) {
		this.tHCumuCount = tHCount;
	}

	public int gettMCumuCount() {
		return tMCumuCount;
	}

	public void settMCumuCount(int tMCount) {
		this.tMCumuCount = tMCount;
	}

	public int gettLCumuCount() {
		return tLCumuCount;
	}

	public void settLCumuCount(int tLCount) {
		this.tLCumuCount = tLCount;
	}

	public int gettTCumuCount() {
		return tTCumuCount;
	}

	public void settTCumuCount(int tTCount) {
		this.tTCumuCount = tTCount;
	}

	public int getiHCumuCount() {
		return iHCumuCount;
	}

	public void setiHCumuCount(int iHCount) {
		this.iHCumuCount = iHCount;
	}

	public int getiMCumuCount() {
		return iMCumuCount;
	}

	public void setiMCumuCount(int iMCount) {
		this.iMCumuCount = iMCount;
	}

	public int getiLCumuCount() {
		return iLCumuCount;
	}

	public void setiLCumuCount(int iLCount) {
		this.iLCumuCount = iLCount;
	}

	public int getiTCumuCount() {
		return iTCumuCount;
	}

	public void setiTCumuCount(int iTCount) {
		this.iTCumuCount = iTCount;
	}

	public int getdHCumuCount() {
		return dHCumuCount;
	}

	public void setdHCumuCount(int dHCount) {
		this.dHCumuCount = dHCount;
	}

	public int getdMCumuCount() {
		return dMCumuCount;
	}

	public void setdMCumuCount(int dMCount) {
		this.dMCumuCount = dMCount;
	}

	public int getdLCumuCount() {
		return dLCumuCount;
	}

	public void setdLCumuCount(int dLCount) {
		this.dLCumuCount = dLCount;
	}

	public int getdTCumuCount() {
		return dTCumuCount;
	}

	public void setdTCumuCount(int dTCount) {
		this.dTCumuCount = dTCount;
	}

}

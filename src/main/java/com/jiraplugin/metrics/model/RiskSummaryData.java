package com.jiraplugin.metrics.model;

public class RiskSummaryData {
	String issueLink;
	String responsibility;
	String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponsibility() {
		return responsibility;
	}
	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}
	public String getIssueLink() {
		return issueLink;
	}
	public void setIssueLink(String issueLink) {
		this.issueLink = issueLink;
	}
	public String getIssueKey() {
		return issueKey;
	}
	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}
	public String getRiskDescription() {
		return riskDescription;
	}
	public void setRiskDescription(String riskDescription) {
		this.riskDescription = riskDescription;
	}
	public String getRiskProbability() {
		return riskProbability;
	}
	public void setRiskProbability(String riskProbability) {
		this.riskProbability = riskProbability;
	}
	public String getBusinessImpact() {
		return businessImpact;
	}
	public void setBusinessImpact(String businessImpact) {
		this.businessImpact = businessImpact;
	}
	public String getRiskValue() {
		return riskValue;
	}
	public void setRiskValue(String riskValue) {
		this.riskValue = riskValue;
	}
	public String getBusinessImpactDes() {
		return businessImpactDes;
	}
	public void setBusinessImpactDes(String businessImpactDes) {
		this.businessImpactDes = businessImpactDes;
	}
	public String getRiskAction() {
		return riskAction;
	}
	public void setRiskAction(String riskAction) {
		this.riskAction = riskAction;
	}
	
	
	String issueKey;
	String riskDescription;
	String riskProbability;
	String businessImpact;
	String riskValue;
	String businessImpactDes;
	String riskTreatmentOp;
	public String getRiskTreatmentOp() {
		return riskTreatmentOp;
	}
	public void setRiskTreatmentOp(String riskTreatmentOp) {
		this.riskTreatmentOp = riskTreatmentOp;
	}


	String riskAction;
 
	

}

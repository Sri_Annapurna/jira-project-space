package com.jiraplugin.metrics.model;

public class RiskOccurrenceData {

	String riskDescription; 
	String occurrenceDate;
	String EventResponseAction;
	String Responsibility;
	String status;
	String remarks;
	String issuelink;
	String key;
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getIssuelink() {
		return issuelink;
	}

	public void setIssuelink(String issuelink) {
		this.issuelink = issuelink;
	}

	
	public String getRiskDescription() {
		return riskDescription;
	}

	public void setRiskDescription(String riskDescription) {
		this.riskDescription = riskDescription;
	}

	public String getOccurrenceDate() {
		return occurrenceDate;
	}

	public void setOccurrenceDate(String occurrenceDate) {
		this.occurrenceDate = occurrenceDate;
	}

	public String getEventResponseAction() {
		return EventResponseAction;
	}

	public void setEventResponseAction(String eventResponseAction) {
		EventResponseAction = eventResponseAction;
	}

	public String getResponsibility() {
		return Responsibility;
	}

	public void setResponsibility(String responsibility) {
		Responsibility = responsibility;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}

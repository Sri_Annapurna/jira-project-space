package com.jiraplugin.metrics.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiskRating {
	
	@JsonProperty("fields")
	List<RiskRangeData> fields = new ArrayList<>();

	public List<RiskRangeData> getFields() {
		return fields;
	}

	public void setFields(List<RiskRangeData> fields) {
		this.fields = fields;
	}
	
}


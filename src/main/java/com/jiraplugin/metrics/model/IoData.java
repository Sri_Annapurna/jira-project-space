package com.jiraplugin.metrics.model;

public class IoData {
	String sno,processImprovements,actionPlan,piresponsibility,targetDate,actualDate,remarks;

	public String getPiresponsibility() {
		return piresponsibility;
	}

	public void setPiresponsibility(String piresponsibility) {
		this.piresponsibility = piresponsibility;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getProcessImprovements() {
		return processImprovements;
	}

	public void setProcessImprovements(String processImprovements) {
		this.processImprovements = processImprovements;
	}

	public String getActionPlan() {
		return actionPlan;
	}

	public void setActionPlan(String actionPlan) {
		this.actionPlan = actionPlan;
	}

	

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public String getActualDate() {
		return actualDate;
	}

	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}

package com.jiraplugin.metrics.model;

import java.util.ArrayList;
import java.util.Map;

public class RiskManagementData {

	public Map<String, ArrayList<RiskSummaryData>> getCriticalRiskCount() {
		return criticalRiskCount;
	}

	public void setCriticalRiskCount(Map<String, ArrayList<RiskSummaryData>> map) {
		this.criticalRiskCount = map;
	}

	public Map<String, String> getRiskAcceptablePercentage() {
		return riskAcceptablePercentage;
	}

	public void setRiskAcceptablePercentage(Map<String, String> riskAcceptablePercentage) {
		this.riskAcceptablePercentage = riskAcceptablePercentage;
	}

	public Map<String, String> getRiskOccurenceCount() {
		return riskOccurenceCount;
	}

	public void setRiskOccurenceCount(Map<String, String> riskOccurenceCount) {
		this.riskOccurenceCount = riskOccurenceCount;
	}

	public Map<String, ResultRangeData> getExposureValues() {
		return exposureValues;
	}

	public void setExposureValues(Map<String, ResultRangeData> map) {
		this.exposureValues = map;
	}

	Map<String, ArrayList<RiskSummaryData>> criticalRiskCount;
	Map<String, String> riskAcceptablePercentage;
	Map<String, String> riskOccurenceCount;
	Map<String, ResultRangeData> exposureValues;

}

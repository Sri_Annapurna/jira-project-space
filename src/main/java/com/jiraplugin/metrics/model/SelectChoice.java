package com.jiraplugin.metrics.model;

import java.time.LocalDate;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class SelectChoice {

	@NotNull
	 String projectname;

	 String atlassianConnectToken;

	
	 String quarter;

	public String getAtlassianConnectToken() {
		return atlassianConnectToken;
	}

	public void setAtlassianConnectToken(String atlassianConnectToken) {
		this.atlassianConnectToken = atlassianConnectToken;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}


	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	
	Date selectedDate;
	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	String startdate;

	String enddate;
	public String getCycle() {
		return cycle;
	}
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	String cycle;
	 
	String audityear;
public String getAudityear() {
	return audityear;
}
public void setAudityear(String audityear) {
	this.audityear = audityear;
}
	String catId;

public String getCatId() {
	return catId;
}

public void setCatId(String catId) {
	this.catId = catId;
}

}
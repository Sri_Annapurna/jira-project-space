package com.jiraplugin.metrics.model;

public class MeanCycleTimeData {
String month, minHighTime, maxHighTime, avgHighTime, minMedTime, maxMedTime, avgMedTime, minLowTime, maxLowTime, avgLowTime;

public String getMonth() {
	return month;
}

public void setMonth(String month) {
	this.month = month;
}

public String getMinHighTime() {
	return minHighTime;
}

public void setMinHighTime(String minHighTime) {
	this.minHighTime = minHighTime;
}

public String getMaxHighTime() {
	return maxHighTime;
}

public void setMaxHighTime(String maxHighTime) {
	this.maxHighTime = maxHighTime;
}

public String getAvgHighTime() {
	return avgHighTime;
}

public void setAvgHighTime(String avgHighTime) {
	this.avgHighTime = avgHighTime;
}

public String getMinMedTime() {
	return minMedTime;
}

public void setMinMedTime(String minMedTime) {
	this.minMedTime = minMedTime;
}

public String getMaxMedTime() {
	return maxMedTime;
}

public void setMaxMedTime(String maxMedTime) {
	this.maxMedTime = maxMedTime;
}

public String getAvgMedTime() {
	return avgMedTime;
}

public void setAvgMedTime(String avgMedTime) {
	this.avgMedTime = avgMedTime;
}

public String getMinLowTime() {
	return minLowTime;
}

public void setMinLowTime(String minLowTime) {
	this.minLowTime = minLowTime;
}

public String getMaxLowTime() {
	return maxLowTime;
}

public void setMaxLowTime(String maxLowTime) {
	this.maxLowTime = maxLowTime;
}

public String getAvgLowTime() {
	return avgLowTime;
}

public void setAvgLowTime(String avgLowTime) {
	this.avgLowTime = avgLowTime;
}

}
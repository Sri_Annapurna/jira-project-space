package com.jiraplugin.metrics.model;

public class EffortsData {
	Double totalReviewEffort, reviewEffort, totalUnitTestDesignReviewEffort, testableCount;

	public Double getTotalReviewEffort() {
		return totalReviewEffort;
	}

	public void setTotalReviewEffort(Double totalReviewEffort) {
		this.totalReviewEffort = totalReviewEffort;
	}

	public Double getReviewEffort() {
		return reviewEffort;
	}

	public void setReviewEffort(Double reviewEffort) {
		this.reviewEffort = reviewEffort;
	}

	public Double getTotalUnitTestDesignReviewEffort() {
		return totalUnitTestDesignReviewEffort;
	}

	public void setTotalUnitTestDesignReviewEffort(Double totalUnitTestDesignReviewEffort) {
		this.totalUnitTestDesignReviewEffort = totalUnitTestDesignReviewEffort;
	}

	public Double getTestableCount() {
		return testableCount;
	}

	public void setTestableCount(Double testableCount) {
		this.testableCount = testableCount;
	}
}

package com.jiraplugin.metrics.model;

public class EffortData {
	Double analysis_review_effort, design_review_effort, code_review_effort, unit_test_design_review_effort, re_by_developer, re_by_qa, testable_count;
	public Double getAnalysis_review_effort() {
		return analysis_review_effort;
	}
	public void setAnalysis_review_effort(Double analysis_review_effort) {
		this.analysis_review_effort = analysis_review_effort;
	}
	public Double getDesign_review_effort() {
		return design_review_effort;
	}
	public void setDesign_review_effort(Double design_review_effort) {
		this.design_review_effort = design_review_effort;
	}
	public Double getCode_review_effort() {
		return code_review_effort;
	}
	public void setCode_review_effort(Double code_review_effort) {
		this.code_review_effort = code_review_effort;
	}
	public Double getUnit_test_design_review_effort() {
		return unit_test_design_review_effort;
	}
	public void setUnit_test_design_review_effort(Double unit_test_design_review_effort) {
		this.unit_test_design_review_effort = unit_test_design_review_effort;
	}
	public Double getRe_by_developer() {
		return re_by_developer;
	}
	public void setRe_by_developer(Double re_by_developer) {
		this.re_by_developer = re_by_developer;
	}
	public Double getRe_by_qa() {
		return re_by_qa;
	}
	public void setRe_by_qa(Double re_by_qa) {
		this.re_by_qa = re_by_qa;
	}
	public Double getTestable_count() {
		return testable_count;
	}
	public void setTestable_count(Double testable_count) {
		this.testable_count = testable_count;
	}
	
	
}

package com.jiraplugin.metrics.model;

public class ConfluencePagesData {
		String atpURL, teURL, clientURL, networkURL, configurationURL,processURL, retroURL, momURL;
		public String getRetroURL() {
			return retroURL;
		}

		public void setRetroURL(String retroURL) {
			this.retroURL = retroURL;
		}

		public String getMomURL() {
			return momURL;
		}

		public void setMomURL(String momURL) {
			this.momURL = momURL;
		}


		public String getAtpURL() {
			return atpURL;
		}

		public void setAtpURL(String atpURL) {
			this.atpURL = atpURL;
		}

		public String getProcessURL() {
			return processURL;
		}

		public void setProcessURL(String processURL) {
			this.processURL = processURL;
		}

		public String getTeURL() {
			return teURL;
		}

		public void setTeURL(String teURL) {
			this.teURL = teURL;
		}

		public String getClientURL() {
			return clientURL;
		}

		public void setClientURL(String clientURL) {
			this.clientURL = clientURL;
		}

		public String getNetworkURL() {
			return networkURL;
		}

		public void setNetworkURL(String networkURL) {
			this.networkURL = networkURL;
		}

		public String getConfigurationURL() {
			return configurationURL;
		}

		public void setConfigurationURL(String configurationURL) {
			this.configurationURL = configurationURL;
		}
		
}

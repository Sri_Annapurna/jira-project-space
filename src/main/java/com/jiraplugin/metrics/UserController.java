
package com.jiraplugin.metrics;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jiraplugin.metrics.model.AllGoalsData;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;
import com.jiraplugin.metrics.model.ConfluencePagesData;
import com.jiraplugin.metrics.model.CumulativeData;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.EffectivenessData;
import com.jiraplugin.metrics.model.GoalsAndTargetsData;
import com.jiraplugin.metrics.model.MeanCycleTimeData;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.ProjectData;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskManagementData;
import com.jiraplugin.metrics.model.RiskOccurrenceData;
import com.jiraplugin.metrics.model.RiskSummaryData;
import com.jiraplugin.metrics.model.SelectChoice;
import com.jiraplugin.metrics.model.TestCase;
import com.jiraplugin.metrics.model.VelocityData;
import com.jiraplugin.metrics.service.AppreciationService;
import com.jiraplugin.metrics.service.CasualAnalysis;
import com.jiraplugin.metrics.service.CidiThreadsService;
import com.jiraplugin.metrics.service.ClientSatisfaction;
import com.jiraplugin.metrics.service.ClientSatisfactionDateService;
import com.jiraplugin.metrics.service.ClientSatisfactionService;
import com.jiraplugin.metrics.service.ClientSatisfactionThread;
import com.jiraplugin.metrics.service.ComplaintsService;
import com.jiraplugin.metrics.service.ComplianceThreadsService;
import com.jiraplugin.metrics.service.ConfluencePages;
import com.jiraplugin.metrics.service.CriticalDeliveryRisk;
import com.jiraplugin.metrics.service.CycleTimeThreadsService;
import com.jiraplugin.metrics.service.DEMonthsThread;
import com.jiraplugin.metrics.service.DeliveryService;
import com.jiraplugin.metrics.service.GoalsAndTargets;
import com.jiraplugin.metrics.service.GoalsAndTargetsCSATService;
import com.jiraplugin.metrics.service.GoalsAndTargetsRiskService;
import com.jiraplugin.metrics.service.GoalsAndTargetsService;
import com.jiraplugin.metrics.service.IntiativeThreadService;
import com.jiraplugin.metrics.service.JiraCustomFieldMap;
import com.jiraplugin.metrics.service.MetricsService;
import com.jiraplugin.metrics.service.MetricsThreads;
import com.jiraplugin.metrics.service.MonthWiseCycleTime;
import com.jiraplugin.metrics.service.MonthsService;
import com.jiraplugin.metrics.service.NewVelocityChart;
import com.jiraplugin.metrics.service.ProjectService;
import com.jiraplugin.metrics.service.RequestComplianceService;
import com.jiraplugin.metrics.service.RiskAcceptableExecutor;
import com.jiraplugin.metrics.service.RiskManagementService;
import com.jiraplugin.metrics.service.RiskService;
import com.jiraplugin.metrics.service.SprintService;
import com.jiraplugin.metrics.service.TestcaseMetricsService;
import com.jiraplugin.metrics.service.TimeAndEffort;
import com.jiraplugin.metrics.service.VelocityService;
import com.jiraplugin.metrics.service.WebHookHandler;

@Controller
@EnableJpaRepositories
public class UserController {
	
	@Value("${ClientSatisfactionSingle_IssueType}")
	String clientSatisfactionIT;

	@Value("${Quarter_Start_Date}")
	String qsd;
	
	@Value("${PassWord}")
	String password;
	
	int dateFlag = 0;
	
	Date selectedDate;
	
	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	ArrayList<String> projectList;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private MetricsService metricsService;

	@Autowired
	ClientSatisfaction clientsat;
	@Autowired
	private CasualAnalysis casualAnalysis;

	@Autowired
	ProjectService j;
	@Autowired
	VelocityService v;
	@Autowired
	ClientSatisfactionService cliSatSer;

	@Autowired
	GoalsAndTargets td = new GoalsAndTargets();
	GsonBuilder gsonBuilder = new GsonBuilder();
	Gson gson = gsonBuilder.create();
	
	
	Logger logger = LoggerFactory.getLogger(UserController.class);

	

	@GetMapping(value = "/cidiControl")
	public String cidiControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			 {
		isProjectSelected(mychoice, session, request);
		Map<String, CIDIData> result = new LinkedHashMap<>();
		try {
			result = cliSatSer.getCidiMethod(hostUser, getSelectedProjectFromSession(session), session);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		model.addAttribute("CIDI_Data", result);
		model.addAttribute("cidiTarget", getGoalsFromSession(session).getCidiTarget());
		return "cidiView";
	}

	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

	@GetMapping(value = "/clientComplaint")
	public String clientComplaint(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			{
		isProjectSelected(mychoice, session, request);
		Map<String, ArrayList<ComplaintData>> result = new LinkedHashMap<>();
		try {
			result = cliSatSer.getComplaint(hostUser, getSelectedProjectFromSession(session), session);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		for (Map.Entry<String, ArrayList<ComplaintData>> entry : result.entrySet()) {
			logger.info(entry.getKey());
			for (int i = 0; i < entry.getValue().size(); i++) {
			logger.info(ReflectionToStringBuilder.toString(entry.getValue().get(i)));
			}

		}
		model.addAttribute("comlaintsTarget",getGoalsFromSession(session).getClientComplaintsTarget());
		model.addAttribute("recurrenceComplaints",getGoalsFromSession(session).getRecurrenceComplaintTarget());
		model.addAttribute("data", result);
		return "complaints";
	}

	@GetMapping(value = "/clientAppreciation")
	public String clientAppreciation(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			 {
		isProjectSelected(mychoice, session, request);
		Map<String, ArrayList<AppreciationData>> result = new LinkedHashMap<>();
		try {
			result = cliSatSer.getAppreciation(hostUser, getSelectedProjectFromSession(session), session);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		model.addAttribute("data", result);
		return "appreciationReport";
	}

	@GetMapping(value = "/velocityProjectList")
	public String velocityProjectList(@ModelAttribute("mychoice") SelectChoice mychoice, Model model,
			@AuthenticationPrincipal AtlassianHostUser hostUser, HttpServletRequest request,
			HttpServletResponse response, HttpSession session){
		isProjectSelected(mychoice, session, request);
		if (getSelectedProjectFromSession(session) != null) {
			
			try {
				ArrayList<VelocityData> velocityData = ss.getVelocity(Integer.parseInt(session.getAttribute("selectedprojectID").toString()), hostUser, session);
				if(velocityData.size()!=0){
				
					JSONArray jsonVelocityData = new JSONArray(velocityData);
					JSONArray newVelocityChart = new JSONArray();
					
					
					for (int i = jsonVelocityData.length()-1; i >= 0; i--) {
						JSONArray j = new JSONArray();
						j.put(((JSONObject) jsonVelocityData.get(i)).getString("sprintName"));
						j.put(((JSONObject) jsonVelocityData.get(i)).getInt("committedStoryPoints"));
						j.put(Integer.toString(((JSONObject) jsonVelocityData.get(i)).getInt("committedStoryPoints")));
						j.put(((JSONObject) jsonVelocityData.get(i)).getInt("completedStoryPoints"));
						j.put(Integer.toString(((JSONObject) jsonVelocityData.get(i)).getInt("completedStoryPoints")));
						j.put(((JSONObject) jsonVelocityData.get(i)).getInt("sprintGoal"));
						j.put(Integer.toString(((JSONObject) jsonVelocityData.get(i)).getInt("sprintGoal")));
						newVelocityChart.put(j);
					}
					
					model.addAttribute("newVelocityChart",newVelocityChart);
					
				}
				else{
					model.addAttribute("newVelocityChart", 0);
				}
				model.addAttribute("velocityData", velocityData);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "newVelocityChart";
	}

	public void isProjectSelected(SelectChoice mychoice, HttpSession session, HttpServletRequest request) {
		try {

			String selectProjectFromSession = getSelectedProjectFromSession(session);

			if (selectProjectFromSession != null) {
			logger.error("Project Already Selected!");
			} else {

				setSelectedProjectToSession(mychoice, request);

				selectedDate = mychoice.getSelectedDate();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}



	@Autowired
	GoalsAndTargetsService gt;

	@Autowired
	TimeAndEffort te;
	
	@Autowired
	ConfluencePages cp;

	@RequestMapping(value = "/getTargetData", method = RequestMethod.GET)
	public String getTargetData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		// targetdata = gt.getGoalsAndTargetsData(hostUser, projectChosen);
		try {
		//	goals = td.getgoalsandtargets(hostUser, getSelectedProjectFromSession(session), session);
			
			//Getting Confluence pages
			//ConfluencePagesData c = cp.getConfluencePages(hostUser, getSelectedProjectFromSession(session),session);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		return "landingPage";
	}
	
	
	@RequestMapping(value = "/monthlyMetricsData", method = RequestMethod.GET)
	public String metricsData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, JSONException, InterruptedException, ExecutionException {
		int crhMe = 0, crmMe = 0, crlMe = 0, crtMe = 0, cthMe = 0, ctlMe = 0, ctmMe = 0, cttMe = 0, cihMe = 0, cimMe = 0, cilMe = 0, citMe = 0,
				cdhMe = 0, cdmMe = 0, cdlMe = 0, cdtMe = 0;
		isProjectSelected(mychoice, session, request);
		
		if (getSelectedProjectFromSession(session) != null) {
			ArrayList<MetricsData> listdata = new ArrayList<MetricsData>();
			if (session.getAttribute("selectedcategory").equals("Software Maintenance Release Mode") || session.getAttribute("selectedcategory").equals("Software Development") ) {
				try {
					System.out.println("enter into the method");
					listdata = ss.getSprintsMonthly(Integer.parseInt(session.getAttribute("selectedprojectID").toString()), hostUser,session);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
				model.addAttribute("type", "Iteration");
			} else {
				model.addAttribute("type", "Month-Year");
			
			try {
				listdata = metricsService.getMonthlyDefectsAsync(getSelectedProjectFromSession(session), hostUser,
						monthsDifference,session);
			} catch (Exception e) {

				logger.error(e.getMessage());
				e.printStackTrace();

			}
			}
				for (MetricsData monMet : listdata) {
					crhMe = crhMe + monMet.getD().getrHCount();
					crmMe = crmMe + monMet.getD().getrMCount();
					crlMe = crlMe + monMet.getD().getrLCount();
					crtMe = crtMe + monMet.getD().getrTCount();
					cthMe = cthMe + monMet.getD().gettHCount();
					ctlMe = ctlMe + monMet.getD().gettLCount();
					ctmMe = ctmMe + monMet.getD().gettMCount();
					cttMe = cttMe + monMet.getD().gettTCount();
					cihMe = cihMe + monMet.getD().getiHCount();
					cilMe = cilMe + monMet.getD().getiLCount();
					cimMe = cimMe + monMet.getD().getiMCount();
					citMe = citMe + monMet.getD().getiTCount();
					cdhMe = cdhMe + monMet.getD().getdHCount();
					cdlMe = cdlMe + monMet.getD().getdLCount();
					cdmMe = cdmMe + monMet.getD().getdMCount();
					cdtMe = cdtMe + monMet.getD().getdTCount();
				}
			
			model.addAttribute("result", listdata);
			model.addAttribute("crh", crhMe);
			model.addAttribute("crl", crlMe);
			model.addAttribute("crm", crmMe);
			model.addAttribute("crt", crtMe);
			model.addAttribute("cth", cthMe);
			model.addAttribute("ctm", ctmMe);
			model.addAttribute("ctl", ctlMe);
			model.addAttribute("ctt", cttMe);
			model.addAttribute("cih", cihMe);
			model.addAttribute("cil", cilMe);
			model.addAttribute("cim", cimMe);
			model.addAttribute("cit", citMe);
			model.addAttribute("cdl", cdlMe);
			model.addAttribute("cdm", cdmMe);
			model.addAttribute("cdh", cdhMe);
			model.addAttribute("cdt", cdtMe);
			
			model.addAttribute("delDefectHighTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectHigh());
			model.addAttribute("delDefectMediumTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectMedium());
			model.addAttribute("delDefectLowTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectLow());
			model.addAttribute("inprocessDefectsTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getInProcessDefects());
			
			if(listdata.size()!=0){
				
				JSONArray jsonData = new JSONArray(listdata);
		//		JSONObject jsonObj = new JSONObject();
	
				JSONArray DRE = new JSONArray();
	
				try {
					for (int i = jsonData.length()-1; i >= 0; i--) {
						JSONArray j = new JSONArray();
						j.put(((JSONObject) jsonData.get(i)).getString("month"));
						if (((JSONObject) jsonData.get(i)).getString("dreff").equals("N/A")) {
							j.put(0);
							j.put("N/A");
						} else {
							j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("dreff")));
							j.put(((JSONObject) jsonData.get(i)).getString("dreff"));
						}
						j.put(getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDreTarget());
						DRE.put(j);
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
	
				model.addAttribute("DRE", DRE);
				model.addAttribute("dreTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDreTarget());
	
				JSONArray defectsBar = new JSONArray();
	
				for (int i = jsonData.length()-1; i >= 0; i--) {
					JSONArray j = new JSONArray();
					j.put(((JSONObject) jsonData.get(i)).getString("month"));
					j.put(((JSONObject) jsonData.get(i)).getInt("deld"));
					j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("deld")));
					j.put(((JSONObject) jsonData.get(i)).getInt("total_review_defects"));
					j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("total_review_defects")));
					j.put(((JSONObject) jsonData.get(i)).getInt("tesd"));
					j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("tesd")));
					defectsBar.put(j);
				}
				model.addAttribute("defectsBar", defectsBar);
	
				JSONArray LineEff = new JSONArray();
	
				for (int i = jsonData.length()-1; i >= 0; i--) {
					JSONArray j = new JSONArray();
					j.put(((JSONObject) jsonData.get(i)).getString("month"));
					if (((JSONObject) jsonData.get(i)).getString("revef").equals("N/A")) {
						j.put(0);
						j.put("N/A");
					} else {
						j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("revef")));
						j.put(((JSONObject) jsonData.get(i)).getString("revef"));
					}
					j.put(getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getRevEffTarget());
					LineEff.put(j);
				}
	
				model.addAttribute("LineEff", LineEff);
				model.addAttribute("revTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getRevEffTarget());
	
				JSONArray TestLineEff = new JSONArray();
				JSONArray head = new JSONArray();
				head.put("Month");
				head.put("Testing Effectiveness(%)");
				TestLineEff.put(head);
	
				for (int i = jsonData.length()-1; i >= 0; i--) {
					JSONArray j = new JSONArray();
					j.put(((JSONObject) jsonData.get(i)).getString("month"));
					if (((JSONObject) jsonData.get(i)).getString("testeff").equals("N/A")) {
						j.put(0);
					} else {
						j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("testeff")));
					}
					TestLineEff.put(j);
				}
				model.addAttribute("TestLineEff", TestLineEff);
			}
		else{
			model.addAttribute("DRE", 0);
			model.addAttribute("defectsBar", 0);
			model.addAttribute("TestLineEff", 0);
			
		}
		}
		return "MonthlyMetricsData";

	}

	@RequestMapping(value = "/monthlymetrics", method = RequestMethod.GET)
	public String metricsReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, JSONException, InterruptedException, ExecutionException {

		
		String selectProjectFromSession = getSelectedProjectFromSession(session);
		
		if (selectProjectFromSession != null) {
			model.addAttribute("project_title", selectProjectFromSession);
			try{
			CompletableFuture<AllGoalsData> goals = td.getgoalsandtargets(hostUser, selectProjectFromSession, session);
			setGoalsToSession(goals.get(), request);
			
			CompletableFuture<ConfluencePagesData> cpUrl = cp.getConfluencePages(hostUser, selectProjectFromSession,session);
			setConfluenceURLToSession(cpUrl.get(), request);
			
			model.addAttribute("clientURL", cpUrl.get().getClientURL());
			model.addAttribute("configurationURL", cpUrl.get().getConfigurationURL());
			model.addAttribute("networkURL", cpUrl.get().getNetworkURL());
			model.addAttribute("teURL", cpUrl.get().getTeURL());
			model.addAttribute("processURL", cpUrl.get().getProcessURL());
			model.addAttribute("atpURL",cpUrl.get().getAtpURL());
			model.addAttribute("retroURL",cpUrl.get().getRetroURL());
			model.addAttribute("momURL",cpUrl.get().getMomURL());
			}
			catch(Exception e){
				System.out.println(e);
			}
			
		} else {
			setSelectedProjectToSession(mychoice, request);
			model.addAttribute("project_title", getSelectedProjectFromSession(session));
	try{
			CompletableFuture<AllGoalsData> goals = td.getgoalsandtargets(hostUser, getSelectedProjectFromSession(session), session);
				setGoalsToSession(goals.get(), request);
				
				CompletableFuture<ConfluencePagesData> cpUrl = cp.getConfluencePages(hostUser, getSelectedProjectFromSession(session),session);
				setConfluenceURLToSession(cpUrl.get(), request);
				
				model.addAttribute("clientURL", cpUrl.get().getClientURL());
				model.addAttribute("configurationURL", cpUrl.get().getConfigurationURL());
				model.addAttribute("networkURL", cpUrl.get().getNetworkURL());
				model.addAttribute("teURL", cpUrl.get().getTeURL());
				model.addAttribute("processURL", cpUrl.get().getProcessURL());
				model.addAttribute("atpURL",cpUrl.get().getAtpURL());
				model.addAttribute("retroURL",cpUrl.get().getRetroURL());
				model.addAttribute("momURL",cpUrl.get().getMomURL());
				
				}
				catch(Exception e){
					System.out.println(e);
				}
		}
		ArrayList<ProjectData> projectList = getProjectListFromSession(session);
		if(projectList == null) {
			projectList = projectService.getProjectData();
		}
		model.addAttribute("projectListSize", projectList.size());
		String category = session.getAttribute("selectedcategory").toString();
		if(category.equals("Software Maintenance Release Mode") || category.equals("Software Development") ) {
			model.addAttribute("tabName", "Velocity Chart");
			model.addAttribute("function", "velocityProjectList");
		}else {
			model.addAttribute("tabName", "Mean Cycle Time");
			model.addAttribute("function", "meanCycleTime");
		}
		return "MonthlyMetricsTable";		
	}

	public ArrayList<String> getMonths(String stDate, String enDate,HttpServletRequest request) throws ParseException {
		ArrayList<String> formattedDates = new ArrayList<String>();
		ArrayList<String> dates = new ArrayList<String>();
		SimpleDateFormat actualFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date sd = actualFormat.parse(stDate);
		Date ed = actualFormat.parse(enDate);
		String sDate = targetFormat.format(sd);
		String eDate = targetFormat.format(ed);
		
		Map<String, String> actualTargetDates = new LinkedHashMap<>();
		actualTargetDates.put("actualSD", stDate);
		actualTargetDates.put("actualED", enDate);
		actualTargetDates.put("targetSD", sDate);
		actualTargetDates.put("targetED", eDate);
		
		setDatesToSession(actualTargetDates, request);
		
		Date endDate = targetFormat.parse(eDate);
		Date startD = targetFormat.parse(sDate);
		dateFlag = 1;
		formattedDates.add(sDate);
		formattedDates.add(eDate);
		while (startD.compareTo(endDate) <= 0) {
			dates.add(new SimpleDateFormat("MMM-yy").format(startD));
			startD = DateUtils.addMonths(startD, 1);
		}
		Collections.reverse(dates);
		setDatesListMonthWise(dates, request);
		return formattedDates;
	}

	@RequestMapping(value = "/complianceRangeSelection", method = RequestMethod.GET)
	public String rangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageComp = "";
		int isValidUserComp = checkUser(hostUser,session);
		
		Map<String, Object> storeObjComp = new LinkedHashMap<>();
			storeObjComp.put("hostUser", hostUser);
			storeObjComp.put("mychoice", mychoice);
			storeObjComp.put("request", request);
			storeObjComp.put("response", response);
			storeObjComp.put("session", session);
			storeObjComp.put("secTitle", "Requirements Compliance");
			storeObjComp.put("jMethodValue", "requestCompliance");
			storeObjComp.put("pageNavigator", "complianceRangeSelection");
			
		if(isValidUserComp == 1) {
		if (mychoice.getCatId() != null) {
			setSelectedProjectToSession(mychoice, request);
		}
		viewPageComp = getSectionPage(storeObjComp, model);
		}else if(isValidUserComp == 0) {
			viewPageComp = "noAccessPage";
		}else if(isValidUserComp == 2) {
			viewPageComp = "loginErrorPage";
		}
		return viewPageComp;
	}

	@Autowired
	ComplianceThreadsService cts;

	@RequestMapping(value = "/requestCompliance", method = RequestMethod.GET)
	public String getRequestCompliance(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String sDate,eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		} 
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		List<EffectivenessData> result = cts.createComplianceThreads(getSelectedProjectFromSession(session), sDate,
				eDate, hostUser, getDatesListMonthWise(session), session);
		model.addAttribute("data", result);
		return "requestCompliance";
	}

	
	//mean Cycle Time for Date Range
	
	@RequestMapping(value = "/cycletimeRangeSelection", method = RequestMethod.GET)
	public String cycletimerangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageCyc = "";
		int isValidUserCyc = checkUser(hostUser,session);

		
		
		if(isValidUserCyc == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}
		if (getSelectedProjectFromSession(session) != null) {
			model.addAttribute("project_title", getSelectedProjectFromSession(session));
			model.addAttribute("section_title", "Mean Cycle Time");
			if(getGoalsFromSession(session)==null){
				CompletableFuture<AllGoalsData> goals = td.getgoalsandtargets(hostUser, getSelectedProjectFromSession(session), session);
				setGoalsToSession(goals.get(), request);
			}
			if(session.getAttribute("selectedcategory").equals("Software Maintenance Work Request Mode")){
			
			if(getDatesFromSession(session)!= null) {
				model.addAttribute("sDate", getDatesFromSession(session).get("actualSD"));
				model.addAttribute("eDate", getDatesFromSession(session).get("actualED"));
			}
			
			model.addAttribute("jMethod", "cycleTimeDatePages");
			viewPageCyc = "rangeSelection";
		}else{
			viewPageCyc = "noCycleTime";
		}
		}
		else {
			String user = getUser(hostUser);
			ArrayList<ProjectData> projectListFromSession = getProjectListFromSession(session);

			if (projectListFromSession != null) {
				model.addAttribute("pageNavigator", "cycletimeRangeSelection");
				model.addAttribute("projectlist", projectListFromSession);

				model.addAttribute("project_title", getSelectedProjectFromSession(session));

				viewPageCyc = "landingPage";

			} else {
				viewPageCyc = checkPermissionsToView("cycletimeRangeSelection", session, hostUser, request,response, model,4);
			}
		}
		}else if(isValidUserCyc == 0) {
			viewPageCyc = "noAccessPage";
		}else if(isValidUserCyc == 2) {
			viewPageCyc = "loginErrorPage";
		}
		return viewPageCyc;
	}
	
	@Autowired
	CycleTimeThreadsService cycleTimeDates;
	
	@RequestMapping(value = "/cycleTimeDatePages", method = RequestMethod.GET)
	public String getCycleTime(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String sDate,eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		} 
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		ArrayList<ArrayList<MeanCycleTimeData>> result = cycleTimeDates.createCycleTimeThreads(getSelectedProjectFromSession(session), sDate,
				eDate, hostUser, getDatesListMonthWise(session), session);
		
		model.addAttribute("data", result);
		return "cycleTimeDateRange";
	}
	
	
	@RequestMapping(value = "/cidiRangeSelection", method = RequestMethod.GET)
	public String getcidiRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageCidi = "";
		int isValidUserCidi = checkUser(hostUser, session);
		
		
		Map<String, Object> storeObjCidi = new LinkedHashMap<>();
		storeObjCidi.put("hostUser", hostUser);
		storeObjCidi.put("mychoice", mychoice);
		storeObjCidi.put("request", request);
		storeObjCidi.put("response", response);
		storeObjCidi.put("session", session);
		storeObjCidi.put("secTitle", "Creative Ideas and Delivery Improvements");
		storeObjCidi.put("jMethodValue", "cidiRangeData");
		storeObjCidi.put("pageNavigator", "cidiRangeSelection");
		
		
		if(isValidUserCidi == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}
		viewPageCidi = getSectionPage(storeObjCidi, model);
		}else if(isValidUserCidi == 0) {
			viewPageCidi = "noAccessPage";
		}else if(isValidUserCidi == 2) {
			viewPageCidi = "loginErrorPage";
		}
		return viewPageCidi;

	}

	@Autowired
	CidiThreadsService cs;

	@RequestMapping(value = "/cidiRangeData", method = RequestMethod.GET)
	public String getCIDIrange(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String sDate,eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		}
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		Map<String, CIDIData> result = cs.createCidiThreads(getSelectedProjectFromSession(session), sDate, eDate,
				hostUser, getDatesListMonthWise(session), session);
		model.addAttribute("CIDI_Data", result);
		String cidiobj = gson.toJson(result);
		model.addAttribute("cidiobj", cidiobj);
		model.addAttribute("cidiTarget", getGoalsFromSession(session).getCidiTarget());
		return "cidiDateRange";
	}

	@RequestMapping(value = "/clientRangeSelection", method = RequestMethod.GET)
	public String getclientRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
	String viewPageClie = "";
	int isValidUserClie = checkUser(hostUser,session);
	
	
	Map<String, Object> storeObjClie = new LinkedHashMap<>();
	storeObjClie.put("hostUser", hostUser);
	storeObjClie.put("mychoice", mychoice);
	storeObjClie.put("request", request);
	storeObjClie.put("response", response);
	storeObjClie.put("session", session);
	storeObjClie.put("secTitle", "Other Client Satisfaction Metrics");
	storeObjClie.put("jMethodValue", "clientFeedback");
	storeObjClie.put("pageNavigator", "clientRangeSelection");
	
	
		if(isValidUserClie == 1) {
			if(mychoice.getCatId() != null) {
			
			setSelectedProjectToSession(mychoice, request);
			 } 
			
		
			viewPageClie = getSectionPage(storeObjClie, model);
			
		}else if(isValidUserClie == 0) {
			viewPageClie = "noAccessPage";
		}else if(isValidUserClie == 2) {
			viewPageClie = "loginErrorPage";
		}
		return viewPageClie;

	}

	@Autowired
	RequestComplianceService rcs;
	@Autowired
	ClientSatisfactionThread cst;

	@RequestMapping(value = "/clientFeedback", method = RequestMethod.GET)
	public String getClientData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String sd, @RequestParam("enddate") String ed, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String sDate, eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(sd, ed,request);
		}
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");
		

		LinkedHashMap<String, ArrayList<ComplaintData>> complaintsData = new LinkedHashMap<String, ArrayList<ComplaintData>>();
		LinkedHashMap<String, ArrayList<AppreciationData>> appreciationsData = new LinkedHashMap<String, ArrayList<AppreciationData>>();
		ArrayList<String> dates = getDatesListMonthWise(session);
		for (String d : dates ) {
			ArrayList<ComplaintData> c = new ArrayList<ComplaintData>();
			ArrayList<AppreciationData> a = new ArrayList<AppreciationData>();
			complaintsData.put(d, c);
			appreciationsData.put(d, a);
		}

		CompletableFuture<LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>> csfa = rcs.getCidiData(
				getSelectedProjectFromSession(session), "Client Satisfaction Feedback Aspect", sDate, eDate, hostUser,
				getDatesListMonthWise(session), session);
		CompletableFuture<ArrayList<ComplaintData>> complaints = cst.findCount(hostUser,
				getSelectedProjectFromSession(session), sDate, eDate, session);
		CompletableFuture<ArrayList<AppreciationData>> appreciations = cst.findCountOfAppreciation(hostUser,
				getSelectedProjectFromSession(session), sDate, eDate, session);
		ArrayList<ComplaintData> complaintsd = complaints.get();
		ArrayList<AppreciationData> appreciationsd = appreciations.get();
		
		for (ComplaintData c : complaintsd) {
			ArrayList<ComplaintData> cd = complaintsData.get(c.getMonth());
			cd.add(c);
			complaintsData.put(c.getMonth(), cd);
		}
		for (AppreciationData a : appreciationsd) {
			ArrayList<AppreciationData> ad = appreciationsData.get(a.getMonth());
			ad.add(a);
			appreciationsData.put(a.getMonth(), ad);
		}
		model.addAttribute("csfa", csfa.get());

		String appreObj = gson.toJson(appreciationsData);
		String compObj = gson.toJson(complaintsData);
		String csfaObj = gson.toJson(csfa.get());
		model.addAttribute("caData", appreObj);
		model.addAttribute("ccData", compObj);
		model.addAttribute("csfaData", csfaObj);
		model.addAttribute("complaints", complaintsData);
		model.addAttribute("appreciations", appreciationsData);
		model.addAttribute("comlaintsTarget",getGoalsFromSession(session).getClientComplaintsTarget());
		model.addAttribute("recurrenceComplaints",getGoalsFromSession(session).getRecurrenceComplaintTarget());
		return "clientFeedback";
	}

	@RequestMapping(value = "/metricsRangeSelection", method = RequestMethod.GET)
	public String getMetricsRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageMet = "";
		int isValidUserMet = checkUser(hostUser,session);
		
		Map<String, Object> storeObjMet = new LinkedHashMap<>();
		storeObjMet.put("hostUser", hostUser);
		storeObjMet.put("mychoice", mychoice);
		storeObjMet.put("request", request);
		storeObjMet.put("response", response);
		storeObjMet.put("session", session);
		storeObjMet.put("secTitle", "Development Effectiveness");
		storeObjMet.put("jMethodValue", "effectivenessData");
		storeObjMet.put("pageNavigator", "metricsRangeSelection");
		
		
		if(isValidUserMet == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}
		viewPageMet = getSectionPage(storeObjMet, model);
		
		}else if(isValidUserMet == 0) {
			viewPageMet = "noAccessPage";
		}else if(isValidUserMet == 2) {
			viewPageMet = "loginErrorPage";
		}
		return viewPageMet;

	}

	@Autowired
	MetricsThreads mt;

	Map<String, MetricsData> metricsData = new LinkedHashMap<String, MetricsData>();

	@RequestMapping(value = "/effectivenessData", method = RequestMethod.GET)
	public String getDreData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Throwable {
		String sDate;
		String eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		} 
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		Map<String, MetricsData> result = mt.createMetricsThreads(getSelectedProjectFromSession(session),
				sDate, eDate, getDatesListMonthWise(session), hostUser,session);
		metricsData = result;
		model.addAttribute("result", result);
		model.addAttribute("dreTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDreTarget());
		model.addAttribute("revTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getRevEffTarget());
		return "metricsData";
	}

	@RequestMapping(value = "/distributionRangeSelection", method = RequestMethod.GET)
	public String getDistributionRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageDis = "";
	int isValidUserDis = checkUser(hostUser,session);
	
	Map<String, Object> storeObjDis = new LinkedHashMap<>();
	storeObjDis.put("hostUser", hostUser);
	storeObjDis.put("mychoice", mychoice);
	storeObjDis.put("request", request);
	storeObjDis.put("response", response);
	storeObjDis.put("session", session);
	storeObjDis.put("secTitle", "Defect Distribution Data");
	storeObjDis.put("jMethodValue", "defectDistributionData");
	storeObjDis.put("pageNavigator", "distributionRangeSelection");
	
	
		if(isValidUserDis == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}

		viewPageDis = getSectionPage(storeObjDis, model);
		
		}else if(isValidUserDis == 0) {
			viewPageDis = "noAccessPage";
		}else if(isValidUserDis == 2) {
			viewPageDis = "loginErrorPage";
		}
		return viewPageDis;

	}

	@RequestMapping(value = "/defectDistributionData", method = RequestMethod.GET)
	public String getDistributionData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Throwable {
		String sDate,eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		}
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		Map<String, MetricsData> result = mt.createMetricsThreads(getSelectedProjectFromSession(session),
				sDate, eDate, getDatesListMonthWise(session), hostUser,session);
		metricsData = result;
		int crh = 0, crm = 0, crl = 0, crt = 0, cth = 0, ctl = 0, ctm = 0, ctt = 0, cih = 0, cil = 0, cim = 0, cit = 0,
				cdh = 0, cdl = 0, cdm = 0, cdt = 0;
		for (Map.Entry<String, MetricsData> entry : metricsData.entrySet()) {
			MetricsData m = entry.getValue();
			crh = crh + m.getD().getrHCount();
			crm = crm + m.getD().getrMCount();
			crl = crl + m.getD().getrLCount();
			crt = crt + m.getD().getrTCount();
			cth = cth + m.getD().gettHCount();
			ctl = ctl + m.getD().gettLCount();
			ctm = ctm + m.getD().gettMCount();
			ctt = ctt + m.getD().gettTCount();
			cih = cih + m.getD().getiHCount();
			cil = cil + m.getD().getiLCount();
			cim = cim + m.getD().getiMCount();
			cit = cit + m.getD().getiTCount();
			cdh = cdh + m.getD().getdHCount();
			cdl = cdl + m.getD().getdLCount();
			cdm = cdm + m.getD().getdMCount();
			cdt = cdt + m.getD().getdTCount();
		}
		model.addAttribute("crh", crh);
		model.addAttribute("crl", crl);
		model.addAttribute("crm", crm);
		model.addAttribute("crt", crt);
		model.addAttribute("cth", cth);
		model.addAttribute("ctm", ctm);
		model.addAttribute("ctl", ctl);
		model.addAttribute("ctt", ctt);
		model.addAttribute("cih", cih);
		model.addAttribute("cil", cil);
		model.addAttribute("cim", cim);
		model.addAttribute("cit", cit);
		model.addAttribute("cdl", cdl);
		model.addAttribute("cdm", cdm);
		model.addAttribute("cdh", cdh);
		model.addAttribute("cdt", cdt);
		String dobj = gson.toJson(metricsData);
		model.addAttribute("result", metricsData);
		model.addAttribute("dobj", dobj);
		model.addAttribute("delDefectHighTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectHigh());
		model.addAttribute("delDefectMediumTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectMedium());
		model.addAttribute("delDefectLowTarget",getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getDelDefectLow());
		return "distributionData";
	}

	@RequestMapping(value = "/testRangeSelection", method = RequestMethod.GET)
	public String getTestRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String viewPageTes = "";
	int isValidUserTes = checkUser(hostUser, session);
	
	Map<String, Object> storeObjTes = new LinkedHashMap<>();
	storeObjTes.put("hostUser", hostUser);
	storeObjTes.put("mychoice", mychoice);
	storeObjTes.put("request", request);
	storeObjTes.put("response", response);
	storeObjTes.put("session", session);
	storeObjTes.put("secTitle", "Test Metrics");
	storeObjTes.put("jMethodValue", "testMetrics");
	storeObjTes.put("pageNavigator", "testRangeSelection");
	
	
		if(isValidUserTes == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}
		viewPageTes = getSectionPage(storeObjTes, model);
		
		}else if(isValidUserTes == 0) {
			viewPageTes = "noAccessPage";
		}else if(isValidUserTes == 2) {
			viewPageTes = "loginErrorPage";
		}
		return viewPageTes;

	}

	@Autowired
	TestcaseMetricsService tcm;

	@RequestMapping(value = "/testMetrics", method = RequestMethod.GET)
	public String getTestMetricsData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Throwable {
		String sDate, eDate;
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate,request);
		}
		
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");

		Map<String, MetricsData> metricsResult = mt.createMetricsThreads(getSelectedProjectFromSession(session), sDate, eDate, getDatesListMonthWise(session), hostUser,session);
		metricsData = metricsResult;
		Map<String, TestCase> result = tcm.getTestingMetricsData(getSelectedProjectFromSession(session),
				sDate, eDate, getDatesListMonthWise(session), metricsData, hostUser,session);
		model.addAttribute("result", result);
		model.addAttribute("srfTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getSrfTarget());
		return "testMetricsData";
	}

	/*
	 * @RequestMapping(value = "/cidiDateRange", method = RequestMethod.GET) public
	 * String getCidiData(@AuthenticationPrincipal AtlassianHostUser hostUser,
	 *
	 * @ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws
	 * Exception { getMonths(mychoice); ArrayList<EffectivenessData> result =
	 * cts.createComplianceThreads(projectChosen, sDate, eDate, hostUser, dates);
	 * model.addAttribute("data", result); return "iReport"; }
	 */
	int monthsDifference = 0;

	// CAUSAL ANALYSIS

	@Autowired
	private MonthsService monthsService;



	ArrayList<MetricsData> performanceList;

	@RequestMapping(value = "/tests", method = RequestMethod.GET)
	public String testmetrics(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws NumberFormatException, Exception {
		isProjectSelected(mychoice, session, request);
		
		ArrayList<TestCase> test_list = new ArrayList<TestCase>();
		if (getSelectedProjectFromSession(session) != null) {
			
			if (session.getAttribute("selectedcategory").equals("Software Maintenance Release Mode") || session.getAttribute("selectedcategory").equals("Software Development") ) {
				performanceList = ss.getSprintsMonthly(Integer.parseInt(session.getAttribute("selectedprojectID").toString()), hostUser,session);
				test_list = ss.getTestMetricsData(Integer.parseInt(session.getAttribute("selectedprojectID").toString()), hostUser, session,performanceList);
				model.addAttribute("type", "Iteration");
			} else {
				model.addAttribute("type", "Month-Year");
			
		
		try {
			performanceList = metricsService.getMonthlyDefects(getSelectedProjectFromSession(session), hostUser,
					monthsDifference,session);
			test_list = metricsService.getMonthlyTestMetrics(getSelectedProjectFromSession(session), hostUser,
					monthsDifference, performanceList,session);

		} catch (Exception e) {
			System.out.println(e);
		}
		}
		}
		model.addAttribute("result", test_list);
		
		if(test_list.size()!=0){
		JSONArray jsonData = new JSONArray(test_list);
		// JSONArray jsonTargets =new JSONArray(targetdata);
		JSONObject jsonObj = new JSONObject();
		JSONArray SRF = new JSONArray();
		for (int i = jsonData.length()-1; i >= 0; i--) {
			JSONArray j = new JSONArray();
			j.put(((JSONObject) jsonData.get(i)).getString("month"));
			if (((JSONObject) jsonData.get(i)).getString("srf").equals("N/A")) {
				j.put(0);
				j.put("N/A");
			} else {
				j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("srf")));
				j.put(((JSONObject) jsonData.get(i)).getString("srf"));
			}
			j.put(getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getSrfTarget());
			SRF.put(j);
		}

		model.addAttribute("srfData", SRF);
		
		}
		else
		{
			//model.addAttribute("srfTarget", goals.get(3));
			model.addAttribute("srfData", 0);
		}
		model.addAttribute("srfTarget", getGoalsFromSession(session).getDevelopmentEffectivenessTarget().getSrfTarget());
		return "TestsMetrics";

	}



	public ArrayList<String> collectProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) {

		CompletableFuture<Integer> fieldcust = projectService.getFieldKeys(hostUser);
		CompletableFuture.allOf(fieldcust);
		ArrayList<String> projects = projectService.getProjects(hostUser);
		model.addAttribute("projectlist", projects);
		return projects;

		//

	}

	@RequestMapping(value = "/landingPage", method = RequestMethod.GET)
	public String fieldlandingModelSeclection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, Throwable {
		String userId = "";
		try {
			System.out.println(session);
		userId = session.getAttribute("currentUser").toString();
		}catch(Exception e) {
			e.printStackTrace();
		}
		String hostUserId =hostUser.getUserAccountId().toString(); 
		if(!(hostUserId.equals(userId))) {
			setDatesToSession(null, request);
			setSelectedProjectToSession(null, request);
			setProjectListToSession(null,request);
			setCurrentUserId(hostUserId.toString(), request);
		}
		String viewPage = "";
		int isValidUser = checkUser(hostUser,session);
		if(isValidUser == 1	) {
			ArrayList<ProjectData> projectListFromSession = getProjectListFromSession(session);
		String sp = getSelectedProjectFromSession(session);
		if (projectListFromSession != null && sp != null) {
				viewPage = metricsReport(hostUser, mychoice, model, request, response, session);
		} else {
			viewPage = checkPermissionsToView("monthlymetrics",session,hostUser,request,response,model,0);
			}
		}else if(isValidUser == 0) {
			viewPage = "noAccessPage";
		}else if(isValidUser == 2) {
			viewPage = "loginErrorPage";
		}
		return viewPage;
	}

	@RequestMapping(value = "/changeSelectionPage", method = RequestMethod.GET)
	public String changeSelectionPage(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpSession session) throws IOException, InterruptedException, ExecutionException {
		/*projectChosen = null;*/
		setDatesToSession(null, request);
		setSelectedProjectToSession(null, request);
		ArrayList<ProjectData> projectListFromSession = getProjectListFromSession(session);
		if (projectListFromSession != null) {
			model.addAttribute("pageNavigator", "monthlymetrics");
			model.addAttribute("projectlist", projectListFromSession);

			model.addAttribute("project_title", getSelectedProjectFromSession(session));

			return "landingPage";

		} else {
			ArrayList<ProjectData> projectIdsMap = projectService.getProjectData();
			/*ArrayList<String> projects = collectProjectList(hostUser, mychoice, model);*/
			Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
			setProjectListToSession(projectIdsMap, request);
			setJiraFieldListToSession(fieldList, request);
			model.addAttribute("pageNavigator", "monthlymetrics");
			model.addAttribute("projectlist", projectIdsMap);

			return "landingPage";
		}

	}

	// About The Project

	@RequestMapping(value = "/aboutProjectInfo", method = RequestMethod.GET)
	public String fieldModelSeclection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice, session, request);

		Map<String, String> mapFieldsMain = new HashMap<String, String>();

		try {

			mapFieldsMain = criticalDeliveryRisk.riskOpenIssuesMethod(getSelectedProjectFromSession(session), hostUser);
		} catch (Exception e) {
			System.out.println(e);
		}

		model.addAttribute("project_title", getSelectedProjectFromSession(session));
		model.addAttribute("atpInfo", mapFieldsMain);
		return "aboutProjectViews";

	}

	

	@Autowired
	GoalsAndTargetsCSATService gtCSAT;
	
	// Client Satisfaction Control Start!

	@GetMapping(value = "/clientSatisfactionControl")
	public String clientSatisfactionControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session){
		Boolean ratingFlag = false;
		isProjectSelected(mychoice, session, request);
		Calendar cal;
		Map<String, String> userDateRange = new LinkedHashMap<>();
		cal = Calendar.getInstance();
		int monthCount = cal.get(Calendar.MONTH);
		int yearNumber = cal.get(Calendar.YEAR);

		getClientDateRange(userDateRange, monthCount, yearNumber);

		Map<String, ClientSatisfactionData> result = clientSatisfactionService.getClientRating(hostUser,
				getSelectedProjectFromSession(session), userDateRange, session);

	//	result.remove(result.entrySet().iterator().next().getKey());

		int a = 0;
		for (Map.Entry<String, ClientSatisfactionData> entry : result.entrySet()) {
			if ((entry.getValue().getCSAT_Q6()).equals("-")) {
				a += 1;
			} else {
				break;
			}
		}

		if (a != 4) {

			JSONArray cSAT = new JSONArray();
			ArrayList<String> keyList = new ArrayList<>(result.keySet());
			for (int i = 0; i < keyList.size(); i++) {
				JSONArray clientJD = new JSONArray();
				String key = keyList.get(i);
				clientJD.put(key);
				String val = result.get(key).getCSAT_Q6();
				if(!ratingFlag && val.contains("*")){
					ratingFlag = true;
				}
				getClientJD(clientJD, val);
				clientJD.put(getGoalsFromSession(session).getCsatRatingTarget());
				cSAT.put(clientJD);
			}
			model.addAttribute("CSAT", cSAT);
			model.addAttribute("CSATtarget", getGoalsFromSession(session).getCsatRatingTarget());
			model.addAttribute("ratingFlag", ratingFlag);

		} else {
			model.addAttribute("CSAT", 0);
		}
		model.addAttribute("csat", result);
		return "clientSatisfactionView";
	}

	private void getClientJD(JSONArray clientJD, String val) {
		int intValue = 0;
		try {
		String val1 = val.substring(0,1);
		 intValue = Integer.parseInt(val1);
		}catch(Exception e) {
			intValue = 0;
			val = "N/A";
		}
		
		clientJD.put(intValue);
		clientJD.put(val);
		clientJD.put(val);
	}

	private void getClientDateRange(Map<String, String> userDateRange, int monthCount, int yearNumber) {
		if (monthCount >= 0 && monthCount <= 3) {

			userDateRange.put("d1", (yearNumber - 1) + "/" + "01/01");
			userDateRange.put("d2", yearNumber + "/" + "04/30");

		} else if (monthCount >= 4 && monthCount <= 7) {

			userDateRange.put("d1", (yearNumber - 1) + "/" + "05/01");
			userDateRange.put("d2", yearNumber + "/" + "08/31");

		} else if (monthCount >= 8 && monthCount <= 11) {

			userDateRange.put("d1", (yearNumber - 1) + "/" + "09/01");
			userDateRange.put("d2", yearNumber + "/" + "12/31");

		}
	}
	
	
	
	// Client Satisfaction Control End!	
	

	@Autowired
	private RiskService rs;
	@Autowired
	RiskAcceptableExecutor riskAcceptableExecutor;
	@Autowired
	CriticalDeliveryRisk criticalDeliveryRisk;
	@Autowired
	GoalsAndTargetsRiskService gtRisk;
	
	@Value("${snapShotProject}")
	String snapProjName;
	
	@Value("${riskIdentifiedIssueValue}")
	String riskIdentifedIssueType;
	
	@Value("${riskAssessmentValue}")
	String riskAssessmentType;

	@RequestMapping(value = "/deliveryRiskControl", method = RequestMethod.GET)
	public String deliveryRiskControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice, session, request);
		// String snapProject = snapProjName;
		// Map<String, String> projkeys = projectService.getProjectsKeys();
		 // String pkey2 = projkeys.get(getSelectedProjectFromSession(session));
		
		String selectedPro = getSelectedProjectFromSession(session);


		
		try {

			

			CompletableFuture<ArrayList<ArrayList<String>>> acceptableLevelResults = riskAcceptableExecutor
					.startThreads(hostUser, selectedPro, session);

			// Delivery Risk Exposure Rating - After Risk Treatment
			String exposureRatingUrl = "/rest/api/2/search?jql=project='"+ selectedPro +"'AND issuetype='" + riskAssessmentType + "' AND 'Actual Date' != null AND status was not 'Not Applicable' on endOfMonth({month}) AND 'Reported Date'<=endOfMonth({month})&startAt=0&maxResults=100";

			CompletableFuture<Map<String, ResultRangeData>> exposureRatingResults = criticalDeliveryRisk.startExposureRatingThreads(hostUser,exposureRatingUrl, session);

			// Critical Delivery Risks Identified

			String riskUrl ="/rest/api/2/search?jql=project='" + selectedPro
					+ "' AND issuetype= '" + riskAssessmentType + "' AND 'Risk Probability (RP)'!='2' AND 'Risk Probability (RP)'!='1' AND status was not 'Not Applicable' on endOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) &startAt=0&maxResults=100";
			
			String totalRiskUrl ="/rest/api/2/search?jql=project='" + selectedPro
					+ "' AND issuetype= '" + riskAssessmentType + "'AND status was not 'Not Applicable' on endOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) &startAt=0&maxResults=100";
			CompletableFuture<Map<String, ArrayList<RiskSummaryData>>> totalRisks = criticalDeliveryRisk
					.startThreads(hostUser, totalRiskUrl, session);
			CompletableFuture<Map<String, ArrayList<RiskSummaryData>>> criticalDeliveryResults = criticalDeliveryRisk
					.startThreads(hostUser, riskUrl, session);
		
		
			// Risk Occurrence

			String riskUrl2 = "/rest/api/2/search?jql=project='" + selectedPro
					+ "' AND issuetype = 'Risk Assessment' AND 'Risk Occurrence Date' >= startOfMonth({date}) AND 'Risk Occurrence Date' <= endOfMonth({date})";

			CompletableFuture<Map<String, ArrayList<RiskOccurrenceData>>> riskOccurrenceResults = criticalDeliveryRisk.ocurrenceThreads(hostUser, riskUrl2, getJiraFieldListFromSession(session));

			// Risk Recurrence

			/*
			 * CompletableFuture<ArrayList<ArrayList<String>>> recurrenceResults =
			 * criticalDeliveryRisk .riskRecurrenceThreads(hostUser, snapProject, pkey2);
			 */

			// Wait until all the threads are done

			CompletableFuture.allOf(acceptableLevelResults, exposureRatingResults, criticalDeliveryResults,
					riskOccurrenceResults).join();

			Map<String, ResultRangeData> ratingResults = exposureRatingResults.get();
		

			JSONArray riskRatingLine = new JSONArray();

			for (Map.Entry<String,ResultRangeData> entry: ratingResults.entrySet())  {
				JSONArray j = new JSONArray();
				j.put(entry.getValue().getMonth());
				int avgRiskValues = entry.getValue().getAvg_risk(); 
				j.put(avgRiskValues);
				if (avgRiskValues == 0) {
					j.put("N/A");
					j.put("N/A");
				} else {
					j.put(Integer.toString(avgRiskValues));
					j.put(Integer.toString(avgRiskValues));
				}
				int avgResidualRiskValues = entry.getValue().getAvg_residual();
				j.put(avgResidualRiskValues);
				if (avgResidualRiskValues == 0) {
					j.put("N/A");
					j.put("N/A");
				} else {
					j.put(Integer.toString(avgResidualRiskValues));
					j.put(Integer.toString(avgResidualRiskValues));
				}
				// j.put(((JSONObject)
				// jsonTargets.get(0)).getDouble("riskExpoTarget"));
				j.put(getGoalsFromSession(session).getRiskTarget());
				riskRatingLine.put(j);
				System.out.println(ReflectionToStringBuilder.toString(riskRatingLine));
			}

			model.addAttribute("riskRatingLine", riskRatingLine);
			model.addAttribute("riskTarget", getGoalsFromSession(session).getRiskTarget());

			model.addAttribute("project_title", getSelectedProjectFromSession(session));
			model.addAttribute("alr", acceptableLevelResults.get());
			model.addAttribute("err", exposureRatingResults.get());
			model.addAttribute("cdr", criticalDeliveryResults.get());
			model.addAttribute("tr", totalRisks.get());
			model.addAttribute("ror", riskOccurrenceResults.get());
			String riskObj = gson.toJson(criticalDeliveryResults.get());
			model.addAttribute("criObj", riskObj);
			/* model.addAttribute("rr", recurrenceResults.get()); */
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		
		}
		return "deliveryRiskView";
	}

	@Autowired
	private WebHookHandler obj;

	@ResponseBody
	@RequestMapping(consumes = "application/json", produces = "application/json", value = "/issueUpdated", method = RequestMethod.POST)
	public String issueUpdated(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody String json, HttpSession session)
			throws ServletException, IOException, JSONException {

		int errorFlag = 0;
		try {

			String receivedData = json;

			System.out.println("webhook issue updated   " + json);

			obj.updateIssue(json, hostUser, session);
		} catch (Exception e) {
			System.out.println(e);
			errorFlag = 1;
		}

		if (errorFlag == 1) {
			return "webHookDataErrorView";
		} else {
			return "webHookData";
		}

	}



	@Autowired
	DeliveryService d;

	// Requirement Compliance
	@RequestMapping(value = "/iReport", method = RequestMethod.GET)
	public String iReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {
		isProjectSelected(mychoice, session, request);
		if (getSelectedProjectFromSession(session) != null) {
	
			ArrayList<EffectivenessData> data = new ArrayList<EffectivenessData>();
			int errorFlag = 0;
			String category = session.getAttribute("selectedcategory").toString();
			System.out.println("category" + category);
				if (session.getAttribute("selectedcategory").equals("Software Maintenance Release Mode") || session.getAttribute("selectedcategory").equals("Software Development Project") ) {
					try {
						data = ss.getSprintsIReport(Integer.parseInt(session.getAttribute("selectedprojectID").toString()), hostUser);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
					model.addAttribute("metricName", "Requirements Delivered On-time (%)");
					model.addAttribute("type", "Iteration");
				}
		else{
			try {
	
				model.addAttribute("projectname", getSelectedProjectFromSession(session));
				data = d.runThreadsOne(getSelectedProjectFromSession(session), hostUser);
				model.addAttribute("type", "Month-Year");
				model.addAttribute("metricName", "Work Requests Delivered On-time (%)");
			} catch (Exception e) {
				System.out.println(e);
			}
		}
			model.addAttribute("data", data);
			model.addAttribute("reqDelOnTimeTarget", getGoalsFromSession(session).getReqDelOnTimeTarget());
			model.addAttribute("project_title", getSelectedProjectFromSession(session));
			}
		return "iReport";
	}
	
	//Mean Cycle Time
	
	@Autowired
	MonthWiseCycleTime ct;
	
	@RequestMapping(value = "/meanCycleTime", method = RequestMethod.GET)
	public String meanCycleTime(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {
		isProjectSelected(mychoice, session, request);
		ArrayList<ArrayList<MeanCycleTimeData>> a= new ArrayList<ArrayList<MeanCycleTimeData>>();
		if(session.getAttribute("selectedcategory").equals("Software Maintenance Work Request Mode")) {
		try{
		a=ct.CycleTimeMonthThreads(getSelectedProjectFromSession(session), hostUser,session);
		
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		}
		model.addAttribute("cycletime",a);
		return "cycleTimeView";
	}
	
	


	// Defect Distribution Data
	@Autowired
	DEMonthsThread t;

	@RequestMapping(value = "/defectsReport", method = RequestMethod.GET)
	public String defectsReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {
		isProjectSelected(mychoice, session, request);
		if (getSelectedProjectFromSession(session) != null) {
			int rHCount = 0, rMCount = 0, rLCount = 0, rTCount = 0, tHCount = 0, tMCount = 0, tLCount = 0, tTCount = 0,
					iHCount = 0, iMCount = 0, iLCount = 0, iTCount = 0, dHCount = 0, dMCount = 0, dLCount = 0,
					dTCount = 0;
			int errorFlag = 0;
			// String p = null;
			try {
				// p = mychoice.getProjectname();
				model.addAttribute("project_title", getSelectedProjectFromSession(session));
				ArrayList<DEffectivenessData> data = t.runMonthsThreads(getSelectedProjectFromSession(session),
						hostUser);
				model.addAttribute("data", data);
				CumulativeData cumulativeData = new CumulativeData();
				for (DEffectivenessData d : data) {
					rHCount += d.getrHCount();
					rMCount += d.getrMCount();
					rLCount += d.getrLCount();
					rTCount += d.getrTCount();
					tHCount += d.gettHCount();
					tMCount += d.gettMCount();
					tLCount += d.gettLCount();
					tTCount += d.gettTCount();
					iHCount += d.getiHCount();
					iLCount += d.getiLCount();
					iMCount += d.getiMCount();
					iTCount += d.getiTCount();
					dHCount += d.getdHCount();
					dMCount += d.getdMCount();
					dLCount += d.getdLCount();
					dTCount += d.getdTCount();
				}
				cumulativeData.setrHCumuCount(rHCount);
				cumulativeData.setrMCumuCount(rMCount);
				cumulativeData.setrLCumuCount(rLCount);
				cumulativeData.setrTCumuCount(rTCount);
				cumulativeData.settHCumuCount(tHCount);
				cumulativeData.settLCumuCount(tLCount);
				cumulativeData.settMCumuCount(tMCount);
				cumulativeData.settTCumuCount(tTCount);
				cumulativeData.setiHCumuCount(iHCount);
				cumulativeData.setiLCumuCount(iLCount);
				cumulativeData.setiMCumuCount(iMCount);
				cumulativeData.setiTCumuCount(iTCount);
				cumulativeData.setdHCumuCount(dHCount);
				cumulativeData.setdLCumuCount(dLCount);
				cumulativeData.setdMCumuCount(dMCount);
				cumulativeData.setdTCumuCount(dTCount);
				model.addAttribute("cData", cumulativeData);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;
			}
		}
		return "defectsReport";

	}

	// Delivery Improvement Initiatives

	@Autowired
	IntiativeThreadService r;



	// Client Complaints
	@Autowired
	ComplaintsService c;

	
	@RequestMapping(value = "/cSFAControlMethod", method = RequestMethod.GET)
	public String cSFAControlMethod(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice, session, request);
		Map<String, ArrayList<ClientSatisfactionFA>> result = new LinkedHashMap<String, ArrayList<ClientSatisfactionFA>>();

		int errorFlag = 0;

		try {

			result = cliSatSer.getCSFA(hostUser, getSelectedProjectFromSession(session), session);

		} catch (Exception e) {
			errorFlag = 1;
		}
		model.addAttribute("dataCFA", result);

		if (result.isEmpty()) {

			model.addAttribute("selectedIssueType", "Client Satisfaction Feedback Aspect");
			return "errorView";

		} else if (errorFlag == 1) {
			model.addAttribute("selectedIssueType", "Client Satisfaction Feedback Aspect");
			return "exceptionErrorView";
		} else {

			return "csfAspect";
		}

	}
	

	@RequestMapping(value = "/appreciationPList", method = RequestMethod.GET)
	public String appreciationProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws IOException {
		List<String> projects = projectService.getProjects(hostUser);
		model.addAttribute("projectlist", projects);
		return "appreciationProjectList";
	}

	@Autowired
	AppreciationService a;



	@RequestMapping(value = "/modeProjectList", method = RequestMethod.GET)
	public String modeProjectList(@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException {
		ArrayList<ProjectData> projectIdsMap = projectService.getProjectData();
		model.addAttribute("projectlist", projectIdsMap);
		return "modeProjectList";
	}

	@Autowired
	SprintService ss;

	
	// Risk Management Date Range Service

	@Autowired
	RiskManagementService riskManagementService;

	@GetMapping(value = "/riskManagmentDateRangeProjectList")
	public String riskManagmentDateRangeProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception, Throwable {
		String viewPageRis = "";
		int isValidUserRis = checkUser(hostUser,session);
		
		Map<String, Object> storeObjRis = new LinkedHashMap<>();
		storeObjRis.put("hostUser", hostUser);
		storeObjRis.put("mychoice", mychoice);
		storeObjRis.put("request", request);
		storeObjRis.put("response", response);
		storeObjRis.put("session", session);
		storeObjRis.put("secTitle", "Risk Management");
		storeObjRis.put("jMethodValue", "riskManagementDateRangeControl");
		storeObjRis.put("pageNavigator", "riskManagmentDateRangeProjectList");
		
		if(isValidUserRis == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}
		viewPageRis = getSectionPage(storeObjRis, model);
		
		}else if(isValidUserRis == 0) {
			viewPageRis = "noAccessPage";
		}else if(isValidUserRis == 2) {
			viewPageRis = "loginErrorPage";
		}
		return viewPageRis;

	}

	@GetMapping(value = "/riskManagementDateRangeControl")
	public String riskManagementDateRangeControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse reponse, HttpSession session) throws ParseException {
		String sDate,eDate;
		Map<String, String> userDateRange = new LinkedHashMap<>();

		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate, request);
		} 
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");
		userDateRange.put("d1", sDate);
		userDateRange.put("d2", eDate);
		if(getSelectedProjectFromSession(session) != null) {
			
			RiskManagementData result = riskManagementService.getRiskRangeData(hostUser,
					getSelectedProjectFromSession(session), userDateRange, session);

			model.addAttribute("res", result);
			String riskObj = gson.toJson(result.getCriticalRiskCount());
			model.addAttribute("riskObj", riskObj);
			model.addAttribute("riskTarget", getGoalsFromSession(session).getRiskTarget());
			return "riskManagementDateRangeView";
			
		}else {
			
			return "landingPage";
			
		}

		
	}

	@GetMapping(value = "/clientSatisfactionDateProjectList")
	public String riskManagementDateRangeProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Throwable{

		String viewPageCli = "";
		int isValidUserCli = checkUser(hostUser,session);
		Map<String, Object> storeObjCli = new LinkedHashMap<>();
		storeObjCli.put("hostUser", hostUser);
		storeObjCli.put("mychoice", mychoice);
		storeObjCli.put("request", request);
		storeObjCli.put("response", response);
		storeObjCli.put("session", session);
		storeObjCli.put("secTitle", "Client Satisfaction Rating");
		storeObjCli.put("jMethodValue", "clientSatisfactionDateRangeControl");
		storeObjCli.put("pageNavigator", "clientSatisfactionDateProjectList");
		if(isValidUserCli == 1) {
		if (mychoice.getCatId() != null) {

			setSelectedProjectToSession(mychoice, request);
		}

		viewPageCli = getSectionPage(storeObjCli, model);
		
		}else if(isValidUserCli == 0) {
			viewPageCli = "noAccessPage";
		}else if(isValidUserCli == 2) {
			viewPageCli = "loginErrorPage";
		}
		return viewPageCli;
	}
		
	public String getSectionPage(Map<String, Object> storeObj, Model model) throws Throwable {
		
		
			String viewPageLoc = null;
		

			if (getSelectedProjectFromSession((HttpSession)storeObj.get("session")) != null) {
				model.addAttribute("project_title", getSelectedProjectFromSession((HttpSession)storeObj.get("session")));
				if(getGoalsFromSession((HttpSession)storeObj.get("session"))==null){
					CompletableFuture<AllGoalsData> goals = td.getgoalsandtargets((AtlassianHostUser)storeObj.get("hostUser"), getSelectedProjectFromSession((HttpSession)storeObj.get("session")), (HttpSession)storeObj.get("session"));
					setGoalsToSession(goals.get(), (HttpServletRequest) storeObj.get("request"));
				}
				if(getDatesFromSession((HttpSession)storeObj.get("session"))!= null) {
					model.addAttribute("sDate", getDatesFromSession((HttpSession)storeObj.get("session")).get("actualSD"));
					model.addAttribute("eDate", getDatesFromSession((HttpSession)storeObj.get("session")).get("actualED"));
				}
				model.addAttribute("section_title", (String)storeObj.get("secTitle")); 
				model.addAttribute("jMethod", (String)storeObj.get("jMethodValue")); 
				viewPageLoc = "rangeSelection";
			} else {
				String user = getUser((AtlassianHostUser)storeObj.get("hostUser"));
				ArrayList<ProjectData> projectListFromSession = getProjectListFromSession((HttpSession)storeObj.get("session"));

				if (projectListFromSession != null) {
					model.addAttribute("pageNavigator", (String)storeObj.get("pageNavigator"));  
					model.addAttribute("projectlist", projectListFromSession);

					model.addAttribute("project_title", getSelectedProjectFromSession((HttpSession)storeObj.get("session")));

					viewPageLoc = "landingPage";

				} else {
					viewPageLoc = checkPermissionsToView((String) storeObj.get("pageNavigator"), (HttpSession)storeObj.get("session"), (AtlassianHostUser)storeObj.get("hostUser"), (HttpServletRequest) storeObj.get("request"), (HttpServletResponse)storeObj.get("response"), model,1);
				}
			}
				
		
		return viewPageLoc;
	}

	@Autowired
	ClientSatisfactionDateService clientSatisfactionService;

	@GetMapping(value = "/clientSatisfactionDateRangeControl")
	public String clientSatisfactionDateRangeControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String stDate, @RequestParam("enddate") String edDate, Model model,
			HttpServletRequest request, HttpServletResponse reponse, HttpSession session) throws ParseException {
		String sDate,eDate;
		Map<String, String> userDateRange = new LinkedHashMap<>();
		Map<String,String> sessionDates = getDatesFromSession(session);
		if(sessionDates == null || sessionDates.toString().equals("{}")) {
			getMonths(stDate, edDate, request);
		
		}
		sDate = getDatesFromSession(session).get("targetSD");
		eDate = getDatesFromSession(session).get("targetED");
		userDateRange.put("d1", sDate);
		userDateRange.put("d2", eDate);

		Map<String, ClientSatisfactionData> result = clientSatisfactionService.getClientRating(hostUser,
				getSelectedProjectFromSession(session), userDateRange, session);

		String csatjson = gson.toJson(result);

		model.addAttribute("csat", result);
		model.addAttribute("csatJObj", csatjson);
		model.addAttribute("CSATtarget", getGoalsFromSession(session).getCsatRatingTarget());

		return "clientSatisfactionDateRangeView";
	}
	
	
	

	// Reset Session Dates

	@GetMapping(value = "/restDateRangeSelection")
	public String restDateRangeSelection(@AuthenticationPrincipal AtlassianHostUser hostUser, Model model,
			HttpSession session, HttpServletRequest request, @ModelAttribute("mychoice") SelectChoice mychoice) {
		
		setDatesToSession(null, request);
		setDatesListMonthWise(null, request);
		
		return "landingPage";
	}

	public void setCurrentUserId(String userId, HttpServletRequest request)
	{
		request.getSession().setAttribute("currentUser", userId);
	}
	
	// Dates from Session
	
	public String getCurrentId(HttpSession session){
		return session.getAttribute("currentUser").toString();
	}
	
	// Dates to Session
	
	public void setDatesToSession(Map<String, String> parameterDates, HttpServletRequest request)
	{
		String actualTargetDates = new JSONObject(parameterDates).toString();
		request.getSession().setAttribute("ActualTargetDates", actualTargetDates);
	}
	
	// Dates from Session
	
	public Map<String, String> getDatesFromSession(HttpSession session){
		Map<String, String> result = DataStringToMap(session, "ActualTargetDates");
		return result;
	}

	// Project Session

	public void setSelectedProjectToSession(SelectChoice mychoice, HttpServletRequest request) {

		String projectChosenSe;
		String projectID;
		String category;
		if(mychoice != null) {
		String id = mychoice.getCatId();
		String[] arrOfStr = id.split("~");
		projectChosenSe = arrOfStr[2];
		projectID = arrOfStr[0];
		category = arrOfStr[1];
		
		}
		else {
			projectChosenSe = null;
			projectID = null;
			category = null;
		}
		
		request.getSession().setAttribute("selectedProjectInSession", projectChosenSe);
		request.getSession().setAttribute("selectedprojectID", projectID);
		request.getSession().setAttribute("selectedcategory", category);
	}

	public String getSelectedProjectFromSession(HttpSession session) {

		return (String) session.getAttribute("selectedProjectInSession");
	}

	// ProjectList Session

	public void setProjectListToSession(ArrayList<ProjectData> projectsIdsMap, HttpServletRequest request) {
		if(projectsIdsMap != null) {
		int length = projectsIdsMap.size();
		ArrayList<String> projectsData = new ArrayList<>();
		for(int i = 0; i < length; i++) {
			String data = gson.toJson(projectsIdsMap.get(i));
			projectsData.add(data);
		}
		request.getSession().setAttribute("projectList", projectsData);
	}else {
		request.getSession().setAttribute("projectList", projectsIdsMap);
	}
	}
	public ArrayList<ProjectData> getProjectListFromSession(HttpSession session) {
		ArrayList<ProjectData> result = projectsDataStringToMap(session);
		return result;
	}

	public ArrayList<ProjectData> projectsDataStringToMap(HttpSession session){
		ArrayList<ProjectData> result = new ArrayList<>();
		try {
		ArrayList<String> userProjectsData = (ArrayList<String>) session.getAttribute("projectList");
		int length = userProjectsData.size();
		for(int i = 0; i < length; i++) {
			ProjectData p = gson.fromJson(userProjectsData.get(i), ProjectData.class);
			result.add(p);
		}
			}catch(Exception e) {
				logger.error(e.getMessage());
				result = null;
			}
		return result;
		
	}
	// JiraFieldList Session 

	public void setJiraFieldListToSession(Map<String, String> fieldList, HttpServletRequest request) {
		String fieldsList= new JSONObject(fieldList).toString();
		request.getSession().setAttribute("jiraFieldListList", fieldsList);
	}

	public Map<String, String> getJiraFieldListFromSession(HttpSession session) {
		
		Map<String, String> result = DataStringToMap(session, "jiraFieldListList");
		return result;
	}
	
	public Map<String,String> DataStringToMap(HttpSession session,String sessionVariable){
		Map<String,String> result = new HashMap<String,String>();
		
		try {
		String stringObj = session.getAttribute(sessionVariable).toString();
		JSONObject data = new JSONObject(stringObj); 
		
		if(!stringObj.equals("null")){
			 Iterator<?> keys = data.keys();

             while (keys.hasNext())
             {
                 String key = (String) keys.next();
                 String value = data.getString(key);
                 result.put(key, value);

             }
		}
			}catch(Exception e) {
				result = null;
			}
		return result;
		
	}
	
	// Date list month wise
	
	public void setDatesListMonthWise(ArrayList<String> dates,HttpServletRequest request) {
		
		request.getSession().setAttribute("dateListMonth", dates);
	}
	
	public ArrayList<String> getDatesListMonthWise(HttpSession session){
		
		return (ArrayList<String>) session.getAttribute("dateListMonth");
	}
	
	//Confluence URL Session
	public void setConfluenceURLToSession(ConfluencePagesData cpd, HttpServletRequest request) {
		String cpdList = new JSONObject(cpd).toString();
		request.getSession().setAttribute("cpd", cpdList);
	}

	public ConfluencePagesData getConfluenceURLFromSession(HttpSession session) {
		ConfluencePagesData cpd = new ConfluencePagesData();
		JSONObject cpdData = new JSONObject(session.getAttribute("cpd").toString()); 
		cpd.setAtpURL(cpdData.get("atpURL").toString());
		cpd.setClientURL(cpdData.get("clientURL").toString());
		cpd.setConfigurationURL(cpdData.get("configurationURL").toString());
		cpd.setNetworkURL(cpdData.get("networkURL").toString());
		cpd.setProcessURL(cpdData.get("processURL").toString());
		cpd.setTeURL(cpdData.get("teURL").toString());
		cpd.setRetroURL(cpdData.get("retroURL").toString());
		cpd.setMomURL(cpdData.get("momURL").toString());
		return (ConfluencePagesData) session.getAttribute("cpd");
	}
	
	//Goals Session
	public void setGoalsToSession(AllGoalsData goals, HttpServletRequest request) {

		request.getSession().setAttribute("goals", new JSONObject(goals).toString());
	}

	public AllGoalsData getGoalsFromSession(HttpSession session) {
		
		AllGoalsData allGoalsData = new AllGoalsData();
		try {
		JSONObject jsonObj = new JSONObject(session.getAttribute("goals").toString());
		//allGoalsData.setMeanCycleTimeEffort(jsonObj.get("meanCycleTimeEffort").toString());
		allGoalsData.setClientComplaintsTarget(Double.parseDouble(jsonObj.get("clientComplaintsTarget").toString()));
		allGoalsData.setCsatRatingTarget(Double.parseDouble(jsonObj.get("csatRatingTarget").toString()));
		allGoalsData.setCidiTarget(Double.parseDouble(jsonObj.get("cidiTarget").toString()));
		allGoalsData.setRecurrenceComplaintTarget(Double.parseDouble(jsonObj.get("recurrenceComplaintTarget").toString()));
		allGoalsData.setRiskTarget(Double.parseDouble(jsonObj.get("riskTarget").toString()));
		allGoalsData.setReqDelOnTimeTarget(Double.parseDouble(jsonObj.get("reqDelOnTimeTarget").toString()));
		GoalsAndTargetsData developmentEffectivenessTarget = new GoalsAndTargetsData();
		JSONObject developmentEffectivenessJsonObject = jsonObj.getJSONObject("developmentEffectivenessTarget");
		developmentEffectivenessTarget.setDelDefectHigh(Double.parseDouble(developmentEffectivenessJsonObject.get("delDefectHigh").toString()));
		developmentEffectivenessTarget.setDelDefectLow(Double.parseDouble(developmentEffectivenessJsonObject.get("delDefectLow").toString()));
		developmentEffectivenessTarget.setDelDefectMedium(Double.parseDouble(developmentEffectivenessJsonObject.get("delDefectMedium").toString()));
		developmentEffectivenessTarget.setDreTarget(Double.parseDouble(developmentEffectivenessJsonObject.get("dreTarget").toString()));
		developmentEffectivenessTarget.setInProcessDefects(developmentEffectivenessJsonObject.get("inProcessDefects").toString());
		developmentEffectivenessTarget.setRevEffTarget(Double.parseDouble(developmentEffectivenessJsonObject.get("revEffTarget").toString()));
		developmentEffectivenessTarget.setSrfTarget(Double.parseDouble(developmentEffectivenessJsonObject.get("srfTarget").toString()));
		allGoalsData.setDevelopmentEffectivenessTarget(developmentEffectivenessTarget);
		}catch(Exception e) {
			allGoalsData = null;
		}
		return allGoalsData;
	}
	public int checkUser(AtlassianHostUser hostUser, HttpSession session) throws JSONException, Exception {
		int isValidUser = 0;
		try {
		ArrayList<ProjectData> projectIdsMap = projectService.getProjectData();
		int length = projectIdsMap.size();
		if(length > 0) {
				isValidUser = 1;
			}else if(length == 0) {
				isValidUser = 0;
		}
		}catch(Exception e) {
			e.printStackTrace();
			isValidUser = 2;
		}
		return isValidUser;
		}
	public String checkPermissionsToView(String pageNavigator, HttpSession session, AtlassianHostUser hostUser, HttpServletRequest request,HttpServletResponse response, Model model, int ch) throws Exception, Throwable, InterruptedException, ExecutionException{
		String viewPage = "";
		ArrayList<ProjectData> pd = projectService.getProjectData();
		
//		Map<String, String> fieldList = new HashMap<>();
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser, session);
		int length = pd.size();	
		if(length == 0) {
				viewPage = "dashboardErrorPage";
			}else if(length == 1) {
				SelectChoice choice = new SelectChoice();
				choice.setProjectname(pd.get(0).getName());
				String projectDetails = pd.get(0).getId()+ "~" + pd.get(0).getCategory() + "~" + pd.get(0).getName(); 
				choice.setCatId(projectDetails);
				CompletableFuture<AllGoalsData> goals = td.getgoalsandtargets(hostUser, choice.getProjectname(), session);
				setGoalsToSession(goals.get(), request);
				
				setSelectedProjectToSession(choice, request);
				switch(ch) {
				case 0: viewPage = metricsReport(hostUser, choice, model,request,response,session);
						break;
				case 1: viewPage = riskManagementDateRangeProjectList(hostUser,choice,model,request,response,session);
						break;
				case 2: viewPage = getclientRangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 3: viewPage = rangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 4: viewPage = cycletimerangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 5: viewPage = getTestRangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 6: viewPage = getMetricsRangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 7: viewPage = getDistributionRangeSelection(hostUser, choice, model, request, response, session);
						break;
				case 8: viewPage = riskManagmentDateRangeProjectList(hostUser, choice, model, request, response, session);
						break;
				case 9: viewPage = getcidiRangeSelection(hostUser, choice, model, request, response, session);
						break;
			}
			}
			else {
			model.addAttribute("pageNavigator", pageNavigator);
			viewPage = "landingPage";
			}
		model.addAttribute("projectlist", pd);
		setProjectListToSession(pd, request);
		setJiraFieldListToSession(fieldList, request);
		return viewPage;
			}
	
	public String getUser(AtlassianHostUser hostUser) {
		 String user = hostUser.getUserAccountId().toString();
		 user = user.substring(9, user.length()-1);
		 return user;
	 }
}